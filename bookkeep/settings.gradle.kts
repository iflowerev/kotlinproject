pluginManagement {
    repositories {
        maven { url=uri("https://jitpack.io") }
        google()
        mavenCentral()
        gradlePluginPortal()
        maven { url=uri("https://oss.sonatype.org/content/repositories/snapshots") }
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven { url=uri("https://jitpack.io") }
        google()
        mavenCentral()
        maven { url=uri("https://oss.sonatype.org/content/repositories/snapshots") }
    }
}

rootProject.name = "bookkeep"
include(":app")
include(":baselibrary")
