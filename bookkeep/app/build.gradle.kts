import java.text.SimpleDateFormat
import java.util.Date

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
}

val STORFILE: String by project
val STOREPASSWORD: String by project
val KEYPASSWORD: String by project
val KEYALIAS: String by project

android {
    namespace = "com.jay.bookkeep"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.jay.bookkeep"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        ndk {
            // 设置支持的SO库架构
            abiFilters += setOf("arm64-v8a")//, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'
        }
    }

    //**********打包设置开始**********

    //签名配置
    signingConfigs {
        getByName("debug") {
            storePassword = STOREPASSWORD
            keyPassword = KEYPASSWORD
            keyAlias = KEYALIAS
            storeFile = file(STORFILE)
        }
    }

    buildTypes {

        getByName("debug") {
            //签名
            signingConfig = signingConfigs.getByName("debug")
        }

        register("alpha") {
            //继承debug配置
            initWith(getByName("debug"))
            //混淆
            isMinifyEnabled = false
            //ZipAlignEnabled优化
            isZipAlignEnabled = false
            //移除无用的resource文件
            isShrinkResources = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }

        getByName("release") {
            //继承alpha配置
            initWith(getByName("alpha"))
            //关闭debug
        }
    }

    // 输出类型
    android.applicationVariants.all {
        // 编译类型
        val buildType = this.buildType.name
        val date = SimpleDateFormat("yyyy-MM-dd").format(Date())
        outputs.all {
            // 判断是否是输出 apk 类型
            if (this is com.android.build.gradle
                .internal.api.ApkVariantOutputImpl) {
                this.outputFileName = "bk" +
                        "_${android.defaultConfig.versionName}_${date}_${buildType}.apk"
            }
        }
    }
    //**********打包设置结束**********
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    //开启viewBinding
    buildFeatures {
        viewBinding = true
        buildConfig=true
    }
}

dependencies {
    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.8.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation(project(mapOf("path" to ":baselibrary")))
    implementation(files("libs\\AMap_Location_V6.4.2_20231215.jar"))
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    //轮播图
    implementation ("io.github.youth5201314:banner:2.2.2")
    //视频播放器完整版引入
    implementation ("com.github.CarGuo.GSYVideoPlayer:GSYVideoPlayer:v8.3.5-release-jitpack")
    //图表
    implementation ("com.github.PhilJay:MPAndroidChart:v3.1.0")
    //
    implementation ("pl.droidsonroids.gif:android-gif-drawable:1.2.+")
}