package pl.droidsonroids.gif.sample
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.app.baselibrary.base.BaseActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.jay.bookkeep.R
import com.jay.bookkeep.databinding.ActivityGifMainBinding

/**
 * Main activity, hosts the pager

 * @author koral--
 */
class MainActivity : BaseActivity<ActivityGifMainBinding>() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
//		setContentView(R.layout.activity_gif_main)
		val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
		val pager = findViewById<ViewPager2>(R.id.mainViewPager)
		val mainPagerAdapter = MainPagerAdapter(this)
		pager.adapter = mainPagerAdapter
		TabLayoutMediator(tabLayout, pager) { tab, position ->
			tab.text = mainPagerAdapter.getPageTitle(position)
		}.attach()
	}
}
