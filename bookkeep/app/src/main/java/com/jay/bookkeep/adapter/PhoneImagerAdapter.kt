package com.jay.bookkeep.adapter

import android.app.Activity
import android.view.ViewGroup
import com.app.baselibrary.base.BaseAdapter
import com.app.baselibrary.base.BaseViewHolder
import com.app.baselibrary.utils.DensityUtils
import com.app.baselibrary.utils.GlideEngine
import com.jay.bookkeep.bean.Image
import com.jay.bookkeep.bean.PhoneImageData
import com.jay.bookkeep.databinding.ItemImageListBinding
import com.jay.bookkeep.databinding.ItemWallpaperListBinding

/**
 * 手机壁纸列表适配器
 */
class PhoneImagerAdapter(val activity: Activity) : BaseAdapter<PhoneImageData>(false){
    override fun bindItem(data: PhoneImageData, holder: BaseViewHolder, position: Int) {
        if(data.mType==1){
            val viewBinding = ItemImageListBinding.bind(holder.itemView)
            viewBinding.root.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            //得到的图片地址是没有前缀的，所以加上前缀否则显示不出来
            GlideEngine.getInstance().loadImage(activity,data.preview, viewBinding.wallpaper)
        }else{
            val viewBinding = ItemWallpaperListBinding.bind(holder.itemView)
            //获取imageView的LayoutParams
            val layoutParams = viewBinding.ivWallpaper.layoutParams
            if(position%2==0){
                layoutParams.height = DensityUtils.dip2px(activity,300f)
            }else{
                layoutParams.height = DensityUtils.dip2px(activity,150f)
            }
            //重新设置imageView的高度
            viewBinding.ivWallpaper.layoutParams = layoutParams
            GlideEngine.getInstance().loadImage(activity,data.thumb, viewBinding.ivWallpaper)
        }
    }
}