package com.jay.bookkeep.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.app.baselibrary.base.BaseAdapter
import com.app.baselibrary.base.BaseViewHolder
import com.jay.bookkeep.bean.VideoItemData
import com.jay.bookkeep.databinding.ItemVideoListBinding
import com.shuyu.gsyvideoplayer.GSYVideoManager
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack


/**
 * 小视频列表适配器
 */
class VideoListAdapter(val activity: Activity,val gsyVideoOptionBuilder: GSYVideoOptionBuilder) : BaseAdapter<VideoItemData>(false){

    private val TAG = "VideoListAdapter"
    override fun bindItem(data: VideoItemData, holder: BaseViewHolder, position: Int) {
        val viewBinding = ItemVideoListBinding.bind(holder.itemView)
        viewBinding.root.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        //配置视频播放器参数
        gsyVideoOptionBuilder!!.setIsTouchWiget(false)
            .setUrl(data.url)
            .setVideoTitle(data.title)
            .setCacheWithPlay(false)
            .setRotateViewAuto(true)
            .setLockLand(true)
            .setPlayTag(TAG)
            .setShowFullAnimation(true)
            .setNeedLockFull(true)
            .setPlayPosition(position)
            .setReleaseWhenLossAudio(false)
            .setVideoAllCallBack(object : GSYSampleCallBack() {
                override fun onPrepared(url: String?, vararg objects: Any?) {
                    super.onPrepared(url, *objects)
//                    if (!vh.standardGSYVideoPlayer.isIfCurrentIsFullscreen()) {
//                        //静音
//                        //GSYVideoManager.instance().setNeedMute(true);
//                    }
                }

                override fun onQuitFullscreen(url: String?, vararg objects: Any?) {
                    super.onQuitFullscreen(url, *objects)
                    //全屏不静音
                    //GSYVideoManager.instance().setNeedMute(true);
                }

                override fun onEnterFullscreen(url: String?, vararg objects: Any?) {
                    super.onEnterFullscreen(url, *objects)
                    GSYVideoManager.instance().isNeedMute = false
                    viewBinding.detailPlayer.currentPlayer.titleTextView.text = objects!![0].toString()
                }
            }).build(viewBinding.detailPlayer)

        //设置返回键
        viewBinding.detailPlayer.backButton.visibility = View.GONE
        viewBinding.detailPlayer.fullscreenButton.visibility = View.GONE
        viewBinding.ivImager.setImageDrawable(data.bitmap)
//        //设置全屏按键功能
//        viewBinding.detailPlayer.fullscreenButton.setOnClickListener {
//            viewBinding.detailPlayer.startWindowFullscreen(activity, false, true);
//        }
        //实现第一个视频自动播放
        if(position==0){
            viewBinding.detailPlayer.startPlayLogic()
        }
    }
}