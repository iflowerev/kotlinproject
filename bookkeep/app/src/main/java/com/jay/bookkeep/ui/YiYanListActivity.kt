package com.jay.bookkeep.ui
import android.os.Bundle
import androidx.core.os.bundleOf
import com.app.baselibrary.base.BaseActivity
import com.app.baselibrary.ktx.onLongClick
import com.app.baselibrary.ktx.startCustomActivity
import com.jay.bookkeep.databinding.ActivityYiYanListBinding

/**
 * 每日一言
 */
class YiYanListActivity: BaseActivity<ActivityYiYanListBinding>(){

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        requireViewBinding().btnYiyan.onLongClick {
            startCustomActivity<YiYanActivity>(bundleOf(Pair("classType",0)))
        }
        requireViewBinding().btnAnwei.onLongClick {
            startCustomActivity<YiYanActivity>(bundleOf(Pair("classType",1)))
        }
        requireViewBinding().btnTime.onLongClick {
            startCustomActivity<YiYanActivity>(bundleOf(Pair("classType",2)))
        }
    }
}