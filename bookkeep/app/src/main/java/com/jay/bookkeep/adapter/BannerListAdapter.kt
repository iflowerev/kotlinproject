package com.jay.bookkeep.adapter
import android.app.Activity
import android.graphics.Bitmap
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.app.baselibrary.utils.GlideEngine
import com.app.baselibrary.utils.LogUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.jay.bookkeep.utils.ImageHolder
import com.youth.banner.adapter.BannerAdapter

/**
 * 自定义布局，图片
 */
class BannerListAdapter     //设置数据，也可以调用banner提供的方法,或者自己在adapter中实现
    (private val activity: Activity,mDatas: List<String>) :
    BannerAdapter<String, ImageHolder>(mDatas) {
    //更新数据
    fun updateData(data:  List<String>) {
        //这里的代码自己发挥，比如如下的写法等等
        mDatas.clear()
        mDatas.addAll(data)
        notifyDataSetChanged()
    }

    //创建ViewHolder，可以用viewType这个字段来区分不同的ViewHolder
    override fun onCreateHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val imageView = ImageView(parent.context)
        //注意，必须设置为match_parent，这个是viewpager2强制要求的
        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        imageView.layoutParams = params
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        return ImageHolder(imageView)
    }

    override fun onBindView(holder: ImageHolder, data: String, position: Int, size: Int) {
        /**
         * 加载网络图片
         */
        GlideEngine.getInstance().loadImage(activity,data, holder.imageView)
//        LogUtil.logE(data)
//        Glide.with(activity)
//            .asBitmap()
//            .load(data)
////          .apply(RequestOptions().placeholder(R.mipmap.ic_launcher))
//            .into(object : BitmapImageViewTarget(holder.imageView) {
//                override fun setResource(resource: Bitmap?) {
//                    val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(activity.resources, resource)
//                    circularBitmapDrawable.cornerRadius = 5f
//                    holder.imageView.setImageDrawable(circularBitmapDrawable)
//                }
//            })

    }
}