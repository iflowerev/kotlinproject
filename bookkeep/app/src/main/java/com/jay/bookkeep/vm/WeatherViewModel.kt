package com.jay.bookkeep.vm
import androidx.lifecycle.MutableLiveData
import com.app.baselibrary.base.BaseViewModel
import com.jay.bookkeep.bean.WeatherDataEntity

/**
@author: yangjie
@descruption: ViewModel
*/
class WeatherViewModel : BaseViewModel() {

    private val _mWeatherDataEntity= MutableLiveData<WeatherDataEntity>()

    val mWeatherDataEntity by lazy { _mWeatherDataEntity }

    private val weatherRepo by lazy { WeatherRepo(this) }
    fun getWeather(msg:String) {
        weatherRepo.getWeather(msg) {
            _mWeatherDataEntity.postValue(it)
        }
    }
}