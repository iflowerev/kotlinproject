package com.jay.bookkeep.ui
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.startCustomActivity
import com.jay.bookkeep.adapter.CosplayListAdapter
import com.jay.bookkeep.bean.CosplayItemData
import com.jay.bookkeep.databinding.ActivityCosplayBinding
import com.jay.bookkeep.vm.WallpaperViewModel

/**
 * CosPlay图片
 */
class CosplayActivity: BaseViewModelActivity<ActivityCosplayBinding,WallpaperViewModel>(){

    private var currPage: Int = 0

    private val mAdapter by lazy {  CosplayListAdapter(this) }

    private var dataList:List<CosplayItemData>?=null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        requireViewBinding().listData.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        requireViewBinding().listData.adapter = mAdapter
        getData()
        initData()
        initEvent()
    }

    private fun  initData(){
       requireViewModel().mCosplayData.observerNotNull {
           it.data.let { it1->
                   dataList=it1
           }
           mAdapter.setData(dataList!!)
//           if (currPage == 0) {
//               //第一页
//               it.data.let { it1->
//                   dataList=it1
//               }
//               requireViewBinding().smartrefreshList.setEnableLoadMore(true)
//               requireViewBinding().smartrefreshList.finishRefresh()
//           }else{
//               dataList=requireViewModel().getCosPlayList(it.data,dataList!!)
//           }
//            mAdapter.setData(dataList!!)
//           if (currPage!=0&&it.data==null){
//               //最后一页
//               requireViewBinding().smartrefreshList.setEnableLoadMore(false)
//           }
//
//           requireViewBinding().smartrefreshList.finishLoadMore()
       }
    }

    private fun  initEvent(){
//        requireViewBinding().smartrefreshList.setOnRefreshListener {
//            currPage = 0
//            getData()
//            finishRefresh(requireViewBinding().smartrefreshList)
//        }
//        requireViewBinding().smartrefreshList.setOnLoadMoreListener {
//            currPage++
//            getData()
//            finishLoadMore(requireViewBinding().smartrefreshList)
//        }
        mAdapter.setOnItemClickListener {
            startCustomActivity<CosplayDetailsActivity>(bundleOf(Pair("details",dataList!![it])))
        }
    }
    private fun getData(){
        requireViewModel().getCosplay(currPage)
    }
}