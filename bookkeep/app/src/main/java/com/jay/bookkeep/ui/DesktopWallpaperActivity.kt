package com.jay.bookkeep.ui

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.startCustomActivity
import com.jay.bookkeep.adapter.DesktopImagerAdapter
import com.jay.bookkeep.bean.ImageListData
import com.jay.bookkeep.databinding.ActivityWallpaperBinding
import com.jay.bookkeep.vm.WallpaperViewModel

/**
 * 桌面壁纸
 */
class DesktopWallpaperActivity: BaseViewModelActivity<ActivityWallpaperBinding, WallpaperViewModel>(){

    private val mAdapter by lazy { DesktopImagerAdapter(this@DesktopWallpaperActivity) }

    private var mImageListData: ImageListData?=null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        val number = (Math.random() * 100).toInt()
        requireViewModel().bingying(number,50)
        requireViewBinding().listImage.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        requireViewBinding().listImage.adapter = mAdapter
        initData()
    }

    private fun  initData(){
        requireViewModel().mDataEntity.observerNotNull {
            mImageListData=requireViewModel().getImageList(it.images,0)
            mAdapter.setData(mImageListData!!.mData)
        }
        mAdapter.setOnItemClickListener {
            startCustomActivity<DesktopWallpaperListActivity>(bundleOf(Pair("position",it),Pair("image",mImageListData)))
        }
    }

}