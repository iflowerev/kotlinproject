package com.jay.bookkeep.ui
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.startCustomActivity
import com.jay.bookkeep.adapter.PhoneImagerAdapter
import com.jay.bookkeep.bean.PhoneListData
import com.jay.bookkeep.databinding.ActivityWallpaperBinding
import com.jay.bookkeep.vm.WallpaperViewModel

/**
 * 手机壁纸
 */
class PhoneWallpaperActivity: BaseViewModelActivity<ActivityWallpaperBinding, WallpaperViewModel>(){

    private val mAdapter by lazy { PhoneImagerAdapter(this@PhoneWallpaperActivity) }

    private var mImageListData: PhoneListData?=null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
      requireViewModel().getWallPaper(30)
        requireViewBinding().listImage.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        requireViewBinding().listImage.adapter = mAdapter
        initData()
    }

    private fun  initData(){
        requireViewModel().mPhoneImage.observerNotNull {
            mImageListData=requireViewModel().getPhoneImageList(it.res.vertical,0)
            mAdapter.setData(mImageListData!!.mData)
        }
        mAdapter.setOnItemClickListener {
            startCustomActivity<PhoneWallpaperListActivity>(bundleOf(Pair("position",it),Pair("image",mImageListData)))
        }
    }
}