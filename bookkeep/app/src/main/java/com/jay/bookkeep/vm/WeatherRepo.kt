package com.jay.bookkeep.vm
import com.app.baselibrary.ktx.toEntity
import com.jay.bookkeep.api.WallpaperHttpService
import com.jay.bookkeep.bean.WeatherDataEntity

/**
@author: yangjie
@descruption: 数据仓库
*/
class WeatherRepo(weatherViewModel: WeatherViewModel) : BaseHttpRepo(weatherViewModel) {

    fun getWeather( msg:String,action: (WeatherDataEntity)->Unit){
        lifecycleDialogHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getWeather(msg,1))
            .collectMain({
               asString()
            },{
                it.toEntity<WeatherDataEntity>()?.let { it1 -> action.invoke(it1) }
            })
    }


}