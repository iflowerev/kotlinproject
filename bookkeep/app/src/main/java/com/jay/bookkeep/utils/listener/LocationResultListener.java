package com.jay.bookkeep.utils.listener;

import com.amap.api.location.AMapLocation;

public interface LocationResultListener {

    void callBackLocation(AMapLocation location, int code);

}
