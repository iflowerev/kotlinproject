package com.jay.bookkeep.ui
import android.content.res.Configuration
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.utils.DensityUtils
import com.jay.bookkeep.R
import com.jay.bookkeep.adapter.VideoListAdapter
import com.jay.bookkeep.bean.VideoItemData
import com.jay.bookkeep.databinding.ActivityVideoListBinding
import com.jay.bookkeep.utils.ScrollCalculatorHelper
import com.jay.bookkeep.vm.WallpaperViewModel
import com.shuyu.gsyvideoplayer.GSYVideoManager
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder

/**
 * 小视频列表
 */
class VideoListActivity: BaseViewModelActivity<ActivityVideoListBinding, WallpaperViewModel>(){

    var scrollCalculatorHelper: ScrollCalculatorHelper? = null

    private var dataList:MutableList<VideoItemData> = ArrayList()


    private val mAdapter by lazy { VideoListAdapter(this@VideoListActivity, GSYVideoOptionBuilder()) }

    private  val mp4_a = "https://tucdn.wpon.cn/api-girl/videos/BMjAyMTEyMTYwMjI4MzJfMTQ0ODQ5Mjc2Ml82Mjg0MzkxNTI1Ml8xXzM=_b_Beab878b482fd5750bd3f2b4b277040bc.mp4"
    private  val mp4_b = "https://tucdn.wpon.cn/api-girl/videos/BMjAyMTExMTAxNTIxNTNfMjU2MjQ0NDg5Ml82MDYwNzYwMjMxM18xXzM=_b_B931f6618a8cb31cc6ed97e109eda2753.mp4"
    private  val mp4_c= "http://q.weishi.qq.com/q.weishi.qq.com/szg_3476_50001_0b533qaekaaaliacybvwjjsvdxgdixoaarka.f622.mp4?dis_k=fdd2508b6837316745816fc4f2d41ddd&dis_t=1707203373&fromtag=0&pver=1.1.0"
    private  val mp4_d= "http://v.weishi.qq.com/v.weishi.qq.com/gzc_6295_1047_0bc3bmevyaaj54anef44jzsr4cyelqfqsxca.f70.mp4?dis_k=ed963d5493e497b3e755f95bb1292f42&dis_t=1707203123&fromtag=0&pver=1.0.0"
    private  val mp4_e= "http://v.weishi.qq.com/v.weishi.qq.com/gzc_4284_1047_0bc3eubg6aacnialsuvparsrkjien4sqe32a.f70.mp4?dis_k=9dd9ce6cff19ce70c68c939e1e69739b&dis_t=1707203415&fromtag=0&pver=1.0.0"
    private  val mp4_f= "http://q.weishi.qq.com/q.weishi.qq.com/szg_8748_50001_0b53y4acyaaacyaeqpago5svdr6dftdqalca.f622.mp4?dis_k=1072c9e72030bc585f1f561b94084542&dis_t=1707203452&fromtag=0&pver=1.1.0"

    private  val mp4_g= "https://tucdn.wpon.cn/api-girl/videos/95.mp4"
    private  val mp4_h= "https://tucdn.wpon.cn/api-girl/videos/42.mp4"
    private  val mp4_i= "https://tucdn.wpon.cn/api-girl/videos/BMjAyMTExMjYyMTE1NTBfMjIzODExNjlfNjE1ODg1MTg3NzNfMV8z_b_Bc65a11578896b029e9e6ce45feb0acfb.mp4"
    private  val mp4_j= "https://tucdn.wpon.cn/api-girl/videos/BMjAyMTEyMjAxMTEyMDRfMTQ0ODQ5Mjc2Ml82MzEzODgyOTQyNV8xXzM=_b_B06a131c1a686fc80ebffa3938f84cb67.mp4"
    private  val mp4_m= "https://tucdn.wpon.cn/api-girl/videos/21.mp4"
    private  val mp4_n= "https://tucdn.wpon.cn/api-girl/videos/57.mp4"
    private  val mp4_k= "https://tucdn.wpon.cn/api-girl/videos/BMjAyMTExMjYxODMzMzBfMTQ4NDc5Nzg0Ml82MTU3NDM4MTk3Ml8xXzM=_b_B5560b6df28ac83dba88bee5c92b447ff.mp4"
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        requireViewModel().getVideo()
        initData()
        initView()
    }
    private fun  initData(){
        dataList.add(VideoItemData(mp4_a,"mp4_a  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_b,"mp4_b  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_c,"mp4_c  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_d,"mp4_d  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_e,"mp4_e  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_f,"mp4_f  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_h,"mp4_h  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_g,"mp4_g  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_i,"mp4_i  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_j,"mp4_j  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_m,"mp4_m  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_n,"mp4_n  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
        dataList.add(VideoItemData(mp4_k,"mp4_k  ", ContextCompat.getDrawable(this, R.mipmap.ic_launcher)!!))
//        for(i in 0..10){
//            val mData= if (i % 2 == 0) {
//                ContextCompat.getDrawable(this, R.mipmap.ic_launcher)
//                    ?.let { VideoItemData(mp4_a,"傀儡偶段のVideo  " + i, it) }
//            } else {
//                ContextCompat.getDrawable(this, R.mipmap.ic_launcher)
//                    ?.let { VideoItemData(mp4_b,"傀儡偶段のVideo  " + i, it) }
//            }
//            dataList.add(mData!!)
//        }

    }

    private fun initView(){
        val linearLayoutManager = LinearLayoutManager(this)
        requireViewBinding().videoList.layoutManager= linearLayoutManager
        //获取屏幕宽高
        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)
        //自定播放帮助类 限定范围为屏幕一半的上下偏移180   括号里不用在意 因为是一个item一个屏幕
        scrollCalculatorHelper = ScrollCalculatorHelper(R.id.video_list
        , dm.heightPixels / 2 - DensityUtils.dip2px(this, 180f)
        , dm.heightPixels / 2 + DensityUtils.dip2px(this, 180f))
        //让RecyclerView有ViewPager的翻页效果
        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(requireViewBinding().videoList)
        //设置LayoutManager和Adapter
        requireViewBinding().videoList.adapter=mAdapter
        mAdapter.setData(dataList)
        //设置滑动监听
        requireViewBinding().videoList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            //第一个可见视图,最后一个可见视图
            var firstVisibleItem = 0
            var lastVisibleItem = 0
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //如果newState的状态==RecyclerView.SCROLL_STATE_IDLE;
                //播放对应的视频
                scrollCalculatorHelper!!.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition()
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                //一屏幕显示一个item 所以固定1
                //实时获取设置 当前显示的GSYBaseVideoPlayer的下标
                scrollCalculatorHelper!!.onScroll( requireViewBinding().videoList,firstVisibleItem,lastVisibleItem,1)
            }
        })
    }
    override fun onPause() {
        super.onPause()
        GSYVideoManager.onPause()
    }
    override fun onResume() {
        super.onResume()
        GSYVideoManager.onResume()
    }
    override fun onDestroy() {
        super.onDestroy()
        GSYVideoManager.releaseAllVideos()
    }

//    override fun onConfigurationChanged(newConfig: Configuration) {
//        val mConfiguration = this.resources.configuration
//        val ori = mConfiguration.orientation
//        if (ori == Configuration.ORIENTATION_LANDSCAPE) {
//            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); //隐藏状态栏
//
//        } else if (ori == Configuration.ORIENTATION_PORTRAIT) {
//            //当前为竖屏
//            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); //显示状态栏
//        }
//        super.onConfigurationChanged(newConfig)
//    }
}