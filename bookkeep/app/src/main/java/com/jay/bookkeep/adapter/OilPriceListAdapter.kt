package com.jay.bookkeep.adapter
import android.app.Activity
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.baselibrary.base.BaseAdapter
import com.app.baselibrary.base.BaseViewHolder
import com.jay.bookkeep.bean.OilPriceData
import com.jay.bookkeep.databinding.ItemOilPriceBinding


/**
 * 油价列表适配器
 */
class OilPriceListAdapter(val activity: Activity) : BaseAdapter<OilPriceData>(false){
    override fun bindItem(data: OilPriceData, holder: BaseViewHolder, position: Int) {
        val viewBinding = ItemOilPriceBinding.bind(holder.itemView)
        viewBinding.tvCity.text=data.cityName
        viewBinding.listData.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        viewBinding.listData.apply {
            adapter = OilPriceItemAdapter().apply {
                setData(data.mData)
            }
        }
    }
}