package com.jay.bookkeep.ui
import android.app.WallpaperManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.viewpager2.widget.ViewPager2
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.onLongClick
import com.app.baselibrary.ktx.shortToast
import com.app.baselibrary.utils.GlideEngine
import com.app.baselibrary.utils.ImageBitampListener
import com.app.baselibrary.utils.ImageUtil
import com.app.baselibrary.utils.PermissionUtils
import com.jay.bookkeep.adapter.DesktopImagerAdapter
import com.jay.bookkeep.bean.ImageData
import com.jay.bookkeep.bean.ImageListData
import com.jay.bookkeep.databinding.ActivityImageBinding
import com.jay.bookkeep.vm.WallpaperViewModel
import java.io.IOException

/**
 * 壁纸预览
 */
class DesktopWallpaperListActivity : BaseViewModelActivity<ActivityImageBinding,WallpaperViewModel>() {

    private val mAdapter by lazy { DesktopImagerAdapter(this@DesktopWallpaperListActivity) }


    private val mPosition by lazy { intent.getIntExtra("position",0) }


    private val mImageData by lazy { intent.getParcelableExtra<ImageListData>("image") }


    var wallpaperUrl: String? = null

    private var bitmap: Bitmap? = null

    private var mData:List<ImageData>?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PermissionUtils.getInstance().requestStargePermissions(this,object :
            PermissionUtils.ActivityResultData{
            override fun onDataesult(code: Int) {
            }
        })
         requireViewBinding().vp.adapter=mAdapter
         requireViewBinding().vp.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                wallpaperUrl = "http://cn.bing.com"+mImageData!!.mData[position].url
                GlideEngine.getInstance().getBitmapImage(this@DesktopWallpaperListActivity,wallpaperUrl!!,object : ImageBitampListener {
                    override fun bitmapCallBack(mBitamp: Bitmap?) {
                        bitmap=mBitamp
                    }
                })
//                bitmap = getBitMap(wallpaperUrl)
            }
        })
        wallpaperUrl="http://cn.bing.com"+mImageData!!.mData[mPosition].url
        GlideEngine.getInstance().getBitmapImage(this,wallpaperUrl!!,object : ImageBitampListener {
            override fun bitmapCallBack(mBitamp: Bitmap?) {
                bitmap=mBitamp
            }
        })
        mData=requireViewModel().changeImageList(mImageData!!.mData,1)
        mAdapter.setData(mData!!)
        requireViewBinding().vp.setCurrentItem(mPosition,false)
        initEvent()
    }

    private fun  initEvent(){
        requireViewBinding().btnSettingWallpaper.onLongClick {
            //设置壁纸
            if(bitmap!=null){
                showProgress()
                SetWallPaper()
            }else{
                shortToast("bitmap不能为null")
            }
        }
        requireViewBinding().btnDownload.onLongClick {
            //下载壁纸
            if(bitmap!=null){
                showProgress()
                ImageUtil.savePictures(context, bitmap!!)
                shortToast("壁纸保存成功")
                hideProgress()
            }else{
                shortToast("bitmap不能为null")
            }
        }
    }

    //设置壁纸
    fun SetWallPaper(){
        val mWallManager = WallpaperManager.getInstance(this)
        try {
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val height = metrics.heightPixels
            val width = metrics.widthPixels
//           if (ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.READ_EXTERNAL_STORAGE
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//               shortToast("请打开存储权限")
//                return
//            } else {
//               // 获取当前壁纸（非缩略图）
//               val drawable=mWallManager.drawable
//           }
            // 获取当前壁纸的偏移量
//            val xOffset = mWallManager.desiredMinimumWidth
//            val yOffset = mWallManager.desiredMinimumHeight
            // 获取当前壁纸的尺寸
//            val width = mWallManager.desiredMinimumWidth
//            val height = mWallManager.desiredMinimumHeight
//            LogUtil.logE("$width---------$height")
//            val newBitmap = Bitmap.createScaledBitmap(bitmap!!, width, height, true)
            mWallManager.setBitmap(bitmap!!)
            shortToast("桌面壁纸设置成功")
        } catch (e: IOException) {
            e.printStackTrace()
        }
        hideProgress()
    }
}