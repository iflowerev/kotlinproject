package com.jay.bookkeep.api
import com.app.baselibrary.http.HttpConfig
import com.app.baselibrary.http.flow.HttpFlow
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
@author: yangjie
@descruption: 壁纸模块接口
 */
interface WallpaperHttpService {


    companion object {
        fun invoke(mType:Int): WallpaperHttpService {
          return   HttpConfig.createMultipleService(WallpaperHttpService::class.java,mType)
        }

        val singleInvoke = HttpConfig.createService(WallpaperHttpService::class.java,0)

    }

    //必应一图
    @GET("/HPImageArchive.aspx?format=js")
    fun biying(@Query("idx") idx:Int,@Query("n") size:Int): HttpFlow
   //手机壁纸
    @GET("/v1/vertical/vertical?skip=180&adult=false&first=0&order=hot")
    fun getWallPaper(@Query("limit") size:Int): HttpFlow
    //每日一言
    @GET("/api/yiyan/index.php")
    fun getYiYan(): HttpFlow

    @GET("/api/tiangou/index.php")
    fun getTg(): HttpFlow

    @GET("/api/api-wenan-anwei/index.php")
    fun getComfort(@Query("type") type:String): HttpFlow

    //朋友圈一言
    @GET("/api/pyq/index.php")
    fun getPyq(@Query("aa1") type:String): HttpFlow
    //时间一言
    @GET("/api/time-tx/index.php")
    fun getTimeTx(@Query("type") type:String): HttpFlow

    @GET("/api-girl/index.php")
    fun getVideo(@Query("wpon") wpon:String): HttpFlow

    @GET("/api/api-tplist/go.php/api/picture/index")
    fun getCosplay(@Query("page") page:Int): HttpFlow

    //油价
    @GET("/api/api-yj/index.php")
    fun getOilPrice(@Query("shengfen") shengfen:String): HttpFlow

    //天气
    @GET("/api/api-tianqi-3/index.php")
    fun getWeather(@Query("msg") msg:String,@Query("type") type:Int): HttpFlow
}