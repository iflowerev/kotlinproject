package com.jay.bookkeep.ui
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.amap.api.location.AMapLocation
import com.app.baselibrary.base.BaseActivity
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.onLongClick
import com.app.baselibrary.ktx.startCustomActivity
import com.app.baselibrary.utils.LogUtil
import com.app.baselibrary.utils.PermissionUtils
import com.jay.bookkeep.MyApplication
import com.jay.bookkeep.adapter.CosplayListAdapter
import com.jay.bookkeep.adapter.OilPriceListAdapter
import com.jay.bookkeep.databinding.ActivityOilPriceBinding
import com.jay.bookkeep.databinding.ActivityYiYanListBinding
import com.jay.bookkeep.utils.listener.LocationResultListener
import com.jay.bookkeep.utils.listener.MapLocationListener
import com.jay.bookkeep.vm.OilPriceViewModel

/**
 * 油价
 */
class OilPriceActivity: BaseViewModelActivity<ActivityOilPriceBinding,OilPriceViewModel>(),
    LocationResultListener {

    private val mAdapter by lazy {  OilPriceListAdapter(this) }
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        if(MyApplication.getMyMapLocation()!=null){
            getHttpData()
        }else{
            showPerDialog()
        }
        requireViewBinding().listData.layoutManager = GridLayoutManager(this, 2)
        requireViewBinding().listData.adapter = mAdapter
        requireViewModel().mOilPriceList.observerNotNull {
            mAdapter.setData(it.mOilPriceData)
        }
    }

    private fun showPerDialog(){
        PermissionUtils.getInstance().requsetLocationPermission(this,object :
            PermissionUtils.ActivityResultData{
            override fun onDataesult(code: Int){
                if(code==200){
                    //进行定位
                    MapLocationListener.getInstance().setLocationResultListener(this@OilPriceActivity)
                    MapLocationListener.getInstance().startLocation()
                }
            }
        })

    }
    override fun callBackLocation(location: AMapLocation?, code: Int) {
        if (code == 0) {
            //定位成功
            MyApplication.setMyMapLocation(location!!)
            MapLocationListener.getInstance().stopLocation()
            getHttpData()
        }else{
            //定位失败
            LogUtil.logE("定位失败了----------")
        }
    }

    private fun getHttpData(){
        requireViewModel().getOilPrice(MyApplication.getMyMapLocation()!!.province)
    }
}