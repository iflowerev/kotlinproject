package com.jay.bookkeep.view;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;
import java.util.ArrayList;

/**
 * @description: 自定义x轴数据$
 */
public class CustomXAxisFormatter extends ValueFormatter {

    private ArrayList<String> xData;

    public CustomXAxisFormatter(ArrayList<String> mXData){
        xData=mXData;
    }

    @Override
    public String getAxisLabel(float value, AxisBase axis){
        int position=(int)value;
        if(xData.size()==0){
            return "";
        }else{
            return xData.get(position);
        }
    }
}

