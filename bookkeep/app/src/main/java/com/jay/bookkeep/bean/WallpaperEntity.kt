package com.jay.bookkeep.bean
import android.graphics.drawable.Drawable
import android.os.Parcelable
import com.app.baselibrary.base.DataType
import com.jay.bookkeep.R
import kotlinx.android.parcel.Parcelize

/**
 * 必应图片
 */
data class WelcomeData(
    val images: List<Image>,
    val tooltips: Tooltips
)

data class Image(
    val bot: Int,
    val copyright: String,
    val copyrightlink: String,
    val drk: Int,
    val enddate: String,
    val fullstartdate: String,
    val hs: List<Any>,
    val hsh: String,
    val quiz: String,
    val startdate: String,
    val title: String,
    val top: Int,
    val url: String,
    val urlbase: String,
    val wp: Boolean
)
data class Tooltips(
    val loading: String,
    val next: String,
    val previous: String,
    val walle: String,
    val walls: String
)
@Parcelize
 data class ImageListData(
   val mData:List<ImageData>
 ):Parcelable

@Parcelize
data class ImageData(
    val url: String,
    val copyright: String,
    val title: String,
    var mType:Int, //显示哪种布局
): DataType,Parcelable{

    override fun dataType(): Int{
        return 1
    }
    override fun layoutId(): Int {
        return if(mType==0){
            R.layout.item_wallpaper_list
        }else{
            R.layout.item_image_list
        }
    }
}

/**
 * 手机壁纸
 */
data class PhoneImage(
    val code: Int,
    val msg: String,
    val res: Res
)

data class Res(
    val vertical: List<Vertical>
)

data class Vertical(
    val atime: Double,
    val cid: List<String>,
    val cr: Boolean,
    val desc: String,
    val favs: Int,
    val id: String,
    val img: String,
    val ncos: Int,
    val preview: String,
    val rank: Int,
    val rule: String,
    val source_type: String,
    val store: String,
    val tag: List<String>,
    val thumb: String,
    val url: List<Any>,
    val views: Int,
    val wp: String,
    val xr: Boolean
)

@Parcelize
data class PhoneListData(
    val mData:List<PhoneImageData>
):Parcelable

@Parcelize
data class PhoneImageData(
    val img: String,
    val preview: String,
    val thumb: String,
    val tag: List<String>,
    var mType:Int, //显示哪种布局
): DataType,Parcelable{

    override fun dataType(): Int{
        return 1
    }
    override fun layoutId(): Int {
        return if(mType==0){
            R.layout.item_wallpaper_list
        }else{
            R.layout.item_image_list
        }
    }
}

data class AnWeiData(
    val anwei: String
)
data class PyqData(
    val pyq: String
)
data class TimeTxData(
    val code: Int,
    val msg: String,
    val nowtime: String,
    val nxyj: String
)

data class VideoData(
    val error: Int,
    val mp4: String,
    val result: Int
)

data class VideoItemData(
    val url: String,
    val title: String,
    val bitmap: Drawable,
): DataType{

    override fun dataType(): Int{
        return 1
    }
    override fun layoutId(): Int {
        return R.layout.item_video_list
    }
}
@Parcelize
data class CosplayData(
    val code: Int,
    val msg: String,
    val `data`: List<CosplayItemData>?=null,
):Parcelable


@Parcelize
data class CosplayItemData(
    val clientcover: String,
    val clientcover1: String,
    val cover: String,
    val createdate: String,
    val datetime: String,
    val desc: String,
    val imgsum: String,
    val pics: List<String>,
    val postid: String,
    val pvnum: String,
    val replynum: String,
    val scover: String,
    val setid: String,
    val setname: String,
    val seturl: String,
    val tcover: String,
    val topicname: String
): DataType,Parcelable{

    override fun dataType(): Int{
        return 1
    }
    override fun layoutId(): Int {
        return R.layout.item_cosplay_list
    }
}
