package com.jay.bookkeep.vm
import com.app.baselibrary.ktx.toEntity
import com.jay.bookkeep.api.WallpaperHttpService
import com.jay.bookkeep.bean.AnWeiData
import com.jay.bookkeep.bean.CosplayData
import com.jay.bookkeep.bean.PhoneImage
import com.jay.bookkeep.bean.PyqData
import com.jay.bookkeep.bean.TimeTxData
import com.jay.bookkeep.bean.VideoData
import com.jay.bookkeep.bean.WelcomeData

/**
@author: yangjie
@descruption: 数据仓库
*/
class WallpaperPageRepo(wallpaperViewModel: WallpaperViewModel) : BaseHttpRepo(wallpaperViewModel) {


    fun biying(idx:Int,size:Int,action: (WelcomeData)->Unit){

        lifecycleHttpFlow(httpFlow = WallpaperHttpService.singleInvoke.biying(idx,size))
            .collectMain({
               asString()
            },{
                it.toEntity<WelcomeData>()?.let { it1 -> action.invoke(it1) }
            })
    }

    fun getWallPaper(size:Int,action: (PhoneImage)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(1).getWallPaper(size))
            .collectMain({
                asString()
            },{
                it.toEntity<PhoneImage>()?.let { it1 -> action.invoke(it1) }
            })
    }

    fun getYiYan(action: (String)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getYiYan())
            .collectMain({
                asString()
            },{
                action.invoke(it)
            })
    }


    fun getTg(action: (String)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getTg())
            .collectMain({
                asString()
            },{
                action.invoke(it)
            })
    }
    fun getComfort(action: (AnWeiData)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getComfort("json"))
            .collectMain({
                asString()
            },{
                it.toEntity<AnWeiData>()?.let { it1 -> action.invoke(it1) }
            })
    }

    fun getPyq(action: (PyqData)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getPyq("json"))
            .collectMain({
                asString()
            },{
                it.toEntity<PyqData>()?.let { it1 -> action.invoke(it1) }
            })
    }

    fun getCosplay(page:Int,action: (CosplayData)->Unit){
        lifecycleDialogHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getCosplay(page))
            .collectMain({
                asString()
            },{
                it.toEntity<CosplayData>()?.let { it1 -> action.invoke(it1) }
            })
    }

    fun getTimeTx(action: (TimeTxData)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getTimeTx("json"))
            .collectMain({
                asString()
            },{
                it.toEntity<TimeTxData>()?.let { it1 -> action.invoke(it1) }
            })
    }

    fun getVideo(action: (VideoData)->Unit){
        lifecycleHttpFlow(httpFlow = WallpaperHttpService.invoke(3).getVideo("json"))
            .collectMain({
                asString()
            },{
                it.toEntity<VideoData>()?.let { it1 -> action.invoke(it1) }
            })
    }

}