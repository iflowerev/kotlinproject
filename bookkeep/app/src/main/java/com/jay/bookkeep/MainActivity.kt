package com.jay.bookkeep
import android.content.Intent
import android.os.Bundle
import com.app.baselibrary.base.BaseActivity
import com.app.baselibrary.ktx.onLongClick
import com.app.baselibrary.ktx.startCustomActivity
import com.jay.bookkeep.databinding.ActivityMainBinding
import com.jay.bookkeep.ui.CosplayActivity
import com.jay.bookkeep.ui.DesktopWallpaperActivity
import com.jay.bookkeep.ui.OilPriceActivity
import com.jay.bookkeep.ui.PhoneWallpaperActivity
import com.jay.bookkeep.ui.VideoListActivity
import com.jay.bookkeep.ui.WeatherActivity
import com.jay.bookkeep.ui.YiYanListActivity
import pl.droidsonroids.gif.sample.MainActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        requireViewBinding().btnDesktop.onLongClick {
         startCustomActivity<DesktopWallpaperActivity>()
        }
        requireViewBinding().btnPhone.onLongClick {
            startCustomActivity<PhoneWallpaperActivity>()
        }
        requireViewBinding().btnYiyan.onLongClick {
            startCustomActivity<YiYanListActivity>()
        }
        requireViewBinding().btnVideo.onLongClick {
            startCustomActivity<VideoListActivity>()
        }
        requireViewBinding().btnOliPrice.onLongClick {
            startCustomActivity<OilPriceActivity>()
        }
        requireViewBinding().btnWeather.onLongClick {
            startCustomActivity<WeatherActivity>()
        }
        requireViewBinding().btnGif.onLongClick {
            startCustomActivity<MainActivity>()
        }
    }

}