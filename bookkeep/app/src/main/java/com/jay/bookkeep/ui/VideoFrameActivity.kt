package com.jay.bookkeep.ui
import android.os.Bundle
import android.provider.MediaStore
import android.text.Html
import com.app.baselibrary.base.BaseActivity
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.runIOThread
import com.app.baselibrary.ktx.runUI
import com.app.baselibrary.utils.VideoFrameUtils
import com.jay.bookkeep.databinding.ActivityVideoFrameBinding
import com.jay.bookkeep.databinding.ActivityYiYanBinding
import com.jay.bookkeep.vm.WallpaperViewModel

/**
 * 视频帧显示
 */
class VideoFrameActivity: BaseActivity<ActivityVideoFrameBinding>(){

    private val videoUrl1="https://tucdn.wpon.cn/api-girl/videos/BMjAyMTEyMTYwODA3MjZfMTQ0ODQ5Mjc2Ml82Mjg1ODI0NjA1Ml8xXzM=_b_B3ea75778c25324e8acd1c02ceba8d97d.mp4"
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        initData()
    }

    private fun  initData(){
       runIOThread {
           val mBitamp=VideoFrameUtils.createVideoThumbnail(videoUrl1, MediaStore.Images.Thumbnails.MINI_KIND)
           runUI {
               requireViewBinding().ivImage1.setImageBitmap(mBitamp)
           }
       }
      VideoFrameUtils.loadCover(requireViewBinding().ivImage2,videoUrl1,this)
    }
}