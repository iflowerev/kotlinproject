package com.jay.bookkeep.vm
import com.app.baselibrary.ktx.gson
import com.app.baselibrary.ktx.toEntity
import com.jay.bookkeep.R
import com.jay.bookkeep.api.WallpaperHttpService
import com.jay.bookkeep.bean.OilPriceList
import com.app.baselibrary.ktx.resources
import com.app.baselibrary.utils.LogUtil
import com.jay.bookkeep.bean.ImageData
import com.jay.bookkeep.bean.OilPriceData
import com.jay.bookkeep.bean.OilPriceItemData
import org.json.JSONObject

/**
@author: yangjie
@descruption: 数据仓库
*/
class OilPriceRepo(oilPriceViewModel: OilPriceViewModel) : BaseHttpRepo(oilPriceViewModel) {

    val toolsTitlesArray by lazy { resources.getStringArray(R.array.tab_title1) }

    val toolsNameArray by lazy { resources.getStringArray(R.array.tab_title2) }
    fun getOilPrice( shengfen:String,action: (OilPriceList)->Unit){
        lifecycleDialogHttpFlow(httpFlow = WallpaperHttpService.invoke(2).getOilPrice(shengfen))
            .collectMain({
               asString()
            },{
                val jsonObject= JSONObject(it)
                val innerList = mutableListOf<OilPriceData>()
                toolsTitlesArray.forEachIndexed { _, s ->
                    val pr=jsonObject.getString(s)
                    val innerList1 = mutableListOf<OilPriceItemData>()
                    val jsonObject1= JSONObject(pr)
                    toolsNameArray.forEachIndexed { _, ss ->
                        val price=jsonObject1.getDouble(ss)
                        innerList1.add(OilPriceItemData(ss,price))
                    }
                    if(shengfen.contains(s)){
                        innerList.add(0,OilPriceData(s,innerList1))
                    }else{
                        innerList.add(OilPriceData(s,innerList1))
                    }
                }
                action.invoke(OilPriceList(innerList))
            })
    }


}