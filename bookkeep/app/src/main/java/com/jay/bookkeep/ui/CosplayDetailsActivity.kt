package com.jay.bookkeep.ui
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.baselibrary.base.BaseActivity
import com.app.baselibrary.utils.StringUtil
import com.jay.bookkeep.adapter.GridImageAdapter
import com.jay.bookkeep.bean.CosplayItemData
import com.jay.bookkeep.databinding.ActivityCosplayDetailsBinding

/**
 * CosPlay详情
 */
class CosplayDetailsActivity: BaseActivity<ActivityCosplayDetailsBinding>(){

    private val mDetailsData by lazy { intent.getParcelableExtra<CosplayItemData>("details") }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        requireViewBinding().lyTabLayout.tvBaseTitle.text=mDetailsData!!.setname
        val webUrl=getProductBanner(mDetailsData!!.pics)
        requireViewBinding().webLayout.loadDataWithBaseURL(
            null, mDetailsData!!.desc+StringUtil.getHtmlData(webUrl), "text/html",
            "utf-8", null
        )

    }
    fun getProductBanner(data:List<String>): String {
        val stringBuffer = StringBuffer()
        data.forEachIndexed { _, s ->
            stringBuffer.append(StringUtil.generateHtml(s))
        }
        return stringBuffer.toString()
    }
}