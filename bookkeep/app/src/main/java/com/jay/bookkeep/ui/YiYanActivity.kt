package com.jay.bookkeep.ui
import android.os.Bundle
import android.text.Html
import com.app.baselibrary.base.BaseViewModelActivity
import com.jay.bookkeep.databinding.ActivityYiYanBinding
import com.jay.bookkeep.vm.WallpaperViewModel

/**
 * 每日一言详情
 */
class YiYanActivity: BaseViewModelActivity<ActivityYiYanBinding, WallpaperViewModel>(){

    private val mClassType by lazy { intent.getIntExtra("classType",0) }
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        if(mClassType==0){
            requireViewModel().getYiYan()
        }else if(mClassType==1){
            requireViewModel().getComfort()
        }else if(mClassType==2){
            requireViewModel().getTimeTx()
        }else if(mClassType==3){
            requireViewModel().getPyq()
        }else if(mClassType==4){
            requireViewModel().getTg()
        }
        initData()
    }

    private fun  initData(){
       requireViewModel().mYiYan.observerNotNull {
           requireViewBinding().tvContent.text= Html.fromHtml(it)
       }
       requireViewModel().mAnWeiData.observerNotNull {
           requireViewBinding().tvContent.text=it.anwei
       }
        requireViewModel().mTimeTxData.observerNotNull {
            requireViewBinding().tvContent.text=it.msg+"\n"+it.nxyj
        }
        requireViewModel().mPyqData.observerNotNull {
            requireViewBinding().tvContent.text=it.pyq
        }
    }
}