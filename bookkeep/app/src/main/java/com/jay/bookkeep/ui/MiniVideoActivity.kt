package com.jay.bookkeep.ui
import android.os.Bundle
import android.view.View
import com.app.baselibrary.base.BaseViewModelActivity
import com.jay.bookkeep.databinding.ActivityVideoBinding
import com.jay.bookkeep.vm.WallpaperViewModel
import com.shuyu.gsyvideoplayer.GSYVideoManager

/**
 * 小视频
 */
class MiniVideoActivity: BaseViewModelActivity<ActivityVideoBinding, WallpaperViewModel>(){

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        requireViewModel().getVideo()
        //隐藏自带的标题和返回键
        requireViewBinding().detailPlayer.titleTextView.visibility = View.GONE
        requireViewBinding().detailPlayer.backButton.visibility = View.GONE
        //隐藏全屏按键
        requireViewBinding().detailPlayer.fullscreenButton.visibility = View.GONE
        initData()
    }

    private fun  initData(){
        //是否根据视频尺寸，自动选择横竖屏全屏
        requireViewBinding().detailPlayer.isAutoFullWithSize=true
        //音频焦点冲突时是否释放
//        requireViewBinding().detailPlayer.isReleaseWhenLossAudio =true
        //全屏动画
//        requireViewBinding().detailPlayer.isShowFullAnimation=true
        requireViewModel().mVideoData.observerNotNull {
            requireViewBinding().detailPlayer.setUp("https:"+it.mp4, true, "title")
            requireViewBinding().detailPlayer.startPlayLogic()
        }
    }

    override fun onPause() {
        super.onPause()
        requireViewBinding().detailPlayer.onVideoPause()
//        GSYVideoManager.onPause()
    }
    override fun onResume() {
        super.onResume()
        requireViewBinding().detailPlayer.onVideoResume()
//        GSYVideoManager.onResume()
    }
    override fun onDestroy() {
        super.onDestroy()
        GSYVideoManager.releaseAllVideos()
    }

}