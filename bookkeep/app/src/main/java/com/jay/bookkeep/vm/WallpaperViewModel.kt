package com.jay.bookkeep.vm
import androidx.lifecycle.MutableLiveData
import com.app.baselibrary.base.BaseViewModel
import com.app.baselibrary.utils.LogUtil
import com.jay.bookkeep.bean.AnWeiData
import com.jay.bookkeep.bean.CosplayData
import com.jay.bookkeep.bean.CosplayItemData
import com.jay.bookkeep.bean.Image
import com.jay.bookkeep.bean.ImageData
import com.jay.bookkeep.bean.ImageListData
import com.jay.bookkeep.bean.PhoneImage
import com.jay.bookkeep.bean.PhoneImageData
import com.jay.bookkeep.bean.PhoneListData
import com.jay.bookkeep.bean.PyqData
import com.jay.bookkeep.bean.TimeTxData
import com.jay.bookkeep.bean.Vertical
import com.jay.bookkeep.bean.VideoData
import com.jay.bookkeep.bean.WelcomeData
import java.util.stream.Collectors
import java.util.stream.Stream

/**
@author: yangjie
@descruption: ViewModel
*/
class WallpaperViewModel : BaseViewModel() {    private val wallpaperPageRepo by lazy { WallpaperPageRepo(this) }

    private val _mDataEntity= MutableLiveData<WelcomeData>()

    val mDataEntity by lazy { _mDataEntity }

    private val _mPhoneImage= MutableLiveData<PhoneImage>()

    val mPhoneImage by lazy { _mPhoneImage }


    private val _mYiYan= MutableLiveData<String>()

    val mYiYan by lazy { _mYiYan }


    private val _mAnWeiData= MutableLiveData<AnWeiData>()

    val mAnWeiData by lazy { _mAnWeiData }

    private val _mTimeTxData= MutableLiveData<TimeTxData>()

    val mTimeTxData by lazy { _mTimeTxData }

    private val _mVideoData= MutableLiveData<VideoData>()

    val mVideoData by lazy { _mVideoData }

    private val _mPyqData= MutableLiveData<PyqData>()

    val mPyqData by lazy { _mPyqData }

    private val _mCosplayData=MutableLiveData<CosplayData>()

    val mCosplayData by lazy { _mCosplayData }

    fun bingying(idx:Int,size:Int) {
        wallpaperPageRepo.biying(idx,size) {
            _mDataEntity.postValue(it)
        }
    }


    fun getWallPaper(size:Int) {
        wallpaperPageRepo.getWallPaper(size) {
            _mPhoneImage.postValue(it)
        }
    }

    fun getPyq() {
        wallpaperPageRepo.getPyq {
            _mPyqData.postValue(it)
        }
    }
    fun getYiYan() {
        wallpaperPageRepo.getYiYan {
            _mYiYan.postValue(it)
        }
    }

    fun getTg() {
        wallpaperPageRepo.getTg {
            _mYiYan.postValue(it)
        }
    }
    fun getTimeTx() {
        wallpaperPageRepo.getTimeTx {
            _mTimeTxData.postValue(it)
        }
    }

    fun getComfort() {
        wallpaperPageRepo.getComfort {
            _mAnWeiData.postValue(it)
        }
    }

    fun getVideo() {
        wallpaperPageRepo.getVideo {
            _mVideoData.postValue(it)
        }
    }

    fun getCosplay(page:Int) {
        wallpaperPageRepo.getCosplay(page) {
            _mCosplayData.postValue(it)
        }
    }

    fun getImageList(images: List<Image>,mType:Int): ImageListData {
        val innerList = mutableListOf<ImageData>()
        images.forEachIndexed { index, image ->
            innerList.add(ImageData(image.url,image.copyright,image.title,mType))
        }
        return  ImageListData(innerList)
    }

    fun changeImageList(images: List<ImageData>,mType:Int): List<ImageData> {
        images.forEachIndexed { _, image ->
            image.mType=mType
        }
        return  images
    }


    fun getPhoneImageList(images: List<Vertical>, mType:Int): PhoneListData {
        val innerList = mutableListOf<PhoneImageData>()
        images.forEachIndexed { index, image ->
            innerList.add(PhoneImageData(image.img,image.preview,image.thumb,image.tag,mType))
        }
        return  PhoneListData(innerList)
    }

    fun changePhoneImageList(images: List<PhoneImageData>,mType:Int): List<PhoneImageData> {
        images.forEachIndexed { _, image ->
            image.mType=mType
        }
        return  images
    }


    fun getCosPlayList(newData: List<CosplayItemData>?=null,oldData: List<CosplayItemData>): List<CosplayItemData>{
        val innerList = mutableListOf<CosplayItemData>()
        innerList.addAll(oldData)
        newData?.forEachIndexed { _, cosplayItemData2 ->
            var isRepeatList=false
            oldData.forEachIndexed { _, cosplayItemData ->
                if(cosplayItemData2.setname==cosplayItemData.setname
                    &&cosplayItemData2.postid==cosplayItemData.postid){
                    isRepeatList=true
                }
            }
           if(!isRepeatList){
               innerList.add(cosplayItemData2)
           }
        }
        return  innerList
    }
}