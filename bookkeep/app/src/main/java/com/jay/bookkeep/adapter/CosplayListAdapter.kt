package com.jay.bookkeep.adapter
import android.app.Activity
import com.app.baselibrary.base.BaseAdapter
import com.app.baselibrary.base.BaseViewHolder
import com.app.baselibrary.utils.DensityUtils
import com.app.baselibrary.utils.GlideEngine
import com.jay.bookkeep.bean.CosplayItemData
import com.jay.bookkeep.databinding.ItemCosplayListBinding
/**
 * cosplay列表适配器
 */
class CosplayListAdapter(val activity: Activity) : BaseAdapter<CosplayItemData>(false){
    override fun bindItem(data: CosplayItemData, holder: BaseViewHolder, position: Int) {
        val viewBinding = ItemCosplayListBinding.bind(holder.itemView)
        //获取imageView的LayoutParams
        val layoutParams = viewBinding.ivWallpaper.layoutParams
        if(position%2==0){
            layoutParams.height = DensityUtils.dip2px(activity,300f)
        }else{
            layoutParams.height = DensityUtils.dip2px(activity,150f)
        }
        //重新设置imageView的高度
        viewBinding.ivWallpaper.layoutParams = layoutParams
        GlideEngine.getInstance().loadImage(activity,data.cover, viewBinding.ivWallpaper)
        viewBinding.tvTitle.text=data.setname
    }
}