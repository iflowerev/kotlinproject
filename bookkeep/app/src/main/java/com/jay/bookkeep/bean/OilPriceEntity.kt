package com.jay.bookkeep.bean
import com.app.baselibrary.base.DataType
import com.jay.bookkeep.R

/**
 * 油价
 */
data class OilPriceList(
    var mOilPriceData:List<OilPriceData>
)

data class OilPriceData(
    var cityName: String,
    var mData: List<OilPriceItemData>,
): DataType {

    override fun dataType(): Int{
        return 1
    }
    override fun layoutId(): Int {
        return R.layout.item_oil_price
    }
}

data class OilPriceItemData(
    var title:  String,
    var price: Double,
): DataType{

    override fun dataType(): Int{
        return 1
    }
    override fun layoutId(): Int {
        return R.layout.item_oil_price_item
    }
}

