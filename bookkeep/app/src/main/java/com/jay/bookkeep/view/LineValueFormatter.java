package com.jay.bookkeep.view;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.List;

public class LineValueFormatter extends ValueFormatter {

    private final List<Float> mLabels;

    /**
     * 构造方法，把数据源传进来
     * @param labels
     */
    public LineValueFormatter(List<Float> labels) {
        mLabels = labels;
    }

//    @Override
//    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
//
//        //返回值为：29℃
//        return mLabels.get((int) entry.getX())+"℃";
//    }

    @Override
    public String getFormattedValue(float value) {
        return value+"℃";
    }


}