package com.jay.bookkeep
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Html
import com.amap.api.location.AMapLocation
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.runUI
import com.app.baselibrary.ktx.startCustomActivity
import com.app.baselibrary.utils.GlideEngine
import com.app.baselibrary.utils.LogUtil
import com.app.baselibrary.utils.PermissionUtils
import com.app.baselibrary.utils.SPUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import com.jay.bookkeep.databinding.ActivityWelcomePageBinding
import com.jay.bookkeep.utils.listener.LocationResultListener
import com.jay.bookkeep.utils.listener.MapLocationListener
import com.jay.bookkeep.vm.WallpaperViewModel

/**
 * 欢迎页
 */
class WelcomePageActivity : BaseViewModelActivity<ActivityWelcomePageBinding,WallpaperViewModel>(),LocationResultListener {

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        showPerDialog()
        val number = (Math.random() * 100).toInt()
        requireViewModel().bingying(number,1)
        initData()
    }
    private fun  initData(){
        requireViewModel().mDataEntity.observerNotNull {
            val mImage=it.images[0]
            val biyingUrl = "http://cn.bing.com" + mImage.url
            GlideEngine.getInstance().loadImage(this,biyingUrl, requireViewBinding().ivPage)
            requireViewModel().getYiYan()
        }
        requireViewModel().mYiYan.observerNotNull {
            requireViewBinding().tvTitle.text= Html.fromHtml(it)
        }
        runUI(  3000){
            startCustomActivity<MainActivity>()
            finish()
        }
    }

    private fun showPerDialog(){
        PermissionUtils.getInstance().requsetLocationPermission(this,object :
            PermissionUtils.ActivityResultData{
            override fun onDataesult(code: Int){
                if(code==200){
                    //进行定位
                    MapLocationListener.getInstance().setLocationResultListener(this@WelcomePageActivity)
                    MapLocationListener.getInstance().startLocation()
                }
            }
        })

    }


    override fun callBackLocation(location: AMapLocation?, code: Int) {
        if (code == 0) {
            //定位成功
            MyApplication.setMyMapLocation(location!!)
            MapLocationListener.getInstance().stopLocation()
            LogUtil.logE(location!!.city+"----------"+location!!.description)
        }else{
            //定位失败
            LogUtil.logE("定位失败了----------")
        }
    }

}