package com.jay.bookkeep.bean

/**
 * 天气
 */
data class WeatherDataEntity(
    val code: String,
    val `data`: List<Data>
)

data class Data(
    val fengdu: String,
    val pm: String,
    val riqi: String,
    val tianqi: String,
    val wendu: String
)
