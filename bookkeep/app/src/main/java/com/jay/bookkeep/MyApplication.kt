package com.jay.bookkeep
import com.app.baselibrary.BaseApplication
import com.amap.api.location.AMapLocation
import com.amap.api.location.AMapLocationClient
import com.app.baselibrary.ktx.APPLICATION
import com.jay.bookkeep.utils.listener.MapLocationListener

/**
@author: yangjie
 */
class MyApplication : BaseApplication(){

    companion object {

        private val instance = MyApplication()
        /**
         * 获取单例
         */
        fun getInstance() = instance


        //定位数据
        private var mapLocation: AMapLocation? = null

        fun getMyMapLocation(): AMapLocation? {
            return mapLocation
        }

        fun setMyMapLocation(myMapLocation: AMapLocation) {
            mapLocation = myMapLocation
        }
    }

    override fun onCreate() {
        super.onCreate()
        getLocation()
    }

    /**
     * 初始化定位以及高德地图定位同意协议
     */
    private fun  getLocation(){
        AMapLocationClient.updatePrivacyShow(APPLICATION,true,true)
        AMapLocationClient.updatePrivacyAgree(APPLICATION,true)
        MapLocationListener.getInstance().initMLocation()
    }

}