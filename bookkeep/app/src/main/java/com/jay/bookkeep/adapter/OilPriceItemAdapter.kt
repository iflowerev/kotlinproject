package com.jay.bookkeep.adapter
import com.app.baselibrary.base.BaseAdapter
import com.app.baselibrary.base.BaseViewHolder
import com.jay.bookkeep.bean.OilPriceItemData
import com.jay.bookkeep.databinding.ItemOilPriceItemBinding
/**
 * 油价列表适配器
 */
class OilPriceItemAdapter() : BaseAdapter<OilPriceItemData>(false){
    override fun bindItem(data: OilPriceItemData, holder: BaseViewHolder, position: Int) {
        val viewBinding = ItemOilPriceItemBinding.bind(holder.itemView)
        if(data.title=="0号柴油"){
            viewBinding.tvName.text="\u0020\u0020"+"${data.title}"
        }else{
            viewBinding.tvName.text="${data.title}"
        }
        val price=data.price.toString().split(".")
        viewBinding.tvPrice1.text="${price[0]}"
        if(price.size>1){
            val price1=price[1].toCharArray()
            if(price1.size>1){
                viewBinding.tvPrice2.text="${price1[0]}"
                viewBinding.tvPrice3.text="${price1[1]}"
            }else{
                viewBinding.tvPrice2.text="${price1[0]}"
                viewBinding.tvPrice3.text="0"
            }
        }else{
            viewBinding.tvPrice2.text="0"
            viewBinding.tvPrice3.text="0"
        }


    }

}