package com.jay.bookkeep.ui
import android.app.WallpaperManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.viewpager2.widget.ViewPager2
import com.app.baselibrary.base.BaseViewModelActivity
import com.app.baselibrary.ktx.onLongClick
import com.app.baselibrary.ktx.shortToast
import com.app.baselibrary.utils.GlideEngine
import com.app.baselibrary.utils.ImageBitampListener
import com.app.baselibrary.utils.ImageUtil
import com.app.baselibrary.utils.PermissionUtils
import com.jay.bookkeep.adapter.PhoneImagerAdapter
import com.jay.bookkeep.bean.PhoneImageData
import com.jay.bookkeep.bean.PhoneListData
import com.jay.bookkeep.databinding.ActivityImageBinding
import com.jay.bookkeep.vm.WallpaperViewModel
import java.io.IOException

/**
 * 壁纸预览
 */
class PhoneWallpaperListActivity : BaseViewModelActivity<ActivityImageBinding,WallpaperViewModel>() {

    private val mAdapter by lazy { PhoneImagerAdapter(this@PhoneWallpaperListActivity) }


    private val mPosition by lazy { intent.getIntExtra("position",0) }


    private val mImageData by lazy { intent.getParcelableExtra<PhoneListData>("image") }


    var wallpaperUrl: String? = null

    private var bitmap: Bitmap? = null

    private var mData:List<PhoneImageData>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PermissionUtils.getInstance().requestStargePermissions(this,object :
            PermissionUtils.ActivityResultData{
            override fun onDataesult(code: Int) {
            }
        })
        wallpaperUrl = mImageData!!.mData[mPosition].preview
        GlideEngine.getInstance().getBitmapImage(this,wallpaperUrl!!,object : ImageBitampListener {
            override fun bitmapCallBack(mBitamp: Bitmap?) {
                bitmap=mBitamp
            }
        })
         requireViewBinding().vp.adapter=mAdapter
         requireViewBinding().vp.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                wallpaperUrl = mImageData!!.mData[position].preview
                GlideEngine.getInstance().getBitmapImage(this@PhoneWallpaperListActivity,wallpaperUrl!!,object : ImageBitampListener {
                    override fun bitmapCallBack(mBitamp: Bitmap?) {
                        bitmap=mBitamp
                    }
                })
            }
        })
        mData=requireViewModel().changePhoneImageList(mImageData!!.mData,1)
        mAdapter.setData(mData!!)
        requireViewBinding().vp.setCurrentItem(mPosition,false)
        initEvent()
    }

    private fun  initEvent(){
        requireViewBinding().btnSettingWallpaper.onLongClick {
            //设置壁纸
            if(bitmap!=null){
              showProgress()
              SetWallPaper()
             }else{
              shortToast("bitmap不能为null")
            }
        }
        requireViewBinding().btnDownload.onLongClick {
            //下载壁纸
            if(bitmap!=null){
                showProgress()
                ImageUtil.savePictures(context, bitmap!!)
                shortToast("壁纸保存成功")
                hideProgress()
            }else{
                shortToast("bitmap不能为null")
            }
        }
    }

    //设置壁纸
    fun SetWallPaper(){
        val mWallManager = WallpaperManager.getInstance(this)
        try {
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val height = metrics.heightPixels
            val width = metrics.widthPixels
//           if (ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.READ_EXTERNAL_STORAGE
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//               shortToast("请打开存储权限")
//                return
//            } else {
//               // 获取当前壁纸（非缩略图）
//               val drawable=mWallManager.drawable
//           }
            // 获取当前壁纸的偏移量
//            val xOffset = mWallManager.desiredMinimumWidth
//            val yOffset = mWallManager.desiredMinimumHeight
            // 获取当前壁纸的尺寸
//            val width = mWallManager.desiredMinimumWidth
//            val height = mWallManager.desiredMinimumHeight
//            LogUtil.logE("$width---------$height")
//            val newBitmap = Bitmap.createScaledBitmap(bitmap!!, width, height, true)
            mWallManager.setBitmap(bitmap!!)
            shortToast("桌面壁纸设置成功")
        } catch (e: IOException) {
            e.printStackTrace()
        }
        hideProgress()
    }

}