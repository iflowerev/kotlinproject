package com.jay.bookkeep.adapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baselibrary.utils.DensityUtils;
import com.app.baselibrary.utils.GlideEngine;
import com.app.baselibrary.utils.LogUtil;
import com.jay.bookkeep.R;
import java.util.List;

/**
 * @description: 图片加载$
 */
public class GridImageAdapter extends RecyclerView.Adapter<GridImageAdapter.ViewHolder>{
    private LayoutInflater mInflater;
    private List<String> mList;
    private Activity mContext;
    public GridImageAdapter(Activity context, List<String> list) {
        this.mContext=context;
        this.mInflater = LayoutInflater.from(context);
        this.mList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_report_image, null);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


        @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.mImg.getLayoutParams();
////        params.height = DensityUtils.getScreenWidth(mContext)/5;
//        params.height = DensityUtils.dp2px(mContext,100);
//        params.height=DensityUtils.dp2px(mContext,200);
        holder.mImg.setLayoutParams( new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
       GlideEngine.Companion.getInstance().loadImage(mContext,mList.get(position), holder.mImg);
        holder.mImg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //查看大图

            }
        });
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImg;
        public ViewHolder(View view) {
            super(view);
            mImg = view.findViewById(R.id.iv_imag);
        }
    }
}
