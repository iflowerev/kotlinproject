package com.jay.bookkeep.vm
import androidx.lifecycle.MutableLiveData
import com.app.baselibrary.base.BaseViewModel
import com.jay.bookkeep.bean.OilPriceList

/**
@author: yangjie
@descruption: ViewModel
*/
class OilPriceViewModel : BaseViewModel() {

    private val _mOilPriceList= MutableLiveData<OilPriceList>()

    val mOilPriceList by lazy { _mOilPriceList }

    private val oilPriceRepo by lazy { OilPriceRepo(this) }

    fun getOilPrice(shengfen:String) {
        oilPriceRepo.getOilPrice(shengfen) {
            _mOilPriceList.postValue(it)
        }
    }


}