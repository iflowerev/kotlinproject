plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.app.baselibrary"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures.viewBinding = true
}

dependencies {
    //新添加的代码
    api ("androidx.recyclerview:recyclerview:1.3.0")
    // ViewModel
    api ("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.1")
    // LiveData
    api ("androidx.lifecycle:lifecycle-livedata-ktx:2.6.1")
    api ("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
    api ("androidx.lifecycle:lifecycle-viewmodel-savedstate:2.6.1")
    api ("androidx.lifecycle:lifecycle-common-java8:2.6.1")
    api ("androidx.lifecycle:lifecycle-process:2.6.1")
    //gson
    api ( "com.google.code.gson:gson:2.10.1")
    api ("com.squareup.retrofit2:converter-gson:2.9.0")
    api ("com.squareup.retrofit2:retrofit:2.9.0")
    api ("com.squareup.retrofit2:adapter-rxjava2:2.9.0")
    //网络请求
    api ("com.squareup.okhttp3:okhttp:4.9.1")
    api ("com.squareup.okhttp3:logging-interceptor:4.9.1")
    //图片加载
    api ("com.github.bumptech.glide:glide:4.12.0")
    annotationProcessor ("com.github.bumptech.glide:compiler:4.11.0")
    //https图片处理
    api ("com.github.bumptech.glide:okhttp3-integration:4.11.0")
    implementation("androidx.core:core-ktx:1.10.1")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.8.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    api  ("io.github.scwang90:refresh-layout-kernel:2.0.6")      //核心必须依赖
    api  ("io.github.scwang90:refresh-header-material:2.0.6")    //谷歌刷新头
    api  ("io.github.scwang90:refresh-footer-ball:2.0.6")     //球脉冲加载
    //权限
    api ("com.github.getActivity:XXPermissions:16.6")
}