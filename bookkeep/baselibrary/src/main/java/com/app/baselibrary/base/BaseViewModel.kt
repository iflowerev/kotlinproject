package com.app.baselibrary.base
import androidx.lifecycle.ViewModel
import com.app.baselibrary.delegate.ViewModelStatusOwner
import com.app.baselibrary.delegate.viewModelOwner
import java.io.Closeable
import kotlin.collections.ArrayList

/**
 * 基类
 */
open class BaseViewModel : ViewModel(), ViewModelStatusOwner by viewModelOwner() {

    private val tags by lazy { ArrayList<Any>() }


    fun setTagIfAbsent(tag: Any){
        if (tags.contains(tag)){
            return
        }
        tags.add(tag)
    }


    override fun onCleared() {
        super.onCleared()
        if (tags.isEmpty()){
            return
        }
        val iterable = tags.iterator()
        while (iterable.hasNext()){
            val tag = iterable.next()
            if (tag is Closeable){
                tag.close()
            }
            iterable.remove()
        }
    }

    /**
     * 回调数据
     */
    interface NetStatusResult{

         fun onNetResult(isNetErrow: Boolean)

         fun onServiceResult(isServiceErrow: Boolean)

    }

    interface ActivityResultData{
        fun onDataesult(name: String)
    }

}

