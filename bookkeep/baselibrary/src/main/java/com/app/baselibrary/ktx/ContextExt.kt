package com.app.baselibrary.ktx
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.app.baselibrary.base.BaseActivity
import com.app.baselibrary.base.BaseFragment


/**
 * 启动页面
 */
inline fun <reified T: BaseActivity<*>> BaseActivity<*>.startCustomActivity(bundle: Bundle = Bundle.EMPTY, requestCode: Int = -1){
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundle)
    if (requestCode != -1){
        startActivityForResult(intent, requestCode)
    }else{
        startActivity(intent)
    }
}
inline fun <reified T: BaseActivity<*>> BaseActivity<*>.startCustomActivity(){
    val intent = Intent(this, T::class.java)
    startActivity(intent)
}

/**
 * 启动页面
 */
inline fun <reified T: BaseActivity<*>>Context.startCustomActivity(bundle: Bundle= Bundle.EMPTY){
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundle)
    startActivity(intent)
}

inline fun <reified T: BaseActivity<*>>Context.startCustomActivity(activity: Activity, bundle: Bundle= Bundle.EMPTY, requestCode: Int = -1){
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundle)
    if (requestCode != -1){
        activity.startActivityForResult(intent, requestCode)
    }else{
        startActivity(intent)
    }
}


/**
 * 启动页面
 */
inline fun <reified T: BaseActivity<*>> BaseFragment<*>.startCustomActivity(bundle: Bundle = Bundle.EMPTY, requestCode: Int = -1){
    val intent = Intent(requireContext(), T::class.java)
    intent.putExtras(bundle)
    if (requestCode != -1){
        startActivityForResult(intent, requestCode)
    }else{
        startActivity(intent)
    }
}