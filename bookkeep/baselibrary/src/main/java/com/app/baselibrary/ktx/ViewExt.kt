@file:JvmName("ViewExt")
package com.app.baselibrary.ktx
import android.util.Log
import android.view.View
import java.util.*


private var lastClickTimestamp = 0L

/**
 * 按钮连点间隔
 */
private const val DELAY_TIME = 100L


private const val LONG_TIME = 500L
/**
 * 控件防连点
 */
fun <V: View> V.onClick(action: V.(V)->Unit){
    this.setOnClickListener {
        val date = Date().time
        val offset = date - lastClickTimestamp
        lastClickTimestamp = date
        if (offset > DELAY_TIME){
            action.invoke(this,this)
        }else{
            Log.e("ViewExt", "当前按钮不能连续点击。 间隔时长：$offset  $this")
        }
    }
}

fun <V: View> V.onLongClick(action: V.(V)->Unit){
    this.setOnClickListener {
        val date = Date().time
        val offset = date - lastClickTimestamp
        lastClickTimestamp = date
        if (offset > LONG_TIME){
            action.invoke(this,this)
        }else{
            Log.e("ViewExt", "当前按钮不能连续点击。 间隔时长：$offset  $this")
        }
    }
}