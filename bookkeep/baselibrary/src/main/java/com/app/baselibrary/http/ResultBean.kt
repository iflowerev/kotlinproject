package com.app.baselibrary.http

/**
 * 请求结果
 */
data class ResultBean(var status: String,
                      var message: String? =null,
                      var data: Any? =null){

    /**
     * 是否请求成功
     */
    fun isSuccessful() = status == "200"


    fun getResultData(): Any {
        return data?:""
    }
}