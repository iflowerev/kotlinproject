package com.app.baselibrary.http
import android.accounts.NetworkErrorException
import com.app.baselibrary.http.flow.HttpFlow
import com.google.gson.JsonSyntaxException
import java.io.IOException
import java.io.InterruptedIOException
import java.net.ConnectException
import java.net.SocketTimeoutException

/**
 * 异常转换器
 */
class HttpExceptionConvert : HttpFlow.ExceptionConvert<HttpResultException>{
    init {

    }
    override fun convert(exception: Throwable): HttpResultException {
        return when (exception) {
            is NetworkErrorException -> {
                HttpResultException.NET_ERROR
            }
            is SocketTimeoutException ->{
                HttpResultException.SERVICE_TIMEOUT_ERROR
            }
            is ConnectException ->{
                HttpResultException.CONNECT_ERROR
            }
            is InterruptedIOException ->{
                HttpResultException.INTERRUPTEDIO_ERROR
            }
            is HttpFlow.HttpResponseException -> {
                convertHttpException(exception)
            }
            is JsonSyntaxException -> {
                HttpResultException.DATA_ERROR
            }
            is HttpFlow.HttpResultDataError -> {
                dispatcherResultError(exception)
            }
            is IOException -> {
                HttpResultException.SERVICE_ERROR
            }
            else -> {
                exception.printStackTrace()
                HttpResultException(-3, "未知异常: $exception")
            }
        }
    }


    /**
     * 转换http异常
     */
    private fun convertHttpException(exception: HttpFlow.HttpResponseException): HttpResultException {
        val code = exception.response.code
        exception.printStackTrace()
        return if(code==401){
            HttpResultException(code, "用户未登录")
        }else if (code < 500){
            HttpResultException(code, "网络请求错误")
        }else {
            HttpResultException.SERVICE_ERROR
        }
    }


    /**
     * 分发数据错误码
     */
    private fun dispatcherResultError(exception: HttpFlow.HttpResultDataError): HttpResultException {
        val resultBean = exception.responseBean
        return if(resultBean.isSuccessful()&&resultBean.data!=null&&resultBean.data.toString().isNotEmpty()){
            HttpResultException(200, "数据为空不处理")
        }else{
            HttpResultException(resultBean.status.toInt(), resultBean.message ?: "出错了")
        }
    }

}

class HttpResultException (val code: Int, override val message: String) : Throwable(message){
    companion object {

        val NET_ERROR = HttpResultException(-1, "当前网络不可用~")

        val SERVICE_ERROR = HttpResultException(500, "服务器异常")

        val SERVICE_TIMEOUT_ERROR = HttpResultException(600, "服务连接超时")

        val CONNECT_ERROR = HttpResultException(700, "网络连接异常")

        val INTERRUPTEDIO_ERROR = HttpResultException(800, "网络连接异常")

        val DATA_ERROR = HttpResultException(-2, "数据解析异常")

        val NO_DATA = HttpResultException(200, "数据为空不处理")
    }

}