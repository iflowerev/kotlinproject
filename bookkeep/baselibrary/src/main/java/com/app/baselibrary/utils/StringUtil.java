package com.app.baselibrary.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;

import java.io.File;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @description: $
 * @author: 杨杰
 * @date: 2021/1/20
 */
public class StringUtil {


    /**
     * 获取图片地址
     * @param list
     * @return
     */
    public static List<String> getImageList(String list){
        List<String> mlist = new ArrayList<>();
        if(list.contains(",")) {
            String [] imageList=list.split(",");
            for(String s:imageList){
                mlist.add(s);
            }
        }else{
            mlist.add(list);
        }
        return mlist;
    }
    public static List<String> getImageList(String list,String videoUrl){
        List<String> mlist = new ArrayList<>();
        mlist.add(videoUrl);
        if(list.contains(",")) {
            String [] imageList=list.split(",");
            for(String s:imageList){
                mlist.add(s);
            }
        }else{
            mlist.add(list);
        }
        return mlist;
    }



    /**
     * 判断字符串中是否包含中文
     * @param str
     * 待校验字符串
     * @return 是否为中文
     * @warn 不能校验是否为中文标点符号
     */
    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    public static MultipartBody filesToMultipartBody(Context context, String url) {
        if(!url.contains(".png")&&!url.contains(".jpg")&&!url.contains(".jpeg")&&!url.contains(".mp4")){
            url=getRealPathFromUri(context, Uri.parse(url));
        }
        File files=new File(url);
        if(files.exists()){
            //文件存在
            MultipartBody.Builder builder = new MultipartBody.Builder();
            // TODO: 16-4-2  这里为了简单起见，没有判断file的类型
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), files);
            builder.addFormDataPart("localFile", files.getName(), requestBody);
            builder.setType(MultipartBody.FORM);
            MultipartBody multipartBody = builder.build();
            return multipartBody;
        }else{
            //文件不存在
            return null;
        }
    }
    /**
     * 解决无图片格式地址转成有格式图片地址
     * @param context
     * @param contentUri
     * @return
     */
    private static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static String generateHtml(String imgUrl){
        StringBuffer stringBuffer=new StringBuffer("<p style=\"text-align: center;\">");
        stringBuffer.append("<img src=\"");
        stringBuffer.append(imgUrl);
        stringBuffer.append("\"/></p>");
        return  stringBuffer.toString();
    }
    /**
     * html代码
     * @param bodyHTML
     * @return
     */
    public static String getHtmlData(String bodyHTML){
        String head = "<head>" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> " +
                "<style>img{max-width: 100%; width:auto; height:auto;}</style>" +
                "</head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }

    /**
     * 判断Activity是否Destroy
     * @param mActivity
     * @return true:已销毁
     */
    public static boolean isDestroy(Activity mActivity){
        if (mActivity == null || mActivity.isFinishing() ||
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && mActivity.isDestroyed())) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 描述：获取表示当前日期时间的字符串.
     *
     * @param format 格式化字符串，如："yyyy-MM-dd HH:mm:ss"
     * @return String String类型的当前日期时间
     */
    public static String getCurrentDate(String format) {
        String curDateTime = null;
        try {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
            Calendar c = new GregorianCalendar();
            curDateTime = mSimpleDateFormat.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curDateTime;

    }
    /**
     * 获取今天是星期几
     */
    public  static String getToDayWeek(){
        return  getWeekNumber(getCurrentDate("yyyy-MM-dd"),"yyyy-MM-dd");
    }
    /**
     * 取指定日期为星期几
     *
     * @param strDate  指定日期
     * @param inFormat 指定日期格式 yyyy-MM-dd
     * @return String   星期几
     */
    public static String getWeekNumber(String strDate, String inFormat){
        String week = "周日";
        Calendar calendar = new GregorianCalendar();
        DateFormat df = new SimpleDateFormat(inFormat);
        try {
            calendar.setTime(df.parse(strDate));
        } catch (Exception e) {
            return "错误";
        }
        int intTemp = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        switch (intTemp) {
            case 0:
//                week = "星期日";
                week = "周日";
                break;
            case 1:
//                week = "星期一";
                week = "周一";
                break;
            case 2:
//                week = "星期二";
                week = "周二";
                break;
            case 3:
//                week = "星期三";
                week = "周三";
                break;
            case 4:
//                week = "星期四";
                week = "周四";
                break;
            case 5:
//                week = "星期五";
                week = "周五";
                break;
            case 6:
//                week = "星期六";
                week = "周六";
                break;
        }
        return week;
    }

}
