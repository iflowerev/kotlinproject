package com.app.baselibrary.ktx

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/**
 * 返回的数据可以为空
 */
fun <T> LiveData<T?>.observerNullable(owner: LifecycleOwner, action: (T?) -> Unit){
    runUI {
        this@observerNullable.observe(owner, Observer { action.invoke(it) })
    }
}

/**
 * 返回的数据可以为不为空
 */
fun <T> LiveData<T>.observerNotNull(owner: LifecycleOwner, action: (T) ->Unit){
    runUI {
        this@observerNotNull.observe(owner, Observer {
            if (it != null){
                action.invoke(it)
            }
        })
    }
}