package com.app.baselibrary.base
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.IBinder
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.app.baselibrary.delegate.LifecycleComponent
import com.app.baselibrary.delegate.lifecycleComponent
import com.app.baselibrary.utils.HiStatusBar


/**
 * Activity 基类
 */
abstract class BaseActivity<V : ViewBinding> : AppCompatActivity(),
    LifecycleComponent<V> by lifecycleComponent() {

    var mSavedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSavedInstanceState = savedInstanceState
        window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        bind(this)
        HiStatusBar.setStatusBar(this, true, Color.TRANSPARENT, true)
        setContentView(requireViewBinding().root)
    }

    //点击空白处，键盘退出
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_DOWN) {
            val view = currentFocus
            if (isHideInput(view, ev)) {
                HideSoftInput(view!!.windowToken)
                view.clearFocus()
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    /**
     * 判定是否需要隐藏
     */
    private fun isHideInput(v: View?, ev: MotionEvent): Boolean {
        if (v != null && v is EditText) {
            val l = intArrayOf(0, 0)
            v.getLocationInWindow(l)
            val left = l[0]
            val top = l[1]
            val bottom = top + v.getHeight()
            val right = left + v.getWidth()
            return ev.x <= left || ev.x >= right || ev.y <= top || ev.y >= bottom
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        hideInput()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideInput()
    }

    /**
     * 隐藏键盘
     */
    private fun hideInput() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val v = window.peekDecorView()
        if (null != v) {
            imm.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    /**
     * 隐藏软键盘
     */
    private fun HideSoftInput(token: IBinder?) {
        if (token != null) {
            val manager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.fragments.forEach {
            it.onActivityResult(
                requestCode,
                resultCode,
                data
            )
        }
    }

    /**
     * 显示空布局或者网络异常布局
     */
    fun setShowEmpty(textView: TextView, content: String, resId: Int){
        textView.visibility = View.VISIBLE
        val drawable =resources.getDrawable(resId)
        drawable.setBounds(0, 0, drawable.minimumWidth, drawable.minimumHeight)
        textView.setCompoundDrawables(null, drawable, null, null)
        textView.text=content
    }
}