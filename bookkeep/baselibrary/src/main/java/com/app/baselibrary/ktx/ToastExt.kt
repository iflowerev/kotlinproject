package com.app.baselibrary.ktx
import android.widget.Toast
import androidx.annotation.StringRes

/**
 * Toast扩展类
 * @author thanatos [contact me](mailto:lxf523028638@outlook.com)
 * @since 2020/9/30 2:00 PM
 * @version 1.0
 */


fun showDialog(){

}

fun shortToast(@StringRes msgId: Int){
    toast(getString(msgId), Toast.LENGTH_SHORT)
}

fun  canceToast(){
    ToastUtil.canceToast()
}
fun shortToast(msg: String){
    if(msg.isNotEmpty()){
        toast(msg, Toast.LENGTH_SHORT)
    }
}

fun longToast(@StringRes msgId: Int){
    toast(getString(msgId), Toast.LENGTH_LONG)
}

fun longToast(msg: String){
    toast(msg, Toast.LENGTH_LONG)
}

private fun toast(msg: String, duration: Int){
//    Toast.makeText(LAN_CHUANG_APPLICATION, msg, duration).show()
    ToastUtil.show(msg, duration)
}