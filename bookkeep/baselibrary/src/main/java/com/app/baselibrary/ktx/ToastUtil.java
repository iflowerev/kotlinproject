package com.app.baselibrary.ktx;

import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.StringRes;

/**
 * @description: $
 * @author: 杨杰
 * @date: 2020/12/28
 */
public class ToastUtil {
    private static Toast toast;

    public static void show(String text,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "",LENGTH);
            //这个地方第二个参数需要为null
            toast.setText(text);
        } else {
            toast.cancel();
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(),"", LENGTH);
            toast.setText(text);
        }
        toast.show();
    }


    /**
     * 取消
     */
    public static void canceToast(){
        if (toast != null) {
            toast.cancel();
            toast=null;
        }
    }
    public static void show(@StringRes int resId,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "", LENGTH);
            toast.setText(resId);
        } else {
            toast.cancel();
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(),"", LENGTH);
            toast.setText(resId);
        }
        toast.show();
    }

    /**
     * 弹出多个toast时, 不会一个一个的弹, 后面一个要显示的内容直接显示在当前的toast上
     */
    public static void single(String msg,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "",LENGTH);
            toast.setText(msg);
        } else {
            toast.setText(msg);
        }
        toast.show();
    }

    public static void singleLong(String msg,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "",LENGTH);
            toast.setText(msg);
        } else {
            toast.setText(msg);
        }
        toast.show();
    }

    /**
     * 多行居中显示
     */
    public static void singleCenter(@StringRes int msg,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "", LENGTH);
            toast.setText(msg);
        } else {
            toast.setText(msg);
        }
        ((TextView) toast.getView().findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
        toast.show();
    }

    /**
     * 多行居中显示
     */
    public static void singleCenter(String msg,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "",LENGTH);
            toast.setText(msg);
        } else {
            toast.setText(msg);
        }
        ((TextView) toast.getView().findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
        toast.show();
    }

    /**
     * 弹出多个toast时, 不会一个一个的弹, 后面一个要显示的内容直接显示在当前的toast上
     */
    public static void single(@StringRes int msg,int LENGTH) {
        if (toast == null) {
            toast = Toast.makeText(AppResourceExt.getAPPLICATION(), "", LENGTH);
            toast.setText(msg);
        } else {
            toast.setText(msg);
        }
        toast.show();
    }
}
