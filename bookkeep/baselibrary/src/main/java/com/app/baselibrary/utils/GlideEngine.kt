package com.app.baselibrary.utils

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.app.baselibrary.utils.ActivityCompatHelper.assertValidRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition

/**
 * @describe：Glide加载引擎
 */
class GlideEngine {
    companion object {

        private val instance = GlideEngine()
        /**
         * 获取单例
         */
        fun getInstance() = instance
    }

    /**
     * 加载图片
     * @param context   上下文
     * @param url       资源url
     * @param imageView 图片承载控件
     */
    fun loadImage(context: Context?, url: String?, imageView: ImageView?) {
        if (!assertValidRequest(context)) {
            return
        }
        Glide.with(context!!)
            .load(url)
            .into(imageView!!)
    }

    fun loadImage(
        context: Context?,
        imageView: ImageView?,
        url: String?,
        maxWidth: Int,
        maxHeight: Int
    ) {
        if (!assertValidRequest(context)) {
            return
        }
        Glide.with(context!!)
            .load(url)
            .override(maxWidth, maxHeight)
            .into(imageView!!)
    }

    /**
     * 加载相册目录封面
     *
     * @param context   上下文
     * @param url       图片路径
     * @param imageView 承载图片ImageView
     */
    fun loadAlbumCover(context: Context?, url: String?, imageView: ImageView?) {
        if (!assertValidRequest(context)) {
            return
        }
        Glide.with(context!!)
            .asBitmap()
            .load(url)
            .override(180, 180)
            .sizeMultiplier(0.5f)
            .transform(
                CenterCrop(),
                RoundedCorners(8)
            ) //                .placeholder(R.drawable.ps_image_placeholder)
            .into(imageView!!)
    }

    /**
     * 带弧度的图片
     * @param context
     * @param url
     * @param imageView
     */
    fun loadRoundImage(context: Context, url: String?, imageView: ImageView) {
        if (!assertValidRequest(context)) {
            return
        }
        //        int wHight=(int)DensityUtils.dp2px(context,90f);
        Glide.with(context)
            .asBitmap()
            .load(url) //               .override(wHight, wHight)
            .centerCrop() //                .sizeMultiplier(0.5f)
            //                .apply(new RequestOptions().placeholder(R.drawable.ps_image_placeholder))
            .into(object : BitmapImageViewTarget(imageView) {
                override fun setResource(resource: Bitmap?) {
                    val circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.resources, resource)
                    circularBitmapDrawable.cornerRadius = 15f
                    imageView.setImageDrawable(circularBitmapDrawable)
                }
            })
    }

    /**
     * 加载图片列表图片
     *
     * @param context   上下文
     * @param url       图片路径
     * @param imageView 承载图片ImageView
     */
    fun loadGridImage(context: Context?, url: String?, imageView: ImageView?) {
        if (!assertValidRequest(context)) {
            return
        }
        Glide.with(context!!)
            .load(url)
            .override(200, 200)
            .centerCrop() //                .placeholder(R.drawable.ps_image_placeholder)
            .into(imageView!!)
    }


    fun getBitmapImage(context: Context?, url: String?, bitmapListener: ImageBitampListener) {
        if (!assertValidRequest(context)) {
            return
        }
        Glide.with(context!!).asBitmap().load(url).listener(object : RequestListener<Bitmap>{
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Bitmap>?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

            override fun onResourceReady(
                resource: Bitmap?,
                model: Any?,
                target: Target<Bitmap>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                bitmapListener.bitmapCallBack(resource)
                return false
            }
        }).submit()
    }


    fun getBitmapImageTwo(context: Context?, url: String?, bitmapListener: ImageBitampListener) {
        if (!assertValidRequest(context)) {
            return
        }
        val myWidth = Target.SIZE_ORIGINAL
        val myHeight = Target.SIZE_ORIGINAL
        Glide.with(context!!).asBitmap().load(url).into(object : SimpleTarget<Bitmap>(myWidth, myHeight){
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                bitmapListener.bitmapCallBack(resource)
            }

        })
    }
}

interface ImageBitampListener {
    fun bitmapCallBack(mBitamp: Bitmap?)
}