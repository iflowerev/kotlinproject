package com.app.baselibrary.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.app.baselibrary.delegate.ViewModelComponent
import com.app.baselibrary.delegate.viewModelComponent

/**
 * aac fragment 基类
 */
abstract class BaseViewModelFragment<V: ViewBinding, M: BaseViewModel>: BaseFragment<V>(),
    ViewModelComponent<M> by viewModelComponent<M>() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        bindViewModelComponent(this)
        return view
    }
}