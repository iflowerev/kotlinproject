package com.app.baselibrary.base

import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.app.baselibrary.delegate.ViewModelComponent
import com.app.baselibrary.delegate.viewModelComponent

/**
 * aac activity 基类
 */
abstract class BaseViewModelActivity<V: ViewBinding, M: BaseViewModel> : BaseActivity<V>(),
    ViewModelComponent<M> by viewModelComponent() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindViewModelComponent(this)
    }
}