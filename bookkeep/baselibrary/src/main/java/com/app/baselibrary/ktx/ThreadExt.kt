package com.app.baselibrary.ktx

import com.app.baselibrary.utils.ThreadUtil


/**
 * 运行在主线程
 * @param runnable 线程环境切换回调
 * @param delay 延迟执行时间
 */
fun runUI(delay: Long = 0, runnable: ()->Unit){
    ThreadUtil.runUI(Runnable(runnable), delay)
}
/**
 * 运行在工作线程，子线程
 * @param runnable 线程环境切换回调
 */
fun runWorkThread(runnable: ()->Unit){
    ThreadUtil.runWorkThread(Runnable(runnable))
}

/**
 * 运行在IO线程
 * @param runnable 线程环境切换回调
 */
fun runIOThread(runnable: ()->Unit){
    ThreadUtil.runIOThread(Runnable(runnable))
}