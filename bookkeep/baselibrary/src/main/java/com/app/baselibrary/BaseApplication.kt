package com.app.baselibrary
import android.app.Application
import android.content.Context
import com.app.baselibrary.delegate.ApplicationDelegate
import com.app.baselibrary.delegate.applicationDelegate
import com.app.baselibrary.utils.NetworkUtil

/**
 * 项目初始化入口，上层业务必须继承自当前
 */

abstract class BaseApplication : Application() , ApplicationDelegate by applicationDelegate(){

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        this.attachApplication(this)
    }

    override fun onCreate() {
        super.onCreate()
        this.onDelegateCreate()
        NetworkUtil.init()
    }

}