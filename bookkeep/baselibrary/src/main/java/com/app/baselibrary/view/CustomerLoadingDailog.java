package com.app.baselibrary.view;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.app.baselibrary.R;

public class CustomerLoadingDailog extends Dialog{
    public CustomerLoadingDailog(Context context) {
        super(context);
    }

    public CustomerLoadingDailog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {
        private Context context;
        private boolean isCancelable = false;
        private boolean isCancelOutside = false;


        public Builder(Context context) {
            this.context = context;
        }


        /**
         * 设置是否可以按返回键取消
         *
         * @param isCancelable
         * @return
         */

        public Builder setCancelable(boolean isCancelable) {
            this.isCancelable = isCancelable;
            return this;
        }

        /**
         * 设置是否可以取消
         *
         * @param isCancelOutside
         * @return
         */
        public Builder setCancelOutside(boolean isCancelOutside) {
            this.isCancelOutside = isCancelOutside;
            return this;
        }

        public CustomerLoadingDailog create(String message){
            LayoutInflater inflater = LayoutInflater.from(context);
            View mView = inflater.inflate(R.layout.dialog_loading, null);
            CustomerLoadingDailog loadingDailog = new CustomerLoadingDailog(context, R.style.IosDialogStyle);
            TextView msgText = (TextView) mView.findViewById(R.id.tipTextView);
            if(message!=null&&!message.isEmpty()){
                msgText.setText(message);
            }
            loadingDailog.setContentView(mView);
            loadingDailog.setCancelable(isCancelable);
            loadingDailog.setCanceledOnTouchOutside(isCancelOutside);
            return loadingDailog;
        }
    }
}
