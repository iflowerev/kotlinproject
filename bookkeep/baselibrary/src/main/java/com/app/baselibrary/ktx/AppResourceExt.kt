@file:JvmName("AppResourceExt")
package com.app.baselibrary.ktx
import android.app.Application
import android.content.res.Resources
import android.os.Build
import androidx.annotation.CheckResult
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.app.baselibrary.BaseApplication
import com.app.baselibrary.delegate.ApplicationDelegateImpl


/**
 * 获取当前项目的[Application]
 */
val APPLICATION: BaseApplication by lazy { ApplicationDelegateImpl.baseApplication }

/**
 * 获取当前项目的[Resources]
 */
val resources: Resources by lazy { APPLICATION.resources }

/**
 * 当前程序前后台状态
 */
@CheckResult
fun isAppBackground() = APPLICATION.isAppBackground()


/**
 * 获取字符串资源
 */
fun getString(@StringRes resId: Int): String {
    return APPLICATION.getString(resId)
}

/**
 * 获取颜色资源
 */
fun getColor(@ColorRes colorId: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        APPLICATION.resources.getColor(colorId, APPLICATION.theme)
    } else {
        @Suppress("DEPRECATION")
        APPLICATION.resources.getColor(colorId)
    }
}