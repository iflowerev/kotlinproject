package com.app.baselibrary.delegate
import androidx.lifecycle.MutableLiveData
import com.app.baselibrary.R
import com.app.baselibrary.ktx.getString

/**
 * viewModel 状态管理器
 */
interface ViewModelStatusOwner {

    val progressLiveData: MutableLiveData<ProgressInfoBean>

    fun showProgress()

    /**
     * 显示加载框
     */
    fun showProgress(title: String)


    /**
     * 隐藏加载框
     */
    fun hideProgress()

}

internal class ContextViewModelStatusOwner :
    ViewModelStatusOwner {

    override val progressLiveData by lazy { MutableLiveData<ProgressInfoBean>() }

    override fun showProgress() {
        progressLiveData.postValue(
            ProgressInfoBean(
                true,
                getString(R.string.base_progress_dialog_title)
            )
        )
    }

    /**
     * 隐藏加载框
     */
    override fun hideProgress(){
        progressLiveData.postValue(
            ProgressInfoBean(
                false,
                ""
            )
        )
    }


    override fun showProgress(title: String) {
        progressLiveData.postValue(
            ProgressInfoBean(
                true,
                title
            )
        )
    }
}

/**
 * 加载进度框状态类
 */
data class ProgressInfoBean(val show: Boolean = false, val title: String)

/**
 * 代理方法
 */
internal fun viewModelOwner() =
    ContextViewModelStatusOwner()