package com.app.baselibrary.http
import okhttp3.Interceptor
import okhttp3.Response
/**
 * 请求头认证添加
 */
class AuthHeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val token= ""
        val newRequest=request.newBuilder().addHeader("token",token).build()
         return chain.proceed(newRequest)
    }
}