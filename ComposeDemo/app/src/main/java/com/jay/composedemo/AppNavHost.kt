package com.jay.composedemo
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.jay.composedemo.setting.aboutScreen
import com.jay.composedemo.setting.navigateToAboutScreen
import com.jay.composedemo.setting.navigateToPrivacyScreen
import com.jay.composedemo.setting.navigateToSettingsGraph
import com.jay.composedemo.setting.privacyScreen
import com.jay.composedemo.setting.settingsGraph


@Composable
fun AppNavHost(navController: NavHostController){
        NavHost(
            navController = navController,
            startDestination = ROUTE_HOME_GRAPH){
            homeGraph(
                onSettingsClick = {  navController.navigateToSettingsGraph() },
                onPoemClick = {  },
                onPoemSentenceClick = {  },
                onChineseWisecrackClick = {  },
                onIdiomClick = { },
                onChineseColorClick = {  },
                nestGraph = {
                    settingsGraph(
                        onBackClick = navController::navigateUp,
                        onAboutClick = { navController.navigateToAboutScreen() },
                        onPrivacyClick = { navController.navigateToPrivacyScreen() },
                        nestGraph = {
                            aboutScreen(
                                onBackClick = navController::navigateUp
                            )
                            privacyScreen(
                                onBackClick = navController::navigateUp
                            )
                        }
                    )
                }
            )
        }
}