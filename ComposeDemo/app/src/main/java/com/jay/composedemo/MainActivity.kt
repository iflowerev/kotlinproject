package com.jay.composedemo
import android.annotation.SuppressLint
import android.icu.text.CaseMap.Title
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import com.jay.composedemo.ui.theme.ComposeDemoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val appNavController = rememberNavController()
            ComposeDemoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
//                    GreetingOne()
//                    LandingScreen()
                    AppNavHost(navController = appNavController)
                }
            }
        }
    }
}

/**
 * @Composable注解用于标记一个函数为可组合函数
 */
@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

/**
 * Compose 会在初始组合期间将由 remember 计算的值存储在组合内存中，并在重组期间返回存储的值。
 * remember 既可用于存储可变对象，又可用于存储不可变对象
 */
@SuppressLint("UnrememberedMutableState")
@Composable
fun GreetingOne() {
    val state=  remember { mutableStateOf("init") }
    Log.e("GreetingOne", "state:${state.value}")
    Column {
        Text(
            text = state.value,
            color= Color.Red,
            modifier = Modifier.fillMaxWidth()
        )
        Button(onClick = {state.value="Jetpack Compose"}) {
            Text(text = "点击更改文本")
        }
    }
   
}


/**
 * Column ：可以将多个项垂直地放置在屏幕上；
 * Row ：可以将多个项水平地放置在屏幕上；
 * Box ：可将元素放在其他元素上，还支持为其包含的元素配置特定的对齐方式。
 */







@Composable
private fun  LandingScreen(
    modifier: Modifier=Modifier
){
    Box(modifier = modifier.fillMaxWidth()){
        Text(
            text = "献给\n\n喜欢中华文字、文学、文化的人",
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.titleLarge,
            modifier= modifier
                .fillMaxWidth()
                .align(Alignment.Center)
        )
    }
}

@Composable
private fun downLoadData(){
    SyncScreen(
        onSyncClick = {

        },
        poemSynced = true,
        tagSynced = true,
        poemTagSynced = true,
        writerSynced = true,
        poemSentenceSynced = true,
        idiomSynced = true,
        chineseWisecrackSynced = true,
        poemProgress = 1f,
        tagProgress = 1f,
        poemTagProgress = 1f,
        writerProgress = 1f,
        poemSentenceProgress = 1f,
        idiomProgress = 1f,
        chineseWisecrackProgress = 1f,

        )
}
@Composable
private fun SyncScreen(
    modifier: Modifier= Modifier,
    onSyncClick:() ->Unit,
    poemSynced: Boolean,
    tagSynced: Boolean,
    poemTagSynced: Boolean,
    writerSynced: Boolean,
    poemSentenceSynced: Boolean,
    idiomSynced: Boolean,
    chineseWisecrackSynced: Boolean,
    poemProgress: Float,
    tagProgress: Float,
    poemTagProgress: Float,
    writerProgress: Float,
    poemSentenceProgress: Float,
    idiomProgress: Float,
    chineseWisecrackProgress: Float
  ){
    Column (
        modifier = modifier
            .fillMaxWidth()
            .padding(16.dp)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.spacedBy(16.dp),
    ){
        Button(onClick = onSyncClick) {
            Text(text = "同步数据")
        }
        Item(title = "古诗词文", progress = if(poemSynced) 1f else poemProgress)
        Item(title = "标签", progress =  if(tagSynced) 1f else tagProgress)
        Item(title = "古诗词文和标签对应关系", progress = if(poemTagSynced) 1f else poemTagProgress)
        Item(title = "诗人", progress = if(writerSynced) 1f else writerProgress)
        Item(title = "古诗词文名句", progress = if(poemSentenceSynced) 1f else poemSentenceProgress)
        Item(title = "成语", progress = if(idiomSynced) 1f else idiomProgress)
        Item(title = "歇后语", progress = if(chineseWisecrackSynced) 1f else chineseWisecrackProgress)
    }
}

@Composable
private fun  Item(
  title: String,
  progress:Float
){
    Text(text = title, style = MaterialTheme.typography.titleMedium)
    Row (modifier = Modifier.fillMaxWidth(),
         horizontalArrangement = Arrangement.SpaceBetween,
         verticalAlignment = Alignment.CenterVertically
        ){
        LinearProgressIndicator(
            progress = progress, modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        )
        Text(
            text = "${String.format("%.2f",(progress*100))}%",
            modifier = Modifier.width(100.dp),
            textAlign = TextAlign.End
            )
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    ComposeDemoTheme {
        Greeting("Android")
    }
}