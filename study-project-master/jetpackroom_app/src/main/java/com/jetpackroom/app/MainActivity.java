package com.jetpackroom.app;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    private void daoInsertUser(String account , String password){
        User user=new User();
        user.setAccount(account);
        user.setPassword(password);
        DBHelper.getDB().userDao().insert(user)
                .compose(RxDBHelper.singleSchedulers())
                .subscribe();

    }


    private void daoGetUser(){
        DBHelper.getDB().userDao().getAll().compose(RxDBHelper.singleSchedulers()).subscribe(new ObserverListener<List<User>>() {
            @Override
            public void onSuccessData(List<User> users) {

            }
            @Override
            public void onErrorData(String e){

            }
        });
    }

//    private void daoInsertUser(String account , String password){
//        User user=new User();
//        user.setAccount(account);
//        user.setPassword(password);
//        DBHelper.getDB().userDao().insert(user)
//                .compose(DBHelper.singleSchedulers())
//                .subscribe(new ObserverListener<List<Long>>() {
//                    @Override
//                    public void success(List<Long> longs) {
//
//                    }
//
//                    @Override
//                    public void onError(String msg) {
//
//                    }
//                });
//
//    }


//    private void daoGetUser(){
//        DBHelper.getDB().userDao().getAll()
//                .compose(DBHelper.singleSchedulers())
//                .subscribe(new ObserverListener<List<User>>() {
//                    @Override
//                    public void success(List<User> users) {
//                        if(users.size()<=0){
//                            daoInsertUser("admin","123456");
//                        }
//                    }
//
//                    @Override
//                    public void onError(String msg) {
//                        ToastHelp.showToast(msg);
//                    }
//                });
//    }



}