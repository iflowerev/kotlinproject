package com.jetpackroom.app;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Single;

/**
 * 完整的增删改查
 */
@Dao
public interface UserDao {

    //插入一条或多条用户数据
    @Insert
    Single<List<Long>> insert(User... users);

    //删除一条数据
    @Delete
    Single<Integer> delete(User user);

    //查询全部数据
    @Query("SELECT * FROM user")
    Single<List<User>> getAll();

    //查询指定账户数据
    @Query("SELECT * FROM user where account=:account")
    Single<List<User>> getOnUser(String account);

    //查询指定id数据
    @Query("SELECT * FROM user where uid=:uid")
    Single<User> getOnUserById(long uid);
}
