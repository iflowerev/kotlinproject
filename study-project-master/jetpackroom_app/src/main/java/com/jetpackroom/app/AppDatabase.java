package com.jetpackroom.app;
import androidx.room.Database;
import androidx.room.RoomDatabase;

/**
 * 定一个Dao管理类AppDatabase，继承自RoomDatabase
 */
@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}