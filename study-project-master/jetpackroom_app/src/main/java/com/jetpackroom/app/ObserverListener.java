package com.jetpackroom.app;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public   interface  ObserverListener<T> extends SingleObserver<T> {

    @Override
    default void onSuccess(T t) {
        onSuccessData(t);
    }
    @Override
    default void onSubscribe(Disposable d) { }


    @Override
    default void onError(Throwable e) {
        onErrorData(e.getMessage());
    }
    void onSuccessData(T t);

    void onErrorData(String e);
}