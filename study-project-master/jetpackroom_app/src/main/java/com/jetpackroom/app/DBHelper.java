package com.jetpackroom.app;

import androidx.room.Room;

/**
 * 封装Room数据库管理工具类DBHelper，管理AppDatabase
 */
public class DBHelper {
    private static AppDatabase appDatabase;

    //初始化数据库
    public static AppDatabase initDB() {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(App.getInstance(),
                    AppDatabase.class, "finance.db").build();
        }
        return appDatabase;
    }

    //获取表管理类AppDatabase
    public static AppDatabase getDB() {
        if(appDatabase==null){
            initDB();
        }
        return appDatabase;
    }


    //关闭数据库
    public static void closeDB() {
        if (appDatabase != null && appDatabase.isOpen()) {
            appDatabase.close();
        }
    }
}
