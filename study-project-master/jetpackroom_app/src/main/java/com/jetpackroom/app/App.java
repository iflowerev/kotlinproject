package com.jetpackroom.app;

import android.app.Application;

public class App  extends Application {
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        if (app == null) {
            app = this;
        }

        //初始化Room数据库
        DBHelper.initDB();
    }

    public static App getInstance() {
        return app;
    }
}
