package com.jay.studyproject.badger;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

public class MobileConfigUtil {
    /**
     * @return 手机类型 华为、小米、vivo、oppo
     * @desc 获取手机类型
     */
    public static String getMobileType() {
        return Build.MANUFACTURER;
    }


    /**
     * @desc 根据手机机型进入app自启动页面
     */
    public static void jumpStartInterface(Context context) {
        Intent intent = new Intent();
        try {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ComponentName componentName = null;
            if (getMobileType().equals("Xiaomi")) { // 小米、红米
//                componentName = new ComponentName(
//                        "com.miui.securitycenter",
//                        "com.miui.permcenter.autostart.AutoStartManagementActivity");
                // 在此根据用户手机当前版本跳转系统设置界面
                intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                intent.setData(Uri.fromParts("package",
                        context.getPackageName(), null));
            } else if (getMobileType().equals("Letv")) { // 乐视
                intent.setAction("com.letv.android.permissionautoboot");
            } else if (getMobileType().equals("samsung")) { // 三星
                componentName = ComponentName.unflattenFromString(
                        "com.samsung.android.sm_cn/com.samsung.android.sm.ui.ram.AutoRunActivity");
            } else if (getMobileType().equals("HONOR") // 荣耀
                    || getMobileType().equals("HUAWEI") // 华为
                    || getMobileType().equals("NOVA"))  // nova
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    componentName = new ComponentName(
                            "com.huawei.systemmanager",
                            "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity");
                } else {
                    componentName = new ComponentName(
                            "com.huawei.systemmanager",
                            "com.huawei.systemmanager.com.huawei.permissionmanager.ui.MainActivity");
                }
            } else if (getMobileType().equals("vivo")) { // vivo
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    componentName = new ComponentName(
//                            "com.vivo.permissionmanager",
//                            "com.vivo.permissionmanager.activity.PurviewTabActivity");
//                } else {
//                    componentName = ComponentName
//                            .unflattenFromString("com.iqoo.secure/.MainActivity");//i管家
//                }
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // 在此根据用户手机当前版本跳转系统设置界面
                intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                intent.setData(Uri.fromParts("package",
                        context.getPackageName(), null));
            } else if (getMobileType().equals("Meizu")) {//魅族
                //魅族内置手机管家界面
                componentName = ComponentName.unflattenFromString("com.meizu.safe" +
                        "/.permission.PermissionMainActivity");
            } else if (getMobileType().equals("OPPO")) { // OPPO
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    componentName = new ComponentName(
                            "com.coloros.safecenter",
                            "com.coloros.safecenter.startupapp.StartupAppListActivity");
                } else {
                    componentName = new ComponentName(
                            "com.color.safecenter",
                            "com.color.safecenter.permission.startup.StartupAppListActivity");
                }
            } else if (getMobileType().equals("ulong")) { // 360手机
                componentName = new ComponentName(
                        "com.yulong.android.coolsafe",
                        ".ui.activity.autorun.AutoRunListActivity");
            } else {
                // 在此根据用户手机当前版本跳转系统设置界面
                intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                intent.setData(Uri.fromParts("package",
                        context.getPackageName(), null));
            }
            intent.setComponent(componentName);
            context.startActivity(intent);
        } catch (Exception e) {//抛出异常就直接打开设置页面
            intent = new Intent(Settings.ACTION_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            Log.e("HLQ_Struggle", "抛出异常就直接打开设置页面");
        }
    }
    /**
     * @desc 去配置界面
     */

    public static void goConfigPage(Context context) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package",
                context.getPackageName(), null));
        context.startActivity(intent);
    }
}
