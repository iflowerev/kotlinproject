package com.jay.studyproject.interview;
/***
 * Android重点面试题
 * **/
public class AndroidContentDemo {
    /**
     * Activity面试题
     * 1.Activity 的生命周期？
     *
     * 2.Activity启动模式以及LaunchMode 的应用场景？
     *
     * 3.Activity中onNewIntent方法的调用时机和使用场景？
     *
     * 4.说说Activity加载的流程？
     *
     * 5.说下切换横竖屏时Activity的生命周期?
     *
     * 6.理解Activity，View,Window三者关系？
     *
     * Fragment面试题
     * 7.Fragment 的生命周期?
     *
     * 8.谈谈 Activity 和 Fragment 的区别？
     *
     * 9.FragmentPagerAdapter 与 FragmentStatePagerAdapter 的区别与使用场景 ?
     *
     * 10.Fragment 中 add 与 replace 的区别（Fragment 重叠）
     *
     * 11.getFragmentManager、getSupportFragmentManager 、 getChildFragmentManager 之间的区别？
     *
     * Service面试题
     * 12.谈一谈startService和bindService的区别，生命周期以及使用场景？
     *
     * BroadcastReceiver面试题
     * 13.BroadcastReceiver
     *
     * 14.BroadcastReceiver 与LocalBroadcastReceiver 有什么区别？
     *
     * 15.广播使用的方式和场景
     * ContentProvider面试题
     *
     * 16.ContentProvider
     *
     * 17.说说ContentProvider、ContentResolver、ContentObserver 之间的关系？
     *
     * Context面试题
     * 18.对于Context，你了解多少?
     *
     * 19.Application 和 Activity 的 Context 对象的区别
     *
     * UI面试题
     * 20.Window和DecorView是什么?DecorView又是如何和Window建立联系的?
     *
     * 21.简述一下Android 中 UI 的刷新机制？
     *
     * 22.LinearLayout, FrameLayout,RelativeLayout 哪个效率高, 为什么？
     *
     * 23.针对RecyclerView你做了哪些优化？
     *
     * 24.谈谈如何优化ListView？
     *
     * 25.什么是RemoteViews？使用场景有哪些？
     *
     * 26.谈谈自定义LayoutManager的流程？
     *
     * 27.getDimension、getDimensionPixelOffset 和getDimensionPixelSize 三者的区别？
     *
     * 28.请谈谈View.inflate和LayoutInflater.inflate的区别？
     *
     * 29.谈一谈SurfaceView与TextureView的使用场景和用法？
     *
     * 30.Android中View几种常见位移方式的区别？
     *
     * 31.为什么ViewPager嵌套ViewPager，内部的ViewPager滚动没有被拦截？
     *
     * 动画面试题
     * 32.动画
     *
     * 33.Android 补间动画和属性动画的区别？
     *
     * 34.谈一谈插值器和估值器？
     *
     * 原理机制面试题
     * 35.Android消息机制
     *
     * 36.View事件分发机制
     *
     * 37.View绘制原理
     *
     * 38.HandlerThread 的使用场景和用法？
     *
     * 39.IntentService 的应用场景和使用方式？
     *
     * 40.AsyncTask的优点和缺点？
     *
     * 41.谈谈你对Activity.runOnUiThread 的理解？
     *
     * 42.子线程能否更新UI？为什么？
     *
     * 43.为什么在子线程中创建Handler会抛异常？
     *
     * 44.试从源码角度分析Handler的post和sendMessage方法的区别和应用场景？
     *
     * 45.请谈谈invalidate()和postInvalidate()方法的区别和应用场景？
     *
     * 46.谈一谈自定义View和自定义ViewGroup？
     *
     * 性能优化面试题
     * 47.内存泄露如何查看和解决?
     *
     * 48.ANR是什么?怎样避免和解决ANR?
     *
     * 49.谈谈你对Android性能优化方面的了解？
     *
     * 50.自定义Handler 时如何有效地避免内存泄漏问题？
     *
     * 51.哪些情况下会导致oom问题？
     *
     * 52.谈谈Android中内存优化的方式？
     *
     * 53.谈谈布局优化的技巧？
     *
     * 54.Android 中的图片优化方案？
     *
     * 55.Android Native Crash问题如何分析定位？
     *
     * 56.谈谈怎么给apk瘦身？
     *
     * 57.谈谈你是如何优化App启动过程的？
     *
     * 58.谈谈代码混淆的步骤？
     *
     * 59.谈谈App的电量优化？
     *
     * 60.谈谈如何对WebView进行优化？
     *
     * 61.如何处理大图的加载？
     *
     * 62.谈谈如何对网络请求进行优化？
     *
     * 63.请谈谈如何加载Bitmap并防止内存溢出
     *
     * 64.内存泄漏和内存溢出区别？
     *
     * 65.说下冷启动与热启动是什么，区别，如何优化，使用场景等
     *
     * 设计模式面试题
     * 67.请简要谈一谈单例模式？
     *
     * 68.对于面向对象的六大基本原则了解多少？
     *
     * 69.谈一谈单例模式，建造者模式，工厂模式的使用场景？如何合理选择？
     *
     * 70.什么是代理模式？如何使用？Android源码中的代理模式？
     *
     * 71.静态代理和动态代理的区别，什么场景使用？
     *
     * 72.谈一谈责任链模式的使用场景？
     *
     * 设计架构面试题
     * 73.简述MVC ？
     *
     * 74.简述MVP？
     *
     * 75.简述MVVM ?
     *
     * 76.MVC 和 MVP 的区别？
     *
     * 77.MVVM和MVP区别?
     *
     * 76.MVP中你是如何处理Presenter层以防止内存泄漏的？
     *
     * 多线程面试题
     * 77.为什么要使用多线程
     *
     * 78.实现Runnable接口和继承Thread的区别
     *
     * 79.线程池的应用
     *
     * 80.AsyncTask和Handler对比
     *
     * 81.线程和进程有什么区别？
     *
     * 82.用户线程和守护线程有什么区别？
     *
     * 83.线程种的run()，start()方法区别？
     *
     * 84.什么是阻塞队列？如何使用阻塞队列来实现生产者-消费者模型？
     *
     * 85.并发三要素？
     *
     * 综合技术面试题
     * 86.请简要谈谈Android系统的架构组成？
     *
     * 87.SharedPreferences 是线程安全的吗？它的 commit 和 apply 方法有什么区别？
     *
     * 88.Serializable和Parcelable的区别?
     *
     * 89.谈谈ArrayMap和HashMap的区别？
     *
     * 90.什么是JNI？具体说说如何实现Java与C++的互调？
     *
     * 91.请简述从点击图标开始app的启动流程？
     *
     * 92.分别介绍下你所知道Android的几种存储方式
     *
     * Android第三方库
     * 92.Okhttp
     *
     * 93.Retrofit
     *
     * 94.Glide
     *
     * Android Framework
     * 95.系统服务
     *
     * 96.应用进程
     *
     * 97.Activity原理
     *
     * 98.其他应用组件
     *
     * 99.UI体系
     *
     * 100.进程通信
     *
     * 101.线程通信
     *
     * 102.请谈谈你对Binder机制的理解？
     *
     * 网络面试题
     * 103.请简述Http和Https的区别？
     *
     * 104.说一说HTTP,UDP,Socket区别？
     *
     * 105.请简述一次HTTP网络请求的过程?
     *
     * 106.谈一谈TCP/IP三次握手，四次挥手？
     *
     * 107.为什么说Http是可靠的数据传输协议？
     *
     * 108.TCP/IP协议分为哪几层？TCP和HTTP分别属于哪一层？
     */
}
