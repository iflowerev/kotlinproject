package com.jay.studyproject.demo.activity;

import android.os.Bundle;

import com.jay.common.HiBaseActivity;
import com.jay.studyproject.R;
import com.jay.studyproject.demo.logic.MainActivityLogic;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @ClassName: HiMainActivity
 * @Description: 框架类tabBottom
 * @Author: yangjie
 * @Date: 2022/4/17 4:12 下午
 */
public class HiMainActivity  extends HiBaseActivity  implements MainActivityLogic.ActivityProvider {

    private MainActivityLogic activityLogic;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_hi_main);
        activityLogic=new MainActivityLogic(this,savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        activityLogic.onSaveInstanceState(outState);
    }
}
