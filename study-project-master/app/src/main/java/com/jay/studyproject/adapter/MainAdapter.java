package com.jay.studyproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jay.studyproject.R;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter {

    private final Context mContext;

    private final List mData;

    public MainAdapter(Context context, List list) {

        this.mContext = context;

        this.mData = list;

    }

    @Override

    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
        BaseViewHolder holder = new BaseViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        String data = (String) mData.get(position % mData.size());
        BaseViewHolder mBaseViewHolder= (BaseViewHolder) holder;
        mBaseViewHolder.content.setText(data);
    }

    @Override

    public int getItemCount() {

        return Integer.MAX_VALUE;

    }

    class BaseViewHolder extends RecyclerView.ViewHolder {

        TextView content;

        public BaseViewHolder(View itemView) {

            super(itemView);

            content = (TextView) itemView.findViewById(R.id.content);

        }

    }

}