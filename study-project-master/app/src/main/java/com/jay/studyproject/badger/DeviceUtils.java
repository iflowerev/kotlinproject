package com.jay.studyproject.badger;

import android.os.Build;

public class DeviceUtils {
    public static Brand getDeviceBrand() {
        String brand = Build.BRAND;
        if (brand != null) {
            switch (brand.toLowerCase()) {
                //小米
                case "xiaomi":
                    return Brand.XIAOMI;
                //荣耀
                case "honor":
                    return Brand.HONOR;
                //华为
                case "huawei":
                    return Brand.HUAWEI;
                //OV
                case "ov":
                    return Brand.OPPO_VIVO;
                default:
                //其他
                    return Brand.OTHER;
            }
        }
        return Brand.UNKNOWN;
    }

    public enum Brand {
        XIAOMI, HONOR, HUAWEI, OPPO_VIVO, OTHER, UNKNOWN
    }
}
