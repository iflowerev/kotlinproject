package com.jay.studyproject.demo.activity

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.jay.loglibrary.HiLog
import com.jay.studyproject.R
import com.jay.tab.bottom.HiTabBottom
import com.jay.tab.bottom.HiTabBottomInfo

/**
 * @ClassName: HiBottomActivity
 * @Description:首页
 * @Author: yangjie
 * @Date: 2022/4/16 8:57 下午
 */
class HiBottomActivity:AppCompatActivity(), View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hi_bottom)
        val tabBottom=findViewById<HiTabBottom>(R.id.bottom)
        val homeInfo=HiTabBottomInfo("首页",
            "fonts/iconfont.ttf",
            getString(R.string.if_home),
            null,
                    "#ff656667",
        "#ffd44949")
        tabBottom.setHiTabInfo(homeInfo)
    }

    override fun onClick(v: View?) {

    }

}