package com.jay.studyproject.demo.logic;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import com.jay.common.tab.HiFragmentTabView;
import com.jay.common.tab.HiTabViewAdapter;
import com.jay.studyproject.R;
import com.jay.studyproject.demo.fragment.CategoryPageFragment;
import com.jay.studyproject.demo.fragment.FavoitePageFragment;
import com.jay.studyproject.demo.fragment.HomePageFragment;
import com.jay.studyproject.demo.fragment.ProfilePageFragment;
import com.jay.studyproject.demo.fragment.RecommendPageFragment;
import com.jay.tab.bottom.HiTabBottomInfo;
import com.jay.tab.bottom.HiTabBottomLayout;
import com.jay.tab.common.IHiTabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentManager;

/**
 * @ClassName: MainActivityLogic
 * @Description: 将HiMainActivity的一些逻辑内聚在这，让HiMainActivity更加清爽
 * @Author: yangjie
 * @Date: 2022/4/17 4:05 下午
 */
public class MainActivityLogic {

    private HiFragmentTabView fragmentTabView;

    private HiTabBottomLayout hiTabBottomLayout;

    private List<HiTabBottomInfo<?>> infoList;

    private ActivityProvider activityProvider;

    private final static String SAVED_CURRENT_ID="SAVED_CURRENT_ID";

    private int currentItemIndex;


    public MainActivityLogic(ActivityProvider activityProvider,Bundle saveInstanceState) {
        this.activityProvider = activityProvider;
        //fix 不保留活动导致的Fragment重叠问题
        if(saveInstanceState!=null){
            currentItemIndex=saveInstanceState.getInt(SAVED_CURRENT_ID);
        }
        initTabBottom();

    }

    public void onSaveInstanceState(@NonNull Bundle oustate){
        oustate.putInt(SAVED_CURRENT_ID,currentItemIndex);
    }
    public HiFragmentTabView getFragmentTabView() {
        return fragmentTabView;
    }

    public HiTabBottomLayout getHiTabBottomLayout() {
        return hiTabBottomLayout;
    }

    public List<HiTabBottomInfo<?>> getInfoList() {
        return infoList;
    }

    private void initTabBottom(){
        hiTabBottomLayout=activityProvider.findViewById(R.id.tab_bottom_layout);
        hiTabBottomLayout.setTabAlpha(0.85f);
        infoList=new ArrayList<>();
        int defaultColor=activityProvider.getResources().getColor(R.color.tabBottomDefaultColor);
        int tintColor=activityProvider.getResources().getColor(R.color.tabBottomTintColor);
        HiTabBottomInfo homeInfo=new HiTabBottomInfo("首页",
                "fonts/iconfont.ttf",
                activityProvider.getString(R.string.if_home),
                null,
                defaultColor,
                tintColor);
        homeInfo.fragment= HomePageFragment.class;
        HiTabBottomInfo infoRecommend=new HiTabBottomInfo("收藏",
                "fonts/iconfont.ttf",
                activityProvider.getString(R.string.if_recommend),
                null,
                defaultColor,
                tintColor);
        infoRecommend.fragment= FavoitePageFragment.class;
        HiTabBottomInfo infoCategory=new HiTabBottomInfo("分类",
            "fonts/iconfont.ttf",
                activityProvider. getString(R.string.if_category),
            null,
                defaultColor,
                tintColor);
        infoCategory.fragment= CategoryPageFragment.class;
        HiTabBottomInfo infoChat=new HiTabBottomInfo("推荐",
                "fonts/iconfont.ttf",
                activityProvider.getString(R.string.if_chat),
                null,
                defaultColor,
                tintColor);
        infoChat.fragment= RecommendPageFragment.class;
        HiTabBottomInfo infoProfile=new HiTabBottomInfo("我的",
                "fonts/iconfont.ttf",
                activityProvider.getString(R.string.if_profile),
                null,
                defaultColor,
                tintColor);
        infoProfile.fragment= ProfilePageFragment.class;
        infoList.add(homeInfo);
        infoList.add(infoRecommend);
        infoList.add(infoCategory);
        infoList.add(infoChat);
        infoList.add(infoProfile);
        hiTabBottomLayout.inflateInfo(infoList);
        initFragmentTabView();
        hiTabBottomLayout.addTabSelectedChangListener(new IHiTabLayout.OnTabSelectedListener<HiTabBottomInfo<?>>() {
            @Override
            public void onTabSelectedListener(int index, @Nullable HiTabBottomInfo<?> pervInfo, @NonNull HiTabBottomInfo<?> nextInfo) {
                fragmentTabView.setCurrentItem(index);
                MainActivityLogic.this.currentItemIndex=index;
            }
        });
        hiTabBottomLayout.defaultSelected(infoList.get(currentItemIndex));
    }

    private void initFragmentTabView(){
        HiTabViewAdapter tabViewAdapter=new HiTabViewAdapter(activityProvider.getSupportFragmentManager(),infoList);
        fragmentTabView=activityProvider.findViewById(R.id.bottom);
        fragmentTabView.setAdapter(tabViewAdapter);
    }

    public interface  ActivityProvider{
        <T extends View> T findViewById(@IdRes int id);

        Resources getResources();

        FragmentManager getSupportFragmentManager();

        String getString(@StringRes int resId);
    }
}
