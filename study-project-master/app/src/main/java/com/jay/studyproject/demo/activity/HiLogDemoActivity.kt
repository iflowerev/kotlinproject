package com.jay.studyproject.demo.activity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.jay.loglibrary.*
import com.jay.studyproject.R
import com.jay.tab.bottom.HiTabBottom
/**
 * 日志打印
 */

class HiLogDemoActivity: AppCompatActivity(){

    var viewPrinter:HiViewPrinter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hi_log)
        viewPrinter= HiViewPrinter(this)
        findViewById<View>(R.id.btn_log).setOnClickListener {
            printLog()
        }
        viewPrinter!!.viewProvider.showFloatingView()
    }
    private fun printLog(){
        HiLogManager.getInstance().addPrinter(viewPrinter)
        //自定义log配置
        HiLog.log(object:HiLogConfig(){
            override fun includeThread(): Boolean {
                return true
            }

            override fun stackTraceDepth(): Int {
                return 0
            }
        },HiLogType.E,"-----","5566")
        HiLog.a("9900")
    }
}