package com.jay.studyproject.test
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.jay.studyproject.adapter.MainAdapter
import com.jay.studyproject.databinding.ActivityTest1MainBinding

/**
 * 滚动列表
 */
class TestMainActivity: AppCompatActivity(){

    private lateinit var binding: ActivityTest1MainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTest1MainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mData = mutableListOf<String>()
        for (i in 0..29) {
            mData.add("加载的第" + i + "条数据")
        }
//        var layoutManager=object : LinearLayoutManager(this){
//            override fun smoothScrollToPosition(
//                recyclerView: RecyclerView?,
//                state: RecyclerView.State?,
//                position: Int
//            ) {
//                var linearSmoothScroller=object : LinearSmoothScroller(recyclerView?.context){
//                    override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics?): Float {
//                        //这边可以自定义进行控制速度
//                        return 3f / (displayMetrics?.density ?: 1f)
//                    }
//                }
//                linearSmoothScroller.setTargetPosition(position)
//                startSmoothScroll(linearSmoothScroller)
//            }
//        }

        binding.listItem.layoutManager=ScrollLinearLayoutManager(this)
        binding.listItem.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        binding.listItem.adapter = MainAdapter(this,mData)
        startItem()
    }
    private val mHandler: Handler = Handler()
    private var scrollRunnable: Runnable? = null
    private var mAnimPosi=0
    private  fun  startItem(){
        scrollRunnable = Runnable {
            run {
//                binding.listItem.scrollBy(0, HiDisplayUtil.dp2px(25f))
//                var linearSmoothScroller=object : LinearSmoothScroller( binding.listItem?.context){
//                    override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics?): Float {
//                        //这边可以自定义进行控制速度
//                        return 3f / (displayMetrics?.density ?: 1f)
//                    }
//                }
//                binding.listItem.layoutManager!!.startSmoothScroll(linearSmoothScroller)
                binding.listItem.smoothScrollToPosition(mAnimPosi)
//                mAnimPosi++
                Log.e("mAnimPosi","----"+mAnimPosi)
                mHandler.postDelayed(scrollRunnable!!,1000)
            }
        }
//        mHandler.postDelayed(scrollRunnable!!, 1000)
    }

    override fun onResume() {
        super.onResume()
        Log.e("mAnimPosi","----开始拉---------")
//        if(mAnimPosi==0){
//            mHandler.post(scrollRunnable!!)
//        }else{
            mHandler.postDelayed(scrollRunnable!!, 1000)
//        }

    }

    override fun onStop() {
        super.onStop()
        mHandler.removeCallbacks(scrollRunnable!!)
    }
}