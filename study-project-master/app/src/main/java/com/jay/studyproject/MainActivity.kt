package com.jay.studyproject
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jay.studyproject.databinding.ActivityMainBinding
import com.jay.studyproject.demo.activity.HiLogDemoActivity
import com.jay.studyproject.demo.activity.HiMainActivity
import com.jay.studyproject.demo.activity.HiTabBottomDemoActivity
import com.jay.studyproject.demo.activity.HiTabTopDemoActivity
import com.jay.studyproject.demo.refresh.HiRefreshDemoActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initEvent()
    }

    private fun initEvent() {
        val mIntent=Intent()
        binding.btnLog.setOnClickListener {
            //日志打印
            mIntent.setClass(this, HiLogDemoActivity::class.java)
            startActivity(mIntent)
            jumpAlipay()
        }
        binding.btnTabBottom.setOnClickListener {
            mIntent.setClass(this, HiTabBottomDemoActivity::class.java)
            startActivity(mIntent)
        }
        binding.btnBottomMain.setOnClickListener {
            mIntent.setClass(this,HiMainActivity::class.java)
            startActivity(mIntent)
        }
        binding.btnTop.setOnClickListener {
            mIntent.setClass(this,HiTabTopDemoActivity::class.java)
            startActivity(mIntent)
        }
        binding.btnRefresh.setOnClickListener {
            mIntent.setClass(this,HiRefreshDemoActivity::class.java)
            startActivity(mIntent)
        }
    }
    //跳转小程序
    fun jumpAlipay() {
        try {
            val uri = ("alipayqr://platformapi/startapp?appId=2021001164644764");
////                    + "&page=pages/index/index?userId=123456" //页面参数
////                    + "&query=itemId=005007") //启动参数
//            val uri = ("https://ur.alipay.com/76Oo0Ql6HYn5NNLtqW0wwr") //启动参数
            val intent = Intent.parseUri(uri, Intent.URI_INTENT_SCHEME)
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}