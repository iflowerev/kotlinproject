package com.jay.studyproject
import android.app.Application
import com.google.gson.Gson
import com.jay.loglibrary.HiConsolerPrinter
import com.jay.loglibrary.HiLogConfig
import com.jay.loglibrary.HiLogManager
class MyApplication : Application(){
    override fun onCreate() {
        super.onCreate()

        HiLogManager.init(object: HiLogConfig(){

            override fun injectJsonParser(): JsonParser {
                return JsonParser { src -> Gson().toJson(src) }
            }
            override fun getGlobalTag(): String {
                return "MyApplication"
            }

            override fun enable(): Boolean {
                return true
            }
        },HiConsolerPrinter())
    }
}
