package com.jay.studyproject.demo.fragment;

import com.jay.common.HiBaseFragment;
import com.jay.studyproject.R;

/**
 * @ClassName: HomePageFragment
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/17 12:43 下午
 */
public class RecommendPageFragment extends HiBaseFragment {
    @Override
    public int getLayoutId() {
        return R.layout.fragment_recommend;
    }
}
