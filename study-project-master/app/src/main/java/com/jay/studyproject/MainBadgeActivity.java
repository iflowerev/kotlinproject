package com.jay.studyproject;
import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import me.leolin.shortcutbadger.ShortcutBadger;
import android.content.Context;

import androidx.core.app.NotificationCompat;

import com.jay.studyproject.badger.MobileConfigUtil;

public class MainBadgeActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badge_main);

//        MobileConfigUtil.jumpStartInterface(this);

        final EditText numInput = findViewById(R.id.numInput);

        Button button = findViewById(R.id.btnSetBadge);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int badgeCount = 0;
                try {
                    badgeCount = Integer.parseInt(numInput.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Error input", Toast.LENGTH_SHORT).show();
                }

                setBadgeNumber(MainBadgeActivity.this,badgeCount);

//                boolean success = ShortcutBadger.applyCount(MainBadgeActivity.this, badgeCount);
//
//                Toast.makeText(getApplicationContext(), "Set count=" + badgeCount + ", success=" + success, Toast.LENGTH_SHORT).show();
            }
        });

        Button launchNotification = findViewById(R.id.btnSetBadgeByNotification);
        launchNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int badgeCount = 0;
                try {
                    badgeCount = Integer.parseInt(numInput.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Error input", Toast.LENGTH_SHORT).show();
                }
                finish();
                startService(
                    new Intent(MainBadgeActivity.this, BadgeIntentService.class).putExtra("badgeCount", badgeCount)
                );
            }
        });

        Button removeBadgeBtn = findViewById(R.id.btnRemoveBadge);
        removeBadgeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = ShortcutBadger.removeCount(MainBadgeActivity.this);

                Toast.makeText(getApplicationContext(), "success=" + success, Toast.LENGTH_SHORT).show();
            }
        });


        //find the home launcher Package
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        String currentHomePackage = "none";
        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);

        // in case of duplicate apps (Xiaomi), calling resolveActivity from one will return null
        if (resolveInfo != null) {
            currentHomePackage = resolveInfo.activityInfo.packageName;
        }

        TextView textViewHomePackage = findViewById(R.id.textViewHomePackage);
        textViewHomePackage.setText("launcher:" + currentHomePackage);
    }


    /**
     * 华为设置角标
     * @param context
     * @param number
     */
    public static void setBadgeNumber(Context context, int number) {
        try {
            if (number < 0) number = 0;
            Bundle bundle = new Bundle();
            bundle.putString("package", context.getPackageName());
            String launchClassName = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()).getComponent().getClassName();
            bundle.putString("class", launchClassName);
            bundle.putInt("badgenumber", number);
            context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void setBadgeNum(Context context,int num) {
//        try {
//            Bundle bunlde = new Bundle();
//            bunlde.putString("package", context.getPackageName()); // com.test.badge is your package name
//            bunlde.putString("class", "com.test. badge.MainActivity"); // com.test. badge.MainActivity is your apk main activity
//            bunlde.putInt("badgenumber", num);
//            this.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, bunlde);
//        } catch (Exception e) {
//
//        }
//    }



//    private void setHUAWEIIconBadgeNum(Context context,int count)  {
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setWhen(System.currentTimeMillis())
//                .setAutoCancel(true);
//
//        mBuilder.setContentTitle("test");
//        mBuilder.setTicker("test");
//        mBuilder.setContentText("test");
//
//        //点击set 后，app退到桌面等待3s看效果（有的launcher当app在前台设置未读数量无效）
//        final Notification notification = mBuilder.build();
//        setNotification(notification, notificationId, context);
//        Bundle bunlde = new Bundle();
//        bunlde.putString("package", context.getPackageName());
//        bunlde.putString("class", getLauncherClassName(context));
//        bunlde.putInt("badgenumber", count);
//        context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, bunlde);
//    }
}
