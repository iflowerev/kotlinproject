package com.jay.studyproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import com.jay.studyproject.adapter.AutoPollAdapter
import com.jay.studyproject.databinding.ActivityTestMainBinding

/**
 * 滚动列表
 */

class TestMainActivity: AppCompatActivity(){

    private lateinit var binding: ActivityTestMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTestMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mData = mutableListOf<String>()
        for (i in 0..29) {
            mData.add("加载的第" + i + "条数据")
        }
        val mAdapter=AutoPollAdapter(this,mData)
        binding.recycleview.layoutManager = LinearLayoutManager(this, VERTICAL, false) //设置
        binding.recycleview.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        binding.recycleview.adapter=mAdapter

//        //使用kotlin动态的创建textview的对象
//        var textview: TextView = TextView(this)
//        //kotlin中使用的是直接如下  .属性 来设置的，不再用setxxx设置属性
//        textview.text = "这是一个动态添加的标题xxxx"
//        textview.textSize = 25f
//        textview.setTextColor(R.color.tabBottomTintColor)
//        textview.gravity = Gravity.CENTER
//
//        binding.flipper.addView(textview) //动态添加一条textview（静态动态添加都可以）
//
//        binding.flipper.startFlipping()
//        // 设置进入动画
//        binding.flipper.inAnimation = AnimationUtils.loadAnimation(this, R.anim.push_up_in)
//        // 设置滚出动画
//        binding.flipper.outAnimation = AnimationUtils.loadAnimation(this, R.anim.push_up_out)
    }

    override fun onResume() {
        super.onResume()
        binding.recycleview.start()//不能掉，不然不会自动滚动，根据自己的需求来
    }

    override fun onStop() {
        super.onStop()
        binding.recycleview.stop()
    }
}