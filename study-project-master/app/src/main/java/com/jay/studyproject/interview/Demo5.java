package com.jay.studyproject.interview;

/**
 * 多线程
 */
public class Demo5 {
    /**
     * 一、为什么要使用多线程
     * a)提高用户体验或避免ANR，在事件处理中需要使用多线程，否则会出现ANR，或者因为响应较慢导致用户体验很差。
     * b)异步，应用中有些情况并不一定需要同步阻塞去等待返回结果，可以通过多线程来实现异步，
     * 例如你的应用的某个Activity需要从云端获取一些图片，加载图片比较耗时，这时需要使用异步加载，加载完成一个图片刷新一个。
     * c)多任务，例如多线程下载
     *
     * 二、什么是ANR
     * ANR全程Application Not Responding，意思是程序未响应，如果一个应用无法响应用户的输入，系统就会弹出一个ANR对话框，用户可以自行选择继续等待亦或停止当前程序。
     * ANR 的四种场景：
     * Service TimeOut:  service 未在规定时间执行完成：前台服务 20s，后台 200s
     * BroadCastQueue TimeOut: 未在规定时间内未处理完广播：前台广播 10s 内, 后台 60s 内
     * ContentProvider TimeOut:  publish 在 10s 内没有完成
     * Input Dispatching timeout:  5s 内未响应键盘输入、触摸屏幕等事件
     * ANR 的根本原因是：应用未在规定的时间内处理 AMS 指定的任务才会 ANR。
     * 另外，人眼可以分辨的时间的160毫秒，超过这个时间就可以感到卡顿，所以要控制好这个时间。
     *
     * 三、实现Runnable接口和继承Thread的区别
     * 1 ,一个类只能继承一个父类，存在局限；一个类可以实现多个接口
     * 2 ,在实现Runable接口的时候调用Thread(Runnable target)创建进程时 ,使用同一个Runnable实例,则建立的多线程的实例变量也是共享的。
     * 但是通过继承Thread类是不能用一个实例建立多个线程，故而实现 Runnable接口适合于资源共享。当然，继承Thread类也能够共享变量, 能共享Thread类的static变量;
     * 3 , Runnable接口和Thread之间的联系:
     * public class Thread extends Object implements Runnable可以看出Thread类也是Runnable接口的子类；
     *
     * 四、线程池的应用
     * new Thread的弊端：
     * a.每次new Thread新建对象性能差.
     * b.线程缺乏统一管理,可能无限制新建线程,相互之间竞争，即可能占用过多系统资源导致死机或oom.
     * c.缺乏更多功能,如定时执行、定期执行、线程中断。
     * 4.1、缓存线程池
     * newCachedThreadPool创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，如无可回收，则新建线程。
     * 4.2、定长线程池
     * newFixedThreadPool创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待。
     * 4.3、单个线程池
     * newSingleThreadExecutor创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序（FIFO,LIFO,优先级）执行。
     * 4.4、定时线程池
     * newScheduledThreadPool创建一个定时线程池，支持定时及周期性任务执行。
     *
     * 五、AsyncTask和Handler对比
     * 1. AsyncTask实现的原理和适用的优缺点
     * AsyncTask,是android提供的轻量级的异步类，可以直接继承AsyncTask,在类中实现异步操作，并提供接口反馈当前异步执行的程度（可以通过接口实现UI进度更新），最后反馈执行的结果给UI主线程。
     * 使用的优点：
     * 简单，快捷，过程可控
     * 使用的缺点：
     * 在使用多个异步操作，并需要进行Ui变更时，就变得复杂起来.
     *
     * 2)Handler异步实现的原理和适用的优缺点
     * 在Handler异步实现时,涉及到Handler, Looper, Message,Thread四个对象，实现异步的流程是主线程启动Thread （子线程）,
     * 子线程运行并生成Message通过Handler发送出去，然后Looper取出消息队列中的Message再分发给Handler进行UI变更。
     * 使用的优点：
     * 结构清晰，功能定义明确。对于多个后台任务时，简单，清晰
     * 使用的缺点：
     * 在单个后台异步处理时，显得代码过多，结构过于复杂。
     *
     *
     * 六、线程和进程有什么区别？
     * 一个进程是一个独立(self contained)的运行环境，它可以被看作一个程序或者一个应用。而线程是在进程中执行的一个任务。线程是进程的子集，
     * 一个进程可以有很多线程，每条线程并行执行不同的任务。不同的进程使用不同的内存空间，
     * 而向所有的线程共享一片相同的内存空间。别把它和栈内存搞混，每个线程都拥有单独的栈内存用来存储本地数据。
     *
     *
     * 七、多线程编程的好处是什么？
     * 在多线程程序中，多个线程被并发的执行以提高程序的效率，CPU不会因为某个线程需要等待资源而进入空闲状态。多个线程共享堆内存(heap memory)，
     * 因此创建多个线程去执行一些任务会比创建多个进程更好。举个例子，Servlets比CGI更好，是因为Servlets支持多线程而CGI不支持。
     *
     *
     * 八、如何在Java中实现线程？
     * 创建线程有两种方式：
     * 一、继承 Thread 类，扩展线程。
     * 二、实现 Runnable 接口。
     *
     *
     * 九、用户线程和守护线程有什么区别？
     * 当我们在Java程序中创建一个线程，它就被称为用户线程。一个守护线程是在后台执行并且不会阻止JVM终止的线程。
     * 当没有用户线程在运行的时候，JVM关闭程序并且退出。一个守护线程创建的子线程依然是守护线程。
     *
     * 十、你对线程优先级的理解是什么？
     *
     * 每一个线程都是有优先级的，一般来说，高优先级的线程在运行时会具有优先权，但这依赖于线程调度的实现，这个实现是和操作系统相关的(OS dependent)。
     * 我们可以定义线程的优先级，但是这并不能保证高优先级的线程会在低优先级的线程前执行。线程优先级是一个int变量(从1-10)，1代表最低优先级，10代表最高优先级。
     *
     * 十一、在多线程中，什么是上下文切换(context-switching)？
     *
     * 上下文切换是存储和恢复CPU状态的过程，它使得线程执行能够从中断点恢复执行。上下文切换是多任务操作系统和多线程环境的基本特征。
     *
     * 十二、启动一个线程是调用run()还是start()方法？
     *
     * 启动一个线程是调用start()方法，使线程所代表的虚拟处理机处于可运行状态，这意味着它可以由JVM 调度并执行，这并不意味着线程就会立即运行。run()方法是线程启动后要进行回调（callback）的方法。
     *
     * 十三、线程种的run()，start()方法区别？
     *
     * 1.start():
     *      先来看看Java API中对于该方法的介绍：
     *      使该线程开始执行；Java 虚拟机调用该线程的 run 方法。
     *      结果是两个线程并发地运行；当前线程（从调用返回给 start 方法）和另一个线程（执行其 run 方法）。
     *      多次启动一个线程是非法的。特别是当线程已经结束执行后，不能再重新启动。
     * 用start方法来启动线程，真正实现了多线程运行，这时无需等待run方法体中的代码执行完毕而直接继续执行后续的代码。
     * 通过调用Thread类的 start()方法来启动一个线程，这时此线程处于就绪（可运行）状态，并没有运行，一旦得到cpu时间片，就开始执行run()方法，
     * 这里的run()方法 称为线程体，它包含了要执行的这个线程的内容，Run方法运行结束，此线程随即终止。
     * 2。run():
     * 同样先看看Java API中对该方法的介绍：
     * 如果该线程是使用独立的 Runnable 运行对象构造的，则调用该 Runnable 对象的 run 方法；否则，该方法不执行任何操作并返回。
      Thread 的子类应该重写该方法。
     * run()方法只是类的一个普通方法而已，如果直接调用Run方法，程序中依然只有主线程这一个线程，其程序执行路径还是只有一条，还是要顺序执行，
     * 还是要等待run方法体执行完毕后才可继续执行下面的代码，这样就没有达到写线程的目的。
     * 3。总结：
     * 调用start方法方可启动线程，而run方法只是thread类中的一个普通方法调用，还是在主线程里执行。
     *
     * 十四、Thread类的sleep()方法和对象的wait()方法都可以让线程暂停执行，它们有什么区别?
     *
     * sleep()方法（休眠）是线程类（Thread）的静态方法，调用此方法会让当前线程暂停执行指定的时间，将执行机会（CPU）让给其他线程，
     * 但是对象的锁依然保持，因此休眠时间结束后会自动恢复（线程回到就绪状态，请参考第66题中的线程状态转换图）。wait()是Object类的方法，
     * 调用对象的wait()方法导致当前线程放弃对象的锁（线程暂停执行），进入对象的等待池（wait pool），
     * 只有调用对象的notify()方法（或notifyAll()方法）时才能唤醒等待池中的线程进入等锁池（lock pool），如果线程重新获得对象的锁就可以进入就绪状态。
     *
     * 十五、线程的sleep()方法和yield()方法有什么区别？
     *
     * sleep()方法给其他线程运行机会时不考虑线程的优先级，因此会给低优先级的线程以运行的机会；
     * yield()方法只会给相同优先级或更高优先级的线程以运行的机会；
     * 线程执行sleep()方法后转入阻塞（blocked）状态，而执行yield()方法后转入就绪（ready）状态；
     * sleep()方法声明抛出InterruptedException，而yield()方法没有声明任何异常；
     * sleep()方法比yield()方法（跟操作系统CPU调度相关）具有更好的可移植性。
     *
     * 十六、你如何确保main()方法所在的线程是Java程序最后结束的线程？
     * 我们可以使用Thread类的joint()方法来确保所有程序创建的线程在main()方法退出前结束。这里有一篇文章关于Thread类的joint()方法。
     *
     *
     * 十七、什么是线程调度器(Thread Scheduler)和时间分片(Time Slicing)？
     * 线程调度器是一个操作系统服务，它负责为Runnable状态的线程分配CPU时间。一旦我们创建一个线程并启动它，它的执行便依赖于线程调度器的实现。
     * 时间分片是指将可用的CPU时间分配给可用的Runnable线程的过程。分配CPU时间可以基于线程优先级或者线程等待的时间。
     * 线程调度并不受到Java虚拟机控制，所以由应用程序来控制它是更好的选择（也就是说不要让你的程序依赖于线程的优先级）。
     *
     * 十八、请说出与线程同步以及线程调度相关的方法。
     *
     * wait()：
     * 使一个线程处于等待（阻塞）状态，并且释放所持有的对象的锁；
     * sleep()：
     * 使一个正在运行的线程处于睡眠状态，是一个静态方法，调用此方法要处理InterruptedException异常；
     * notify()：
     * 唤醒一个处于等待状态的线程，当然在调用此方法的时候，并不能确切的唤醒某一个等待状态的线程，而是由JVM确定唤醒哪个线程，而且与优先级无关；
     * notityAll()：
     * 唤醒所有处于等待状态的线程，该方法并不是将对象的锁给所有线程，而是让它们竞争，只有获得锁的线程才能进入就绪状态；
     *
     * 十九、线程之间是如何通信的？
     *
     * 当线程间是可以共享资源时，线程间通信是协调它们的重要的手段。Object类中wait()\notify()\notifyAll()方法可以用于线程间通信关于资源的锁的状态。点击这里有更多关于线程wait, notify和notifyAll.
     *
     * 二十、为什么线程通信的方法wait(), notify()和notifyAll()被定义在Object类里？
     *
     * Java的每个对象中都有一个锁(monitor，也可以成为监视器) 并且wait()，notify()等方法用于等待对象的锁或者通知其他线程对象的监视器可用。
     * 在Java的线程中并没有可供任何对象使用的锁和同步器。这就是为什么这些方法是Object类的一部分，这样Java的每一个类都有用于线程间通信的基本方法
     *
     * 二十一、为什么wait(), notify()和notifyAll()必须在同步方法或者同步块中被调用？
     *
     * 当一个线程需要调用对象的wait()方法的时候，这个线程必须拥有该对象的锁，接着它就会释放这个对象锁并进入等待状态直到其他线程调用这个对象上的notify()方法。同样的，
     * 当一个线程需要调用对象的notify()方法时，它会释放这个对象的锁，以便其他在等待的线程就可以得到这个对象锁。由于所有的这些方法都需要线程持有对象的锁，这样就只能通过同步来实现，
     * 所以他们只能在同步方法或者同步块中被调用。
     *
     * 二十二.为什么Thread类的sleep()和yield()方法是静态的？
     *
     * Thread类的sleep()和yield()方法将在当前正在执行的线程上运行。所以在其他处于等待状态的线程上调用这些方法是没有意义的。这就是为什么这些方法是静态的。它们可以在当前正在执行的线程中工作，
     * 并避免程序员错误的认为可以在其他非运行线程调用这些方法。
     *
     * 二十三、如何确保线程安全？
     *
     * 在Java中可以有很多方法来保证线程安全——同步，使用原子类(atomic concurrent classes)，实现并发锁，使用volatile关键字，使用不变类和线程安全类。在线程安全教程中，你可以学到更多。
     *
     * 二十四、volatile关键字在Java中有什么作用？
     *
     * 当我们使用volatile关键字去修饰变量的时候，所以线程都会直接读取该变量并且不缓存它。这就确保了线程读取到的变量是同内存中是一致的。
     *
     * 二十五、同步方法和同步块，哪个是更好的选择？
     *
     * 同步块是更好的选择，因为它不会锁住整个对象（当然你也可以让它锁住整个对象）。同步方法会锁住整个对象，哪怕这个类中有多个不相关联的同步块，这通常会导致他们停止执行并需要等待获得这个对象上的锁。
     *
     * 二十六、如何创建守护线程？
     *
     * 使用Thread类的setDaemon(true)方法可以将线程设置为守护线程，需要注意的是，需要在调用start()方法前调用这个方法，否则会抛出IllegalThreadStateException异常。
     *
     * 二十七、什么是ThreadLocal?
     *
     * ThreadLocal用于创建线程的本地变量，我们知道一个对象的所有线程会共享它的全局变量，所以这些变量不是线程安全的，我们可以使用同步技术。但是当我们不想使用同步的时候，我们可以选择ThreadLocal变量。
     * 每个线程都会拥有他们自己的Thread变量，它们可以使用get()\set()方法去获取他们的默认值或者在线程内部改变他们的值。ThreadLocal实例通常是希望它们同线程状态关联起来是private static属性。
     * 在ThreadLocal例子这篇文章中你可以看到一个关于ThreadLocal的小程序。
     *
     * 二十八、什么是Thread Group？为什么不建议使用它？
     *
     * ThreadGroup是一个类，它的目的是提供关于线程组的信息。
     * ThreadGroup API比较薄弱，它并没有比Thread提供了更多的功能。它有两个主要的功能：一是获取线程组中处于活跃状态线程的列表；二是设置为线程设置未捕获异常处理器(ncaught exception handler)。
     * 但在Java 1.5中Thread类也添加了setUncaughtExceptionHandler(UncaughtExceptionHandler eh) 方法，所以ThreadGroup是已经过时的，不建议继续使用。
     *
     * 二十九、什么是Java线程转储(Thread Dump)，如何得到它？
     *
     * 线程转储是一个JVM活动线程的列表，它对于分析系统瓶颈和死锁非常有用。有很多方法可以获取线程转储——使用Profiler，Kill -3命令，jstack工具等等。我更喜欢jstack工具，因为它容易使用并且是JDK自带的。
     * 由于它是一个基于终端的工具，所以我们可以编写一些脚本去定时的产生线程转储以待分析。读这篇文档可以了解更多关于产生线程转储的知识。
     *
     * 三十、什么是死锁(Deadlock)？如何分析和避免死锁？
     *
     * 死锁是指两个以上的线程永远阻塞的情况，这种情况产生至少需要两个以上的线程和两个以上的资源。
     * 分析死锁，我们需要查看Java应用程序的线程转储。我们需要找出那些状态为BLOCKED的线程和他们等待的资源。每个资源都有一个唯一的id，用这个id我们可以找出哪些线程已经拥有了它的对象锁。
     * 避免嵌套锁，只在需要的地方使用锁和避免无限期等待是避免死锁的通常办法，阅读这篇文章去学习如何分析死锁。
     *
     * 三十一、什么是Java Timer类？如何创建一个有特定时间间隔的任务？
     *
     * java.util.Timer是一个工具类，可以用于安排一个线程在未来的某个特定时间执行。Timer类可以用安排一次性任务或者周期任务。
     * java.util.TimerTask是一个实现了Runnable接口的抽象类，我们需要去继承这个类来创建我们自己的定时任务并使用Timer去安排它的执行。
     * 这里有关于java Timer的例子。
     *
     * 三十二、什么是线程池？如何创建一个Java线程池？
     * 一个线程池管理了一组工作线程，同时它还包括了一个用于放置等待执行的任务的队列。
     * java.util.concurrent.Executors提供了一个 java.util.concurrent.Executor接口的实现用于创建线程池。线程池例子展现了如何创建和使用线程池，
     * 或者阅读ScheduledThreadPoolExecutor例子，了解如何创建一个周期任务。
     *
     * 三十三、什么是原子操作？在Java Concurrency API中有哪些原子类(atomic classes)？
     *
     * 原子操作是指一个不受其他操作影响的操作任务单元。原子操作是在多线程环境下避免数据不一致必须的手段。
     * int++并不是一个原子操作，所以当一个线程读取它的值并加1时，另外一个线程有可能会读到之前的值，这就会引发错误。
     * 为了解决这个问题，必须保证增加操作是原子的，在JDK1.5之前我们可以使用同步技术来做到这一点。到JDK1.5，java.util.concurrent.atomic包提供了int和long类型的装类，
     * 它们可以自动的保证对于他们的操作是原子的并且不需要使用同步。可以阅读这篇文章来了解Java的atomic类。
     *
     * 三十四、Java Concurrency API中的Lock接口(Lock interface)是什么？对比同步它有什么优势？
     *
     * Lock接口比同步方法和同步块提供了更具扩展性的锁操作。他们允许更灵活的结构，可以具有完全不同的性质，并且可以支持多个相关类的条件对象。
     * 它的优势有：
     * 可以使锁更公平
     * 可以使线程在等待锁的时候响应中断
     * 可以让线程尝试获取锁，并在无法获取锁的时候立即返回或者等待一段时间
     * 可以在不同的范围，以不同的顺序获取和释放锁
     *
     * 三十五、什么是Executors框架？
     *
     * Executor框架同java.util.concurrent.Executor 接口在Java 5中被引入。Executor框架是一个根据一组执行策略调用，调度，执行和控制的异步任务的框架。
     * 无限制的创建线程会引起应用程序内存溢出。所以创建一个线程池是个更好的的解决方案，因为可以限制线程的数量并且可以回收再利用这些线程。
     * 利用Executors框架可以非常方便的创建一个线程池，阅读这篇文章可以了解如何使用Executor框架创建一个线程池。
     *
     * 三十六、什么是阻塞队列？如何使用阻塞队列来实现生产者-消费者模型？
     *
     * java.util.concurrent.BlockingQueue的特性是：当队列是空的时，从队列中获取或删除元素的操作将会被阻塞，或者当队列是满时，往队列里添加元素的操作会被阻塞。
     * 阻塞队列不接受空值，当你尝试向队列中添加空值的时候，它会抛出NullPointerException。
     * 阻塞队列的实现都是线程安全的，所有的查询方法都是原子的并且使用了内部锁或者其他形式的并发控制。
     * BlockingQueue接口是java collections框架的一部分，它主要用于实现生产者-消费者问题。
     *
     * 三十七、什么是Callable和Future?
     *
     * Java 5在concurrency包中引入了java.util.concurrent.Callable 接口，它和Runnable接口很相似，但它可以返回一个对象或者抛出一个异常。
     * Callable接口使用泛型去定义它的返回类型。Executors类提供了一些有用的方法去在线程池中执行Callable内的任务。
     * 由于Callable任务是并行的，我们必须等待它返回的结果。java.util.concurrent.Future对象为我们解决了这个问题。在线程池提交Callable任务后返回了一个Future对象，
     * 使用它我们可以知道Callable任务的状态和得到Callable返回的执行结果。Future提供了get()方法让我们可以等待Callable结束并获取它的执行结果。
     *
     * 三十八、什么是FutureTask?
     *
     * FutureTask是Future的一个基础实现，我们可以将它同Executors使用处理异步任务。通常我们不需要使用FutureTask类，
     * 单当我们打算重写Future接口的一些方法并保持原来基础的实现是，它就变得非常有用。我们可以仅仅继承于它并重写我们需要的方法。阅读Java FutureTask例子，学习如何使用它。
     *
     * 三十九、什么是并发容器的实现？
     * Java集合类都是快速失败的，这就意味着当集合被改变且一个线程在使用迭代器遍历集合的时候，迭代器的next()方法将抛出ConcurrentModificationException异常。
     * 并发容器支持并发的遍历和并发的更新。
     * 主要的类有ConcurrentHashMap, CopyOnWriteArrayList 和CopyOnWriteArraySet，阅读这篇文章了解如何避免ConcurrentModificationException。
     *
     * 四十、Executors类是什么？
     * Executors为Executor，ExecutorService，ScheduledExecutorService，ThreadFactory和Callable类提供了一些工具方法。
     * Executors可以用于方便的创建线程池
     *
     * 四十一、并发三要素？
     *
     * 原子性：在一个操作中，CPU 不可以在中途暂停然后再调度，即不被中断操作，要么执行完成，要么就不执行。
     * 可见性：多个线程访问同一个变量时，一个线程修改了这个变量的值，其他线程能够立即看得到修改的值。
     * 有序性：程序执行的顺序按照代码的先后顺序执行。
     *
     * 四十二、Synchronized 缺点

     *
     * 四十三．AsyncTask的优点和缺点？
     * AsyncTask,是android提供的轻量级的异步类,可以直接继承AsyncTask,在类中实现异步操作,
     * 并提供接口反馈当前异步执行的程度(可以通过接口实现UI进度更新),最后反馈执行的结果给UI主线程.
     * 使用的优点:
     * 简单,快捷
     * 过程可控
     * 使用的缺点:
     * 在使用多个异步操作和并需要进行Ui变更时,就变得复杂起来.
     * Handler异步实现的原理和适用的优缺点
     * 在Handler 异步实现时,涉及到 Handler, Looper, Message,Thread四个对象，
     * 实现异步的流程是主线程启动Thread（子线程）运行并生成Message-Looper获取Message并传递给HandlerHandler逐个获取Looper中的Message，并进行UI变更。
     * 使用的优点：
     * 结构清晰，功能定义明确
     * 对于多个后台任务时，简单，清晰
     * 使用的缺点：
     * 在单个后台异步处理时，显得代码过多，结构过于复杂（相对性）
     *
     * AsyncTask介绍
     * Android的AsyncTask比Handler更轻量级一些（只是代码上轻量一些，而实际上要比handler更耗资源），适用于简单的异步处理。
     * 首先明确Android之所以有Handler和AsyncTask，都是为了不阻塞主线程（UI线程），且UI的更新只能在主线程中完成，因此异步处理是不可避免的。
     * Android为了降低这个开发难度，提供了AsyncTask。AsyncTask就是一个封装过的后台任务类，顾名思义就是异步任务。
     * AsyncTask直接继承于Object类，位置为android.os.AsyncTask。要使用AsyncTask工作我们要提供三个泛型参数，并重载几个方法(至少重载一个)。
     * AsyncTask定义了三种泛型类型 Params，Progress和Result。
     * ● Params 启动任务执行的输入参数，比如HTTP请求的URL。
     * ● Progress 后台任务执行的百分比。
     * ● Result 后台执行任务最终返回的结果，比如String。
     * 使用过AsyncTask 的同学都知道一个异步加载数据最少要重写以下这两个方法：
     * ● doInBackground(Params…) 后台执行，比较耗时的操作都可以放在这里。注意这里不能直接操作UI。此方法在后台线程执行，完成任务的主要工作，通常需要较长的时间。
     * 在执行过程中可以调用publicProgress(Progress…)来更新任务的进度。
     * ● onPostExecute(Result) 相当于Handler 处理UI的方式，在这里面可以使用在doInBackground 得到的结果处理操作UI。 此方法在主线程执行，任务执行的结果作为此方法的参数返回
     * 有必要的话你还得重写以下这三个方法，但不是必须的：
     * ● onProgressUpdate(Progress…) 可以使用进度条增加用户体验度。 此方法在主线程执行，用于显示任务执行的进度。
     * ● onPreExecute() 这里是最终用户调用Excute时的接口，当任务执行之前开始调用此方法，可以在这里显示进度对话框。
     * ● onCancelled() 用户调用取消时，要做的操作
     * 使用AsyncTask类，以下是几条必须遵守的准则：
     * ● Task的实例必须在UI thread中创建；
     * ● execute方法必须在UI thread中调用；
     * ● 不要手动的调用onPreExecute(), onPostExecute(Result)，doInBackground(Params...), onProgressUpdate(Progress...)这几个方法；
     * ● 该task只能被执行一次，否则多次调用时将会出现异常；
     *
     * Handler介绍
     * 一、 Handler主要接受子线程发送的数据, 并用此数据配合主线程更新UI.
     * 当应用程序启动时，Android首先会开启一个主线程, 主线程为管理界面中的UI控件，进行事件分发,更新UI只能在主线程中更新，子线程中操作是危险的。这个时候，
     * Handler就需要出来解决这个复杂的问题。由于Handler运行在主线程中(UI线程中),它与子线程可以通过Message对象来传递数据,
     * 这个时候，Handler就承担着接受子线程传过来的(子线程用sedMessage()方法传递)Message对象(里面包含数据), 把这些消息放入主线程队列中，配合主线程进行更新UI。
     * 二、Handler的特点
     * Handler可以分发Message对象和Runnable对象到主线程中, 每个Handler实例,都会绑定到创建他的线程中,
     * 它有两个作用:
     * (1)安排消息或Runnable 在某个主线程中某个地方执行
     * (2)安排一个动作在不同的线程中执行
     * Handler中分发消息的一些方法
     * post(Runnable)
     * postAtTime(Runnable,long)
     * postDelayed(Runnable long)
     * sendEmptyMessage(int)
     * sendMessage(Message)
     * sendMessageAtTime(Message,long)
     * sendMessageDelayed(Message,long)
     * 以上post类方法允许你排列一个Runnable对象到主线程队列中,
     * sendMessage类方法, 允许你安排一个带数据的Message对象到队列中，等待更新.
     * 综上所述：数据简单使用AsyncTask:实现代码简单，数据量多且复杂使用handler+thread :相比较AsyncTask来说能更好的利用系统资源且高效。
     *
     * 四十四、子线程能否更新UI？为什么？
     *
     * 四十五、为什么在子线程中创建Handler会抛异常？
     */

}
