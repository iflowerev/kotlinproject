package com.jay.studyproject.interview;
/**
 * 基础框架MVC-MVP-MVVM
 */
public class Demo7 {

    /**
     *
     *
     * 一、简述MVC ？
     * MVC的全称是Model-View-Controller，也就是模型-视图-控制器。
     * 定义：
     * Model层：一些数据处理的工作，比如网络数据请求、数据库操作等。
     * View层：一般由XML布局文件充当。
     * Controller层：通常由Activity、Fragment充当，并在其中进行界面、数据相关的业务处理。
     * 可见在Android中，作为Controller的Activity或Fragment主要起到的作用就是解耦，将View和Model进行分离，两者在Controller中完成具体的操作。
     * MVC缺点：
     * (1)耦合性高：MVC的耦合性还是相对较高的，Activity或Fragment并非标准的Controller，它一方面用来控制了布局，另一方面还要在Activity中写业务逻辑代码，
     * 造成了Activity或Fragment既像View又像Controller，View和Model之间可以互相访问，从而三者间构成回路。
     * (2)Controller臃肿：核心的业务逻辑都写在Controller中，导致Controller中的代码略显臃肿，
     * 这也是我们开发中使用MVC模式所面对的问题，Activity或Fragment中的业务逻辑代码代码少则几百行多则上千行。
     *
     *
     * 二、简述MVP？
     * MVP是MVC架构的一个演化版，全称是Model-View-Presenter。
     * 定义：
     * Model层：同样提供数据操作的功能
     * View层：由Activity、Fragment、或某个View控件担任
     * Presenter层：作为View层和Model层沟通的中介。
     * 通常在View层中，有一个Presenter成员变量，同时我们的View（可以是Activity、Fragment）需要实现一个接口，
     * 这样可以将View层中需要的业务逻辑操作通过该接口转交给Presenter来实现，进而Presenter通过Model得到相应的数据，
     * 并通过View层实现的接口返回到View中。这样View层和Model层的耦合就解除了，同时也将业务逻辑从View中抽离出来，转移到Presenter中，避免了Activity或Fragment过度臃肿，充斥大量业务逻辑代码。
     * MVP的优缺点：
     * 优点：
     * 1.低耦合，实现了 Model 和 View 真正的完全分离，可以修改 View 而不影响 Model；
     * 2.模块职责划分明显，层次清晰；
     * 3.可以修改视图而不影响数据模型；
     * 4.Presenter 可复用，一个 Presenter 可以用于多个 View，而不需要更改 Presenter 的逻辑；
     * 5.利于测试驱动开发，以前的Android开发是难以进行单元测试的；
     * View 可以进行组件化，在MVP当中，View 不依赖 Model。
     * 缺点：
     * 1. Presenter 中除了应用逻辑以外，还有大量的 View——>Model，Model——>View 的手动同步逻辑，造成 Presenter 比较笨重，维护起来会比较困难；
     * 2.由于对视图的渲染放在了 Presenter 中，所以视图和 Presenter 的交互会过于频繁，.如果 Presenter 过多地渲染了视图，往往会使得它与特定的视图的联系过于紧密，
     * 一旦视图需要变更，那么Presenter也需要变更了。
     *
     *
     * 三、简述MVVM ?
     * 数据双向绑定，通过数据驱动UI，M提供数据，V视图，VM即数据驱动层
     *
     *
     * 定义：
     * Model层：Model层保存了业务数据与处理方法，提供最终所需数据
     * View层：对应Activity以及XML，但是比起MVC与MVP框架中的View层，更加简洁
     * ViewModel：负责实现View与Model的交互，将两者分离
     * View层和viewModel层通过DataBinding进行了双向绑定，所以Model数据的更改会通过viewModel表现在View上；反之,View收到操作，
     * 通过viewmodel去Model获取数据，且viewModel不持有View，这就使得视图和控制层之间的耦合程度进一步降低，关注点分离更为彻底，同时减轻了Activity的压力。
     * 所以ViewModel就是View和Model之间沟通的桥梁，根据具体情况处理View或Model的变化，解决了View和Model之间的耦合问题，让操作变得更加灵活、方便。
     *
     *
     * MVVM 的优点和缺点：
     * 优点：
     * 1.低耦合：视图（View）可以独立于Model变化和修改，一个 ViewModel 可以绑定到不同的 View 上，当 View 变化的时候 Model 可以不变，当 Model 变化的时候 View 也可以不变。
     * 2.viewModel可复用：你可以把一些相似的逻辑放在一个 viewModel 里面，让很多相似功能的 view 都去绑定这同一个viewModel,重用这段视图逻辑。
     * 3.ViewModel中解决了MVP中V-P互相持有引用的问题
     * 4.独立开发：可以修改视图而不影响数据模型，开发人员可以专注于业务逻辑和数据的开发（ViewModel），设计人员可以专注于页面设计。
     * 5.可测试：界面素来是比较难于测试的，而现在测试可以针对 ViewModel 来写
     *
     * 缺点
     * 1.数据绑定使得 Bug 很难被调试。你看到界面显示异常（不是崩溃）了，有可能是你 View 的代码有 Bug，也可能是 viewModel 的代码有问题。
     * 数据绑定使得一个位置的 Bug 被快速传递到别的位置，要定位原始出问题的地方就变得不那么容易了。
     * 2.一个大的模块中，viewModel也会很大，并且viewModel持有Model的依赖，长期持有，不释放内存，就造成了花费更多的内存。
     * 3.数据双向绑定不利于View层代码重用。客户端开发最常用的重用是view布局，但是数据双向绑定技术，让你在一个View都绑定了一个viewModel，不同模块的viewModel都不同。那就不能简单重用View了。
     *
     *
     * 四、MVC 和 MVP 的区别？
     * MVC 中是允许 Model 和 View 直接进行交互的，而MVP中，Model 与 View 之间的交互由Presenter完成；
     * MVP 模式就是将 P 定义成一个存放接口方法的地方，然后在每个触发的事件中调用对应接口方法来处理，也就是将逻辑放进了 P 中，需要执行某些操作的时候调用 P 的方法就行了。/。-
     *
     *
     * 五、MVVM和MVP区别?
     * ViewModel 承担了 Presenter 中与 view和 Model 交互的职责，与 MVP模式不同的是，ViewModel 与 View 之间是通过 Databinding 实现的，
     * 而Presenter 是持有 View 的对象，直接调用 View 中的一些接口方法来实现。
     * ViewModel可以理解成是View的数据模型和Presenter的合体。它通过双向绑定(松耦合)解决了MVP中Presenter与View联系比较紧密的问题。
     *
     *
     * 六、MVP中你是如何处理Presenter层以防止内存泄漏的？
     * MVP模式为什么会存在内存泄漏的隐患？
     * 当用户按返回键时，页面Activity退出，如果Model在子线程上执行耗时任务，还没有结束，Model持有Presenter的引用，Presenter持有Activity的引用。那么这个Activity对象就没有办法被回收。
     *
     * 怎么解决这个内存泄漏？
     * 在Activity的onDestroy方法中，通过Presenter间接将Model中的耗时任务取消，然后将Presenter和Model置空。
     *
     * 既然这样可以解决内存泄漏，为什么还要用弱引用（也有代码中用软引用）？
     * 因为onDestroy方法不一定立即执行，有可能主线程有大量的任务要处理，来不及执行onDestroy()方法。在此期间，如果GC触发，
     * Activity对象还是无法被回收。而通过弱引用，将Activity对象转换为弱引用对象，GC一旦运行，不管内存是否充足，都会回收弱引用对象。从而解决内存泄漏。
     *
     * 面试题：
     * 1.堆栈和队列区别
     *
     * 堆和栈的区别
     * 1、数据结构的不同：堆可以看看作一棵树，这个数据结构常用于堆排序；栈是一种先进后出的数据结构；
     * 2、可见度的不同：栈内存归属于单个线程，每个线程都会有一个栈内存，其存储的变量只能在其所属线程中可见，即栈内存可以理解成线程的私有内存。而堆内存中的对象对所有线程可见。堆内存中的对象可以被所有线程访问。
     * 3、存储变量的不同：基本数据类型、局部变量是存放在栈内存中的，用完自动消失；而new创建的实例对象以及数组是放在堆内存中的，用完靠垃圾回收机制去清理；
     * 4、生命周期的不同：栈内存的更新速度要快于堆内存，因为局部变量的生命周期很短。栈内存存放的变量生命周期一旦结束就会被释放，而堆内存存放的实体会被垃圾回收机制不定时的回收。
     * 5、所抛异常的不同：如果栈内存没有可用的空间存储方法调用和局部变量，JVM会抛出java.lang.StackOverFlowError；而如果是堆内存没有可用的空间存储生成的对象，JVM会抛出java.lang.OutOfMemoryError。
     *
     * 栈和队列的区别
     * 1、栈的插入和删除只允许在表尾一端进行，而队列允许在表尾一端进行插入，在表头一端进行删除；
     * 2、栈是先进后出的数据结构；队列是先进先出的数据结构；
     *
     * 2.MVVM相关的问题
     * 3.handler原理
     * 4.activity四种启动模式
     * 5.activity中的onNewIntent()方法什么时候执行
     * 6.项目中的技术框架选型
     * 7.目前在做的项目情况
     */
}
