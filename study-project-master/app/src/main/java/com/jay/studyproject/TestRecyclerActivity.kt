package com.jay.studyproject
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import com.jay.loglibrary.HiLog
import com.jay.studyproject.adapter.ListAdapter
import com.jay.studyproject.databinding.ActivityRecyclerMainBinding
import com.jay.studyproject.databinding.ItemLayoutBinding
/**
 * 滚动列表
 */
class TestRecyclerActivity: AppCompatActivity(){

    private lateinit var binding: ActivityRecyclerMainBinding

    private lateinit var rootBinding: ItemLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerMainBinding.inflate(layoutInflater)
        rootBinding= ItemLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mData = mutableListOf<String>()
        for(i in 0..29) {
            mData.add("加载的第" + i + "条数据")
        }
        val mAdapter= ListAdapter(this,mData)
       // 实例化头部View
        binding.recycleview.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        //设置
         val rootView = LayoutInflater.from(this).inflate(R.layout.item_layout,   binding.recycleview, false)
        binding.recycleview.addFooterView(rootView)
        binding.recycleview.adapter = mAdapter
        rootView.setOnClickListener {
            HiLog.e("====点击了======")
        }
        binding.recycleview.setOnClickListener {
        }
    }
}