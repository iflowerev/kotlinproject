package com.jay.studyproject.interview;
/**
 * 实践操作
 */
public class Demo8 {

    /**
     * 1.如何定位ANR问题
     *  线下定位：
     *  方法一: 打开logcat窗口就可以查看 例如: Input dispatching timed out 错误
     *  分析：定位anr的具体位置
     *  log中搜索关键字 "ANR in" ,快速定位出现anr的应用，查看 "Reason"定位anr原因，分析 "CPU usage"信息，查看是否哪个进程占用cpu过高
     *  方法二: 查看trace日志，路径为 /data/anr/xxx 高版本(Android 11及以上)已经无法查看没有权限，需要root权限
     *  线上定位：
     *  使用第三方的监控SDk，比如 Bugly监控(Crash、ANR)
     *  自定义监控
     *  使用WatchDog(看门狗) 这个是Android系统中的一种监控机制，当SystemServer进程启动，调用start方法之后，WatchDog也就启动了run方法
     *  从上面这张图可以理解WatchDog的原理：首先WatchDog是一个线程，每隔5s发送一个Message消息到主线程的MessageQueue中，
     *  主线程Looper从消息队列中取出Message，如果没有阻塞，那么在5s内会执行这个Message任务，就没有ANR；如果超过5s没有执行，那么就有可能出现ANR。
     *
     * 2.如何定位内存泄漏
     *  线下定位：
     *  Profiler工具 Memory
     *  要捕获堆转储，在 Memory Profiler 工具栏中点击 Dump Java heap
     *  Shallow Size：表示对象在没有引用其他对象的情况下本身占用的内存大小。
     *  Retained Size：是指对象自己本身的Shallow heap的大小+对象直所引用到的对象的大小。
     *  将堆转储另存为hprof文件
     *  第三方工具:
     *  LeakCanary是比较方便的用于检测内存泄漏的内嵌工具
     *  线上定位：
     *
     * 3.如何定位内存溢出(OOM)
     *  线下定位:
     *
     *  线上定位:
     *
     * 4.如何定位内存抖动
     *  线下定位:
     *
     *  线上定位:
     *
     *
     * 5.如何定位混淆代码出现的问题
     *  线下定位:
     *
     *  线上定位:
     *
     * 6.如何定位Native层问题
     *
     * 线下定位:
     *
     * 线上定位:
     *
     */



    /**
     * ANR报错日志
     * ActivityManager         system_server                        E  ANR in com.jay.studyproject (com.jay.studyproject/.MainActivity)
     *                                                                                                     PID: 3631
     *                                                                                                     Reason: Input dispatching timed out (Waiting to send non-key event because the touched window has not finished processing certain input events that were delivered to it over 500.0ms ago.  Wait queue length: 6.  Wait queue head age: 9586.2ms.)
     *                                                                                                     Load: 6.09 / 6.03 / 5.99
     *                                                                                                     CPU usage from 0ms to 12107ms later (2024-03-13 09:42:45.966 to 2024-03-13 09:42:58.073):
     *                                                                                                       1.4% 1570/system_server: 0.7% user + 0.6% kernel / faults: 2643 minor 41 major
     *                                                                                                       1.3% 1343/logd: 0% user + 1.3% kernel / faults: 2 minor
     *                                                                                                       0% 1432/ldinit: 0% user + 0% kernel
     *                                                                                                       0.4% 2343/adbd: 0% user + 0.4% kernel / faults: 16963 minor
     *                                                                                                       0.4% 3631/com.jay.studyproject: 0.3% user + 0.1% kernel / faults: 1802 minor 37 major
     *                                                                                                       0% 1//init: 0% user + 0% kernel / faults: 6 minor 1 major
     *                                                                                                       0.4% 1717/com.android.systemui: 0.3% user + 0% kernel / faults: 1952 minor 3 major
     *                                                                                                       0.3% 1418/android.hardware.graphics.composer@2.1-service: 0% user + 0.2% kernel
     *                                                                                                       0% 1455/media.codec: 0% user + 0% kernel / faults: 1214 minor 7 major
     *                                                                                                       0.2% 1875/com.android.phone: 0.1% user + 0% kernel / faults: 765 minor 2 major
     *                                                                                                       0.1% 1407/android.hardware.audio@2.0-service: 0% user + 0.1% kernel
     *                                                                                                       0.1% 1408/android.hardware.bluetooth@1.0-service-qti: 0% user + 0% kernel
     *                                                                                                       0% 1707/com.android.bluetooth: 0% user + 0% kernel / faults: 115 minor
     *                                                                                                       0% 2149/com.android.coreservice: 0% user + 0% kernel / faults: 684 minor 1 major
     *                                                                                                       0% 7/rcu_preempt: 0% user + 0% kernel
     *                                                                                                       0% 1397/netd: 0% user + 0% kernel
     *                                                                                                       0% 1416/android.hardware.gnss@1.0-service: 0% user + 0% kernel
     *                                                                                                       0% 1424/android.hardware.sensors@1.0-service: 0% user + 0% kernel
     *                                                                                                       0% 1431/android.hardware.wifi@1.0-service: 0% user + 0% kernel
     *                                                                                                       0% 1433/audioserver: 0% user + 0% kernel / faults: 24 minor 4 major
     *                                                                                                       0% 1450/media.metrics: 0% user + 0% kernel / faults: 54 minor
     *                                                                                                       0% 2144/com.android.se: 0% user + 0% kernel / faults: 795 minor 1 major
     *                                                                                                       0% 2993/kworker/0:0: 0% user + 0% kernel
     *                                                                                                       0% 3708/process-tracker: 0% user + 0% kernel
     *                                                                                                      +0% 3757/crash_dump64: 0% user + 0% kernel
     *                                                                                                      +0% 3759/droid.bluetooth: 0% user + 0% kernel
     *                                                                                                     4.3% TOTAL: 0.6% user + 0.8% kernel + 2.6% iowait + 0.1% softirq
     *                                                                                                     CPU usage from 3ms to 236ms later (2024-03-13 09:42:45.968 to 2024-03-13 09:42:46.202):
     *                                                                                                       17% 1570/system_server: 8.7% user + 8.7% kernel / faults: 544 minor 1 major
     *                                                                                                         8.7% 1579/HeapTaskDaemon: 4.3% user + 4.3% kernel
     *                                                                                                         4.3% 1576/ReferenceQueueD: 4.3% user + 0% kernel
     *                                                                                                         4.3% 1584/ActivityManager: 0% user + 4.3% kernel
     *                                                                                                       4.3% 1418/android.hardware.graphics.composer@2.1-service: 0% user + 4.3% kernel
     *                                                                                                     4.3% TOTAL: 2.1% user + 2.1% kernel
     */

}
