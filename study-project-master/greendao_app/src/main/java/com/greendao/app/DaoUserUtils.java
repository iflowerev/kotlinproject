package com.greendao.app;

import java.util.List;

/**
 * 具体的Dao操作类
 */
public class DaoUserUtils {
    private volatile static DaoUserUtils instance;

    public CommonDaoUtils<User> userCommonDaoUtils;

    public static DaoUserUtils getInstance()
    {
        synchronized (DaoUserUtils.class) {
            if(instance==null){
                instance = new DaoUserUtils();
            }
        }

        return instance;
    }

    private DaoUserUtils()
    {
        DaoManager mManager = DaoManager.getInstance();

        userCommonDaoUtils = new CommonDaoUtils(User.class,mManager.getDaoSession().getUserDao());

    }


    //新建用户
    public void daoInsertDefaultUser(){
        String account="boss1";
        String password="123456";
        if(daoQueryAllUser().size()==0){
            User user= new User();
            user.setAccount(account);
            user.setPassword(password);
            userCommonDaoUtils.insert(user);
        }
    }

    //查询用户
    public List<User> daoQueryAllUser(){
        return userCommonDaoUtils.queryAll();
    }

    //查询用户
    public User daoQueryUser(long id){
        return userCommonDaoUtils.queryById(id);
    }


    //删除用户
    public boolean deleteAllUser(){
        return userCommonDaoUtils.deleteAll();
    }

    //更新用户
    public boolean updateUser(User user){
        return userCommonDaoUtils.update(user);
    }

}
