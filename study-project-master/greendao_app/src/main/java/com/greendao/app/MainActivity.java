package com.greendao.app;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.greendao.app.databinding.ActivityMainBinding;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mainBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding=ActivityMainBinding.inflate(LayoutInflater.from(this));
        setContentView(mainBinding.getRoot());
        mainBinding.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("点击了","添加");
                DaoUserUtils.getInstance().daoInsertDefaultUser();
            }
        });

        mainBinding.tvGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("点击了","获取");
                List<User> mUser=DaoUserUtils.getInstance().daoQueryAllUser();
                mainBinding.tvUser.setText(mUser.get(0).getAccount()+"-------"+mUser.get(0).getPassword());
            }
        });

    }
}