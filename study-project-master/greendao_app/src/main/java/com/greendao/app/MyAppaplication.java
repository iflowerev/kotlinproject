package com.greendao.app;

import android.app.Application;

public class MyAppaplication  extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DaoManager.getInstance().init(this);
    }
}
