package com.jay.refresh;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * 辅助监控用户手势
 */
public class HiGestureDetector implements GestureDetector.OnGestureListener {

    /**
     * 用户轻触触摸屏
     */
    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    /**
     * 用户轻触触摸屏，尚未松开或拖动
     */
    @Override
    public void onShowPress(MotionEvent e) {

    }

    /**
     * 用户（轻触触摸屏后）松开
     */
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    /**
     * 用户按下触摸屏，并拖动
     */
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    /**
     * 用户长按触摸屏
     */
    @Override
    public void onLongPress(MotionEvent e) {

    }

    /**
     * 用户按下触摸屏、快速移动后松开
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
