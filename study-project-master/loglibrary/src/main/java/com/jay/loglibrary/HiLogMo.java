package com.jay.loglibrary;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @ClassName: HiLogMo
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/5 9:19 下午
 */
public class HiLogMo {

    public static SimpleDateFormat sdf=new SimpleDateFormat("yy-MM-dd HH:mm:ss", Locale.CHINA);
    public long timeMills;
    public int level;
    public String tag;
    public String log;

    public HiLogMo(long timeMills, int level, String tag, String log) {
        this.timeMills = timeMills;
        this.level = level;
        this.tag = tag;
        this.log = log;
    }

    public String getFlattend(){
        return format(timeMills)+'|'+level+'|'+tag+"|:";
    }

    public String format(long timeMills){
        return sdf.format(timeMills);
    }
}
