package com.jay.loglibrary;

/**
 * @ClassName: HiLogFormatter
 * @Description:
 * @Author: yangjie
 * @Date: 2022/4/3 8:21 下午
 */
public interface HiLogFormatter<T> {
    String format(T data);
}
