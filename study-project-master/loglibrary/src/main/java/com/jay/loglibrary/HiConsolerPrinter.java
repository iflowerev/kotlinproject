package com.jay.loglibrary;

import android.util.Log;

import androidx.annotation.NonNull;

import static com.jay.loglibrary.HiLogConfig.MAX_LEN;

/**
 * @ClassName: HiConsolerPrinter
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/3 9:28 下午
 */
public class HiConsolerPrinter  implements  HiLogPrinter{
    @Override
    public void print(@NonNull HiLogConfig config, int level, String tag, @NonNull String printString) {
        int len =printString.length();
        int countOfSub=len/MAX_LEN;
        if(countOfSub>0){
            int index=0;
            for(int i=0;i<countOfSub;i++){
                Log.println(level,tag,printString.substring(index,index+MAX_LEN));
                index+=MAX_LEN;
            }

            if(index!=len){
                Log.println(level,tag,printString.substring(index,len));
            }
        }else{
            Log.println(level,tag,printString);
        }
    }
}
