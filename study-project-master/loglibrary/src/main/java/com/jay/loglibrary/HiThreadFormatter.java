package com.jay.loglibrary;

/**
 * @ClassName: HiThreadFormatter
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/3 8:22 下午
 */
public class HiThreadFormatter  implements  HiLogFormatter<Thread>{

    @Override
    public String format(Thread data) {
        return "Thread:"+data.getName();
    }

}
