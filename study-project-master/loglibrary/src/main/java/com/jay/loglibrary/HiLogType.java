package com.jay.loglibrary;

import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;

/**
 * @ClassName: HiLogType
 * @Description: 日志类型级别
 * @Author: yangjie
 * @Date: 2022/4/3 6:54 下午
 */
public class HiLogType{
    @IntDef({V,D,I,W,E,A})
    @Retention(RetentionPolicy.SOURCE)
    public  @interface TYPE{

    }
    public final  static  int  V= Log.VERBOSE;

    public final  static  int  D= Log.DEBUG;

    public final  static  int  I= Log.INFO;

    public final  static  int  W= Log.WARN;

    public final  static  int  E= Log.ERROR;

    public final  static  int  A= Log.ASSERT;
}
