package com.jay.loglibrary;

import androidx.annotation.NonNull;

/**
 * @ClassName: HiLogPrinter
 * @Description:
 * @Author: yangjie
 * @Date: 2022/4/3 8:20 下午
 */
public interface HiLogPrinter {
    void  print(@NonNull HiLogConfig config,int level,String tag,@NonNull String printString);

}
