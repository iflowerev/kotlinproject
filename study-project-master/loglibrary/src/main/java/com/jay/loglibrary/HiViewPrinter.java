package com.jay.loglibrary;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @ClassName: HiViewPrinter
 * @Description: 将log显示在界面上
 * @Author: yangjie
 * @Date: 2022/4/5 9:10 下午
 */
public class HiViewPrinter implements HiLogPrinter{

    private RecyclerView recyclerView;
    private LogAdapter adapter;
    private HiViewPrinterProvider viewPrinterProvider;

    public HiViewPrinter(Activity activity) {
        FrameLayout rootView= (FrameLayout) activity.findViewById(android.R.id.content);
        recyclerView=new RecyclerView(activity);
        adapter=new LogAdapter(LayoutInflater.from(recyclerView.getContext()));
        LinearLayoutManager layoutManager=new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        viewPrinterProvider=new HiViewPrinterProvider(rootView,recyclerView);
    }

    /**
     * 获取ViewProvider，通过ViewProvider可以控制log视图的展示和隐藏
     *
     * @return ViewProvider
     */
    @NonNull
    public HiViewPrinterProvider getViewProvider() {
        return viewPrinterProvider;
    }


    @Override
    public void print(@NonNull HiLogConfig config, int level, String tag, @NonNull String printString) {
           //将log展示添加到recycleView
        adapter.addItem(new HiLogMo(System.currentTimeMillis(),level,tag,printString));
           //滚动到对应到位置
        recyclerView.smoothScrollToPosition(adapter.getItemCount()-1);

    }

    private static class  LogAdapter extends RecyclerView.Adapter<LogViewHolder>{
        private LayoutInflater inflater;

        private List<HiLogMo> logs=new ArrayList<>();
        public LogAdapter(LayoutInflater inflater){
            this.inflater=inflater;
        }
        void addItem(HiLogMo logItem){
            logs.add(logItem);
            notifyItemChanged(logs.size()-1);
        }
        @NonNull
        @Override
        public LogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView=inflater.inflate(R.layout.hilog_item,parent,false);
            return new LogViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull LogViewHolder holder, int position) {
             HiLogMo logItem=logs.get(position);
             int color=getHighlightColor(logItem.level);
             holder.tagView.setTextColor(color);
             holder.messageView.setTextColor(color);
             holder.tagView.setText(logItem.getFlattend());
             holder.messageView.setText(logItem.log);
        }

        /**
         * 根据log级别获取不同到高亮颜色
         * @return
         */
        private int getHighlightColor(int logLevel){
            int highlight;
            switch (logLevel){
                case HiLogType.V:
                    highlight=0xffbbbbbb;
                    break;
                case HiLogType.D:
                    highlight=0xffffffff;
                    break;
                case HiLogType.I:
                    highlight=0xff6a8759;
                    break;
                case HiLogType.W:
                    highlight=0xffbbb529;
                    break;
                case HiLogType.E:
                    highlight=0xffff6b68;
                    break;
                default:
                    highlight=0xffffff00;
                    break;
            }
            return highlight;
        }
        @Override
        public int getItemCount() {
            return logs.size();
        }
    }
    private static class LogViewHolder extends RecyclerView.ViewHolder{

        TextView tagView;

        TextView messageView;

        public LogViewHolder(@NonNull View itemView) {
            super(itemView);
            tagView=(TextView) itemView.findViewById(R.id.tag);
            messageView=(TextView) itemView.findViewById(R.id.message);
        }
    }
}
