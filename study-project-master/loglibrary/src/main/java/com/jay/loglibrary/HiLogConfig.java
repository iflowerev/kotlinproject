package com.jay.loglibrary;

/**
 * @ClassName: HiLogConfig
 * @Description: Log配置
 * @Author: yangjie
 * @Date: 2022/4/3 7:13 下午
 */
public abstract  class HiLogConfig {

    static  int MAX_LEN=512;

    static HiThreadFormatter HI_THREAD_FORMATTER=new HiThreadFormatter();

    static HiStackTraceFormatter HI_STACK_TRACE_FORMATTER=new HiStackTraceFormatter();


    public JsonParser injectJsonParser(){

        return null;

    }
    public String getGlobalTag(){
        return "HiLog";
    }


    public boolean enable(){
        return true;
    }

    public  boolean includeThread(){
        return false;
    }

    public  int stackTraceDepth(){
        return 5;
    }

    public  HiLogPrinter[] printers(){
        return  null;
    }

    public interface  JsonParser{
        String toJson(Object str);

    }
}
