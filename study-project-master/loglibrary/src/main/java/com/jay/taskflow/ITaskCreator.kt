package com.jay.taskflow

interface ITaskCreator {
    fun  createTask(taskName:String):Task
}