package com.jay.taskflow

interface  TaskListener{
    fun  onStart(task:Task)

    fun  onRunning(task:Task)

    fun  onFinished(task: Task)
}