package com.jay.tab.common;

import android.view.ViewGroup;

import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 外部容器
 */
public interface IHiTabLayout<Tab extends ViewGroup,D> {
    Tab findTab(@NonNull D data);

    void addTabSelectedChangListener(OnTabSelectedListener<D>  listener);

    void defaultSelected(@NonNull D defaultInfo);

    void inflateInfo(@NonNull List<D> infoList);


    interface  OnTabSelectedListener<D>{
        void onTabSelectedListener(int index, @Nullable D pervInfo, @NonNull D nextInfo);
    }
}
