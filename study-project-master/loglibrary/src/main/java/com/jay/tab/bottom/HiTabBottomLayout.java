package com.jay.tab.bottom;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ScrollView;

import com.jay.utils.HiDisplayUtil;
import com.jay.loglibrary.R;
import com.jay.tab.common.IHiTabLayout;
import com.jay.utils.HiViewUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @ClassName: HiTabBottomLayout
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/16 9:32 下午
 */
public class HiTabBottomLayout  extends FrameLayout implements IHiTabLayout<HiTabBottom,HiTabBottomInfo<?>> {

    private List<OnTabSelectedListener<HiTabBottomInfo<?>>> tabSelectedChangeListenerList=new ArrayList<>();
    private HiTabBottomInfo<?> selectedInfo;
    private float bottomAlpha=1f;
    //TabBottom的高度
    private float tabBottomHieght=50;
    //TabBottom的头部线条高度
    private float bottomLineHeight=0.5f;
    //TabBottom的头部线条颜色
    private String bottomLineColor="#dfe0e1";
    private List<HiTabBottomInfo<?>> infoList;

    public HiTabBottomLayout(Context context) {
        super(context);
    }

    public HiTabBottomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HiTabBottomLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public HiTabBottom findTab(@NonNull HiTabBottomInfo<?> info) {
        ViewGroup ll=findViewWithTag(TAG_TAB_BOTTOM);
        for(int i=0;i<ll.getChildCount();i++){
            View child=ll.getChildAt(i);
            if(child instanceof  HiTabBottom){
                HiTabBottom tab=(HiTabBottom) child;
                if(tab.getHiTabInfo()==info){
                    return tab;
                }
            }
        }
        return null;
    }

    @Override
    public void addTabSelectedChangListener(OnTabSelectedListener<HiTabBottomInfo<?>> listener) {
     tabSelectedChangeListenerList.add(listener);
    }

    @Override
    public void defaultSelected(@NonNull HiTabBottomInfo<?> defaultInfo) {
          onSelected(defaultInfo);
    }

    public static final  String TAG_TAB_BOTTOM="TAG_TAB_BOTTOM";
    @Override
    public void inflateInfo(@NonNull List<HiTabBottomInfo<?>> infoList) {
       if(infoList.isEmpty()){
           return;
       }
       this.infoList=infoList;
       //移除之前已经添加的view
        for(int i=getChildCount()-1;i>0;i++){
            removeViewAt(i);
        }
        selectedInfo=null;
        addBackground();
        //清楚之前添加当HiTabBottom listener,Tips:Java foreach remove问题
        Iterator<OnTabSelectedListener<HiTabBottomInfo<?>>> iterator=tabSelectedChangeListenerList.iterator();
        while (iterator.hasNext()){
            if(iterator.next() instanceof  HiTabBottom){
                iterator.remove();
            }
        }

        int height=HiDisplayUtil.dp2px(tabBottomHieght,getResources());
        FrameLayout ll=new FrameLayout(getContext());
        ll.setTag(TAG_TAB_BOTTOM);
        int width=HiDisplayUtil.getDisplayWidthInPx(getContext())/infoList.size();
        for(int i=0;i<infoList.size();i++){
            HiTabBottomInfo<?> info=infoList.get(i);
            //Tips:为何不用LinearLayout:当动态改变child大小后Gravity.BOTTOM会失败
           LayoutParams params=new LayoutParams(width,height);
            params.gravity=Gravity.BOTTOM;
            params.leftMargin=i*width;
            HiTabBottom tabBottom=new HiTabBottom(getContext());
            tabSelectedChangeListenerList.add(tabBottom);
            tabBottom.setHiTabInfo(info);
            ll.addView(tabBottom,params);
            tabBottom.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelected(info);
                }
            });
        }
        LayoutParams flParams=new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        flParams.gravity=Gravity.BOTTOM;
        addBottomLine();
         addView(ll,flParams);
        fitContentView();
    }


    public void setTabAlpha(float alpha) {
        this.bottomAlpha = alpha;
    }

    public void setTabHieght(float tabHieght) {
        this.tabBottomHieght = tabHieght;
    }

    public void setBottomLineHeight(float bottomLineHeight) {
        this.bottomLineHeight = bottomLineHeight;
    }


    public void setBottomLineColor(String bottomLineColor) {
        this.bottomLineColor = bottomLineColor;
    }

    private void addBottomLine(){
        View bottomLine=new View(getContext());
        bottomLine.setBackgroundColor(Color.parseColor(bottomLineColor));
        LayoutParams bottomLineParams=new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,HiDisplayUtil.dp2px(bottomLineHeight));
        bottomLineParams.gravity=Gravity.BOTTOM;
        bottomLineParams.bottomMargin=HiDisplayUtil.dp2px(tabBottomHieght-bottomLineHeight,getResources());
        addView(bottomLine,bottomLineParams);
        bottomLine.setAlpha(bottomAlpha);
    }
    private void onSelected(@NonNull HiTabBottomInfo<?>newInfo){
        for(OnTabSelectedListener<HiTabBottomInfo<?>> listener:tabSelectedChangeListenerList){
            listener.onTabSelectedListener(infoList.indexOf(newInfo),selectedInfo,newInfo);
        }
        this.selectedInfo=newInfo;
    }
    /**
     * 背景色
     */
   private void addBackground(){
       View view= LayoutInflater.from(getContext()).inflate(R.layout.hi_bottom_layout_bg,null);
       LayoutParams params=new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, HiDisplayUtil.dp2px(tabBottomHieght,getResources()));
       params.gravity= Gravity.BOTTOM;
       addView(view,params);
       view.setAlpha(bottomAlpha);

    }

    /**
     * 修复内容区域当底部Padding
     */
    private void fitContentView(){
       if(!(getChildAt(0) instanceof  ViewGroup)){
           return;
       }
       ViewGroup rootView=(ViewGroup) getChildAt(0);
       ViewGroup targetView= HiViewUtil.findTypeView(rootView, RecyclerView.class);
       if(targetView==null){
           targetView=HiViewUtil.findTypeView(rootView, ScrollView.class);
       }
       if(targetView==null){
           targetView=HiViewUtil.findTypeView(rootView, AbsListView.class);
       }
        if(targetView!=null){
            targetView.setPadding(0,0,0,HiDisplayUtil.dp2px(tabBottomHieght,getResources()));
            targetView.setClipChildren(false);
        }
    }
}
