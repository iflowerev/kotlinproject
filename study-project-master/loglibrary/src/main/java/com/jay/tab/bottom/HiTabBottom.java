package com.jay.tab.bottom;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jay.loglibrary.R;
import com.jay.tab.common.IHiTab;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Px;

/**
 * @ClassName: HiTabBottom
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/11 10:08 下午
 */
public class HiTabBottom extends RelativeLayout implements IHiTab<HiTabBottomInfo<?>>{

    private HiTabBottomInfo<?> tabInfo;
    private ImageView tabImageView;
    private TextView tabIconView;
    private TextView tabNameView;

    public HiTabBottom(Context context) {
        this(context,null);
    }

    public HiTabBottom(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public HiTabBottom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        LayoutInflater.from(getContext()).inflate(R.layout.hi_tab_bottom,this);
        tabImageView=(ImageView) findViewById(R.id.iv_image);
        tabIconView=(TextView) findViewById(R.id.tv_icon);
        tabNameView=(TextView) findViewById(R.id.tv_name);
    }

    @Override
    public void setHiTabInfo(@NonNull HiTabBottomInfo<?> data) {
        this.tabInfo=data;
        inflateInfo(false,true);
    }

    private void inflateInfo(boolean selected,boolean init){
        if(tabInfo.tabType==HiTabBottomInfo.TabType.ICON){
            if(init){
                tabImageView.setVisibility(GONE);
                tabIconView.setVisibility(VISIBLE);
                Typeface typeface=Typeface.createFromAsset(getContext().getAssets(),tabInfo.iconFont);
                tabIconView.setTypeface(typeface);
                if(!TextUtils.isEmpty(tabInfo.name)){
                    tabNameView.setText(tabInfo.name);
                }
            }
            if(selected){
                tabIconView.setText(TextUtils.isEmpty(tabInfo.selectedIconName)?tabInfo.defaultIconName:tabInfo.selectedIconName);
                tabIconView.setTextColor(getTextColor(tabInfo.tintColor));
                tabNameView.setTextColor(getTextColor(tabInfo.tintColor));
            }else{
                tabIconView.setText(tabInfo.defaultIconName);
                tabIconView.setTextColor(getTextColor(tabInfo.defaultColor));
                tabNameView.setTextColor(getTextColor(tabInfo.defaultColor));
            }
        }else if(tabInfo.tabType==HiTabBottomInfo.TabType.BITMAP){
            if(init){
                tabImageView.setVisibility(VISIBLE);
                tabIconView.setVisibility(GONE);
                if(!TextUtils.isEmpty(tabInfo.name)){
                    tabNameView.setText(tabInfo.name);
                }
            }
            if(selected){
                tabImageView.setImageBitmap(tabInfo.selectedBitmap);
            }else{
                tabImageView.setImageBitmap(tabInfo.defaultBitmap);
            }
        }
    }
    public HiTabBottomInfo<?> getHiTabInfo() {
        return tabInfo;
    }

    public TextView getTabNameView() {
        return tabNameView;
    }


    /**
     *改变某个tab的高度
     * @param height
     */
    @Override
    public void resetHeight(@Px int height) {
        ViewGroup.LayoutParams layoutParams=getLayoutParams();
        layoutParams.height=height;
        setLayoutParams(layoutParams);
        getTabNameView().setVisibility(GONE);
    }

    @Override
    public void onTabSelectedListener(int index, @Nullable HiTabBottomInfo<?> pervInfo, @NonNull HiTabBottomInfo<?> nextInfo) {
        if(pervInfo!=tabInfo&&nextInfo!=tabInfo||pervInfo==nextInfo){
            return;
        }
        if(pervInfo==tabInfo){
            inflateInfo(false,false);
        }else{
            inflateInfo(true,false);
        }
    }

    @ColorInt
    private int getTextColor(Object color){
        if(color instanceof  String){
            return Color.parseColor((String)color);
        }else{
            return  (int)color;
        }

    }
}
