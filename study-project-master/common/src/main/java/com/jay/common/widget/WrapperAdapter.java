package com.jay.common.widget;

import androidx.recyclerview.widget.RecyclerView;

public interface  WrapperAdapter {
    public RecyclerView.Adapter getWrappedAdapter() ;
}
