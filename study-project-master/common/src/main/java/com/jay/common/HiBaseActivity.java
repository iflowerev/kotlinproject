package com.jay.common;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @ClassName: HiBaseActivity
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/17 11:53 上午
 */
public class HiBaseActivity  extends AppCompatActivity implements HiBaseActionInterface {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
