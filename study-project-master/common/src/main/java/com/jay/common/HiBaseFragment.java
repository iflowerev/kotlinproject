package com.jay.common;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
/**
 * @ClassName: HiBaseFragment
 * @Description: java类作用描述
 * @Author: yangjie
 * @Date: 2022/4/17 11:53 上午
 */
public abstract  class HiBaseFragment  extends Fragment {
  protected View layoutView;

  @LayoutRes
  public abstract int getLayoutId();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutView=inflater.inflate(getLayoutId(),container,false);
        return layoutView;
    }
}
