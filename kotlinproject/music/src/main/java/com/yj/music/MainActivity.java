package com.yj.music;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.yj.music.agentweb.AgentWebFragment;
import com.yj.music.bean.BannerBean;
import java.util.ArrayList;
import java.util.List;
import cn.bingoogolapple.bgabanner.BGABanner;

public class MainActivity extends AppCompatActivity implements  BGABanner.Delegate<ImageView, Integer>,BGABanner.Adapter<ImageView, Integer> {
    private FragmentManager mFragmentManager;
    private AgentWebFragment mAgentWebFragment;
    private BGABanner banner;
    private ImageView iv_back;
    private TextView toolbar_title,tv_content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //避免切换横竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFragmentManager=getSupportFragmentManager();
        FragmentTransaction ft=mFragmentManager.beginTransaction();
        mAgentWebFragment=new AgentWebFragment();
        //清除缓存
        mAgentWebFragment.toCleanWebCache();
        ft.add(R.id.container_framelayout,mAgentWebFragment);
        ft.commit();
        initView();
    }

    private void initView() {
        banner=findViewById(R.id.banner);
        iv_back=findViewById(R.id.iv_back);
        tv_content=findViewById(R.id.tv_content);
        toolbar_title=findViewById(R.id.toolbar_title);
        setBannerData();
        iv_back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        toolbar_title.setText("唯有运动与轻音乐不可辜负");
        tv_content.setText("低头感知深夜里的歌，之后总会再遇见--壹柒捌");
    }

    private List<String> titleList=new ArrayList<>(); //图片标题
    private List<Integer> imageList=new ArrayList<>(); //图片ID
//    private String [] cotentList={"跌落在这虚拟的世界里，貌似只有身体上的困乏，才能略微感觉像是活着。",
//            "如果你的周围都是灯，那你也得尝试发光，并传递自己的热量。",
//            "若每天都能像今天一样活，我想坚持每天洗澡将不再有多难，即便去就近的河里。",
//            "生活在自己的那片土地里，劳作在田间，滴落的汗水也将滚烫，炽热每一寸沃土。"};
    private int [] resId={R.mipmap.ic_iamge1, R.mipmap.ic_iamge2,R.mipmap.ic_iamge3,R.mipmap.ic_iamge4,R.mipmap.ic_iamge5,R.mipmap.ic_iamge6,R.mipmap.ic_iamge7,R.mipmap.ic_iamge8,R.mipmap.ic_iamge9,R.mipmap.ic_iamge10, R.mipmap.ic_iamge11,R.mipmap.ic_iamge12,R.mipmap.ic_iamge13,R.mipmap.ic_iamge14};
    public void setBannerData(){
        titleList.clear();
        imageList.clear();
        titleList.add("跌落在这虚拟的世界里，");
        titleList.add("貌似只有身体上的困乏，");
        titleList.add("才能略微感觉像是活着。");
        titleList.add("如果你的周围都是灯，");
        titleList.add("那你也得尝试发光，");
        titleList.add("并传递自己的热量。");
        titleList.add("若每天都能像今天一样活，");
        titleList.add("我想坚持每天洗澡将不再有多难，");
        titleList.add("即便去就近的河里。");
        titleList.add("生活在自己的那片土地里，");
        titleList.add("劳作在田间，滴落的汗水也将滚烫，");
        titleList.add("炽热每一寸沃土。");
        for(int i=0;i<12;i++){
            int index=(int)(1+Math.random()*(14-1+1));
            if(index==14){
                imageList.add(resId[index-1]);
            }else{
                imageList.add(resId[index]);
            }

        }
        Log.e(imageList.size()+"----",titleList.size()+"===========");
        banner.setAdapter(this);
        banner.setData(imageList,titleList);
        banner.setDelegate(this);
    }


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if(mAgentWebFragment!=null){
//            return  mAgentWebFragment.onFragmentKeyDown(keyCode,event);
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void fillBannerItem(BGABanner banner, ImageView itemView, @Nullable Integer model, int position) {
        itemView.setImageResource(model);
        Log.e("index",position+"===============");
        if(position==0){
            tv_content.setText(titleList.get(position).replace("，","").replace("。","")+"--壹柒捌");
        }else{
            tv_content.setText(titleList.get(position-1).replace("，","").replace("。","")+"--壹柒捌");
        }

    }

    @Override
    public void onBannerItemClick(BGABanner banner, ImageView itemView, @Nullable Integer model, int position) {
//        //查看大图
//        Log.e("fillBannerItem",position+"---------");
        tv_content.setText(titleList.get(position).replace("，","").replace("。","")+"--壹柒捌");

    }
}