package com.yj.music.bean;

import java.util.List;

public class BannerBean {

    private List<BannerContent> contents;

    public BannerBean(List<BannerContent> contents) {

        this.contents = contents;
    }

    public  class BannerContent{

        String  titleList;

         String imageList;

        public BannerContent(String titleList, String imageList) {
            this.titleList = titleList;
            this.imageList = imageList;
        }
    }
}
