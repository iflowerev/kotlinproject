package jcc.example.com.broweimg.util;

import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.View;

/**
 * 修改图片颜色
 */
public class ChangeImageColorUtils {
    private static ChangeImageColorUtils instance;

    public static ChangeImageColorUtils getInstance() {
        if (instance == null) {
            instance = new ChangeImageColorUtils();
        }
        return instance;
    }
    /**
     * 模糊
     */
    public Bitmap makeBlurBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        /**
         * 模糊遮罩 滤镜效果
         * Blur.NORMAL 内外都模糊绘制
         * Blur.SOLID 内部正常绘制，外部模糊
         * Blur.OUTER 内部模糊，外部不绘制
         * Blur.INNER  内部不绘制，外部模糊
         */
        paint.setMaskFilter(new BlurMaskFilter(50, android.graphics.BlurMaskFilter.Blur.NORMAL));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
    /**
     * 颜色增强
     */
    public Bitmap makeEnhancementBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        /**
         * 透明度不反相
         */
        ColorMatrix matrix = new ColorMatrix(new float[]{
                1.2f, 0, 0, 0, 0,
                0, 1.2f, 0, 0, 0,
                0, 0, 1.2f, 0, 0,
                0, 0, 0, 1.2f, 0,
        });

        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
    /**
     * 反相效果
     */
    public Bitmap makeInverseEffecttBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();

        ColorMatrix matrix = new ColorMatrix(new float[]{
                -1, 0, 0, 0, 255,
                0, -1, 0, 0, 255,
                0, 0, -1, 0, 255,
                0, 0, 0, 1, 0,
        });

        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
    /**
     * 复古
     * @param inputBitmap
     * @return
     */
    public Bitmap makeRetroStyleBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        ColorMatrix matrix = new ColorMatrix(new float[]{
                1/2f, 1/2f, 1/2f, 0, 0,
                1/3f, 1/3f, 1/3f, 0, 0,
                1/4f, 1/4f, 1/4f, 0, 0,
                0, 0, 0, 1f, 0,
        });
        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
    /**
     * 反色
     * @param inputBitmap
     * @return
     */
    public Bitmap makeReverseBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        //反色效果
        ColorMatrix matrix = new ColorMatrix(new float[]{
                0, 1f, 0, 0, 0,
                1f, 0, 0, 0, 0,
                0, 0, 1f, 0, 0,
                0, 0, 0, 1f, 0,
        });
        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }

    /**
     * 黑白图片
     * @param inputBitmap
     * @return
     */
    public Bitmap makeBlackOrWhiteBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        /**
         * 去色原理：只要把RGB三通道的色彩信息设置成一样，即：R=G=B
         * 那么图像就变成了灰色，并且，为了保证图像亮度不变，
         * 同一个通道中的R+G+B=1；如0.213+0.175+0.072 =1;
         * RGB=0.213,0.175,0.072
         * 三个数字是根据色彩光波频率及色彩心理学计算出来的
         */
        ColorMatrix matrix = new ColorMatrix(new float[]{
                0.213f, 0.715f, 0.072f, 0,0,
                0.213f, 0.715f, 0.072f, 0,0,
                0.213f, 0.715f, 0.072f, 0,0,
                0,      0,           0, 1,0,
        });
        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
    /**
     * 通过矩阵变换将一个图片、颜色块，过滤其中的红色、绿色（只留下蓝色）
     *
     * @param inputBitmap
     * @return
     */
    public Bitmap makeBlueBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        ColorMatrix matrix = new ColorMatrix(new float[]{
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 0, 1, 0,
        });
        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
    /**
     * 浮雕
     */
    public Bitmap makeEmbossBitmap(Bitmap inputBitmap) {
        if (inputBitmap == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        /**
         * direction, 指定长度为xxx的数组标量[x,y,z],用来指定光源的位置
         * ambient,   指定周边背景光源(0~1)
         * specular,  指镜面反射系数
         * blurRadius，指定模糊半径
         */
        paint.setMaskFilter(new EmbossMaskFilter(new float[]{400,300,100},0.5f,60,100 ));
        canvas.drawBitmap(inputBitmap, 0, 0, paint);
        return outputBitmap;
    }
}
