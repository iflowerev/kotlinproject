package jcc.example.com.broweimg.ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mi.baselibrary.base.view.BaseActivity;

import jcc.example.com.broweimg.R;
import jcc.example.com.broweimg.widget.MyImage;

public class CooMatrixActivity  extends BaseActivity {
    Button change;

    MyImage sv;

    EditText tv_et0;

    EditText tv_et1;

    EditText tv_et2;

    EditText tv_et3;

    EditText tv_et4;

    EditText tv_et5;

    EditText tv_et6;

    EditText tv_et7;

    EditText tv_et8;

    EditText tv_et9;

    EditText tv_et10;

    EditText tv_et11;

    EditText tv_et12;

    EditText tv_et13;

    EditText tv_et14;

    EditText tv_et15;

    EditText tv_et16;

    EditText tv_et17;

    EditText tv_et18;

    EditText tv_et19;
    private EditText[] et;
    private float []carray=new float[20];
    @Override
    public int getContentView() {
        return R.layout.activity_coomatrix;
    }

    @Override
    public void initView() {
        change=findViewById(R.id.set);
        sv=findViewById(R.id.MyImage);
        tv_et0=findViewById(R.id.tv_et0);
        tv_et1=findViewById(R.id.tv_et1);
        tv_et2=findViewById(R.id.tv_et2);
        tv_et3=findViewById(R.id.tv_et3);
        tv_et4=findViewById(R.id.tv_et4);
        tv_et5=findViewById(R.id.tv_et5);
        tv_et6=findViewById(R.id.tv_et6);
        tv_et7=findViewById(R.id.tv_et7);
        tv_et8=findViewById(R.id.tv_et8);
        tv_et9=findViewById(R.id.tv_et9);
        tv_et10=findViewById(R.id.tv_et10);
        tv_et11=findViewById(R.id.tv_et11);
        tv_et12=findViewById(R.id.tv_et12);
        tv_et13=findViewById(R.id.tv_et13);
        tv_et14=findViewById(R.id.tv_et14);
        tv_et15=findViewById(R.id.tv_et15);
        tv_et16=findViewById(R.id.tv_et16);
        tv_et17=findViewById(R.id.tv_et17);
        tv_et18=findViewById(R.id.tv_et18);
        tv_et19=findViewById(R.id.tv_et19);
        et= new EditText[]{tv_et0, tv_et1, tv_et2, tv_et3, tv_et4, tv_et5, tv_et6, tv_et7, tv_et8,tv_et9,tv_et10,tv_et11,tv_et12,tv_et13,tv_et14,tv_et15,tv_et16,tv_et17,tv_et18,tv_et19};
        change.setOnClickListener(l);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    private Button.OnClickListener l=new Button.OnClickListener(){

        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
            getValues();
            sv.setValues(carray);
            sv.invalidate();
        }

    };
    public   void getValues(){
        for(int i=0;i<20;i++){
            carray[i]=Float.valueOf(et[i].getText().toString());
        }
    }

}
