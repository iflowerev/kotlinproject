package jcc.example.com.broweimg.widget;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;
import jcc.example.com.broweimg.R;
/**
 * 遮罩滤镜处理-----浮雕
 */
public class EmbossMaskFiltersView extends View {

    public EmbossMaskFiltersView(Context context) {
        super(context);
    }

    public EmbossMaskFiltersView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EmbossMaskFiltersView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        //需要关闭硬件加速(没有关闭则没效果)
        setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_iamge1);
        canvas.drawBitmap(bitmap,null,new RectF(0,0,400,400*bitmap.getHeight()/bitmap.getWidth()),paint);
        canvas.translate(400,0);
        /**
         * direction, 指定长度为xxx的数组标量[x,y,z],用来指定光源的位置
         * ambient,   指定周边背景光源(0~1)
         * specular,  指镜面反射系数
         * blurRadius，指定模糊半径
         */
        paint.setMaskFilter(new EmbossMaskFilter(new float[]{400,300,100},0.5f,60,100 ));
        canvas.drawBitmap(bitmap,null,new RectF(0,0,400,400*bitmap.getHeight()/bitmap.getWidth()),paint);
    }
}