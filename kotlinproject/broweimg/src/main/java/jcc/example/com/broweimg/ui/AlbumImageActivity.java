package jcc.example.com.broweimg.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.mi.baselibrary.base.view.BaseActivity;
import java.io.File;
import jcc.example.com.broweimg.R;
import jcc.example.com.broweimg.picturezoom.picman.ImagePagerActivity;
import jcc.example.com.broweimg.picturezoom.utils.AllUtils;
import jcc.example.com.broweimg.util.SystemImageUtils;

/**
 * 相册图片
 */
public class AlbumImageActivity extends BaseActivity {
    public static final int TAKE_PHOTO = 1;//声明一个请求码，用于识别返回的结果
    public static final int CHOOSE_PHOTO = 2;
    String imagePath = null;

    ImageView picture;

    Button btnChoosePhoto;

    Button choose_came;
    private final String filePath = Environment.getExternalStorageDirectory() + File.separator + "output_image.jpg";
    @Override
    public int getContentView() {
        return R.layout.activity_album_image;
    }

    @Override
    public void initView() {
        picture = (ImageView) findViewById(R.id.picture);
        btnChoosePhoto=findViewById(R.id.choose_photo);
        choose_came=findViewById(R.id.choose_came);
        btnChoosePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissino();
            }
        });
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//              requestPermissino();
               //图片放大
                if(TextUtils.isEmpty(imageUrl)){
                    AllUtils.startImagePage(AlbumImageActivity.this, ImagePagerActivity.class,filePath,picture);
                }else{
                    AllUtils.startImagePage(AlbumImageActivity.this, ImagePagerActivity.class,imageUrl,picture);
                }

            }
        });
        choose_came.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageUrl="";
                requestPhotoPermission();
            }
        });
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    /**
     * 动态申请相册权限
     */
    private void requestPermissino() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }else {
            SystemImageUtils.getInstance().openAlbum(this,CHOOSE_PHOTO);
        }
    }

    //动态请求权限
    private void requestPhotoPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //请求权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
        } else {
            //调用
            SystemImageUtils.getInstance().requestCamera(this,filePath,TAKE_PHOTO);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    SystemImageUtils.getInstance().requestCamera(this,filePath,TAKE_PHOTO);
                }else {
                    Toast.makeText(this,"你拒绝了该权限",Toast.LENGTH_SHORT).show();
                }
                break;
            case 2:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    SystemImageUtils.getInstance().openAlbum(this,CHOOSE_PHOTO);
                }else {
                    Toast.makeText(this,"你拒绝了该权限",Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }
    String imageUrl="";
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case TAKE_PHOTO:
                if(resultCode == RESULT_OK){
                    SystemImageUtils.getInstance().setBitmapImage(picture,filePath);
                }
                break;
            case CHOOSE_PHOTO:
                if(resultCode == RESULT_OK){
                    imageUrl= SystemImageUtils.getInstance().getRealPathFromUri(this,data.getData());
                    SystemImageUtils.getInstance().displayImage(imageUrl,picture,this);
                }
                break;
            default:
                break;
        }
    }
}
