package jcc.example.com.broweimg.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;

import jcc.example.com.broweimg.R;

/**
 * 反相效果（类似曝光）
 */
public class InverseEffectView extends View{

    public InverseEffectView(Context context) {
        super(context);
    }

    public InverseEffectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public InverseEffectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //需要关闭硬件加速(没有关闭则没效果)
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.argb(255, 200, 100, 100));
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_iamge1);

        canvas.drawBitmap(bitmap, null,
                new RectF(0, 0, 400, 400 * bitmap.getHeight() / bitmap.getWidth()), paint);

        canvas.translate(400, 0);

        /**
         * 反相效果
         * 透明度不反相
         */
        ColorMatrix matrix = new ColorMatrix(new float[]{
                -1, 0, 0, 0, 255,
                0, -1, 0, 0, 255,
                0, 0, -1, 0, 255,
                0, 0, 0, 1, 0,
        });

        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawRect(0, 0, 400, 400, paint);
        canvas.drawBitmap(bitmap, null,
                new RectF(0, 0, 400, 400 * bitmap.getHeight() / bitmap.getWidth()), paint);

    }
}
