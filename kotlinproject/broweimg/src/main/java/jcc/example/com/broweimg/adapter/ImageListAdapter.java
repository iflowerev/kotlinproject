package jcc.example.com.broweimg.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import jcc.example.com.broweimg.R;
import jcc.example.com.broweimg.util.JImageShowUtil;
import jcc.example.com.broweimg.util.ScreenUtil;

/**
 * 图片列表
 */
public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.MyViewHolder>{

    private Context context;
    private List<String> paths;
   private List<ImageView> imageViews=new ArrayList<>();
    public ImageListAdapter(Context mContext,List<String> mPaths) {
        this.context = mContext;
        this.paths = mPaths;
        imageViews.clear();
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return  new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_image, parent,
                false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.iv_iamge.getLayoutParams();
        params.height = ScreenUtil.getScreenWidth(context)/3-ScreenUtil.dp2px(context,19);
        holder.iv_iamge.setLayoutParams(params);
        JImageShowUtil.displayImage(paths.get(position), holder.iv_iamge);
        imageViews.add(holder.iv_iamge);
        //itemView 的点击事件
        if (mItemClickListener != null) {
            holder.itemView.setOnClickListener(v -> {
                int adapterPosition = holder.getAdapterPosition();
                mItemClickListener.onItemClick(v, adapterPosition,imageViews);
            });
        }
    }
    @Override
    public int getItemCount() {
        return paths.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_iamge;
        public MyViewHolder(View view) {
            super(view);
            iv_iamge=(ImageView) view.findViewById(R.id.iv_image);
        }
    }
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener l) {
        this.mItemClickListener = l;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position, List<ImageView> mImageView);
    }

}
