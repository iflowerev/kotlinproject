package jcc.example.com.broweimg.picturezoom.dialog;

public interface PictureIndeterminate {
    void setAnimationSpeed(float scale);
}
