package jcc.example.com.broweimg.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import jcc.example.com.broweimg.R;

/**
 * 复古风格
 */
public class RetroStyleView  extends View{

    public RetroStyleView(Context context) {
        super(context);
    }

    public RetroStyleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RetroStyleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //需要关闭硬件加速(没有关闭则没效果)
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_iamge1);
        canvas.drawBitmap(bitmap, null,
                new RectF(0, 0, 400, 400 * bitmap.getHeight() / bitmap.getWidth()), paint);
        canvas.translate(400, 0);

        ColorMatrix matrix = new ColorMatrix(new float[]{
                1/2f, 1/2f, 1/2f, 0, 0,
                1/3f, 1/3f, 1/3f, 0, 0,
                1/4f, 1/4f, 1/4f, 0, 0,
                0, 0, 0, 1f, 0,
        });
        //设置颜色过滤器
        paint.setColorFilter(new ColorMatrixColorFilter(matrix));
        canvas.drawBitmap(bitmap, null,
                new RectF(0, 0, 400, 400 * bitmap.getHeight() / bitmap.getWidth()), paint);

    }
}
