package jcc.example.com.broweimg;

import com.mi.baselibrary.base.app.BaseApplication;

/**
 * Created by jincancan on 2018/3/15.
 * Description:
 */

public class JApp extends BaseApplication {

    private static JApp instance;

    public static JApp getIns(){
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

}
