package jcc.example.com.broweimg.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.LogUtils;
import com.mi.baselibrary.base.view.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import jcc.example.com.broweimg.R;
import jcc.example.com.broweimg.adapter.ChangeImageAdapter;
import jcc.example.com.broweimg.util.ChangeImageColorUtils;

/**
 * 修改图片
 */
public class ChangeImageActivity extends BaseActivity {


    ImageView iv_image;

    RecyclerView change_image;
    private ChangeImageAdapter changeImageAdaptere;

    @Override
    public int getContentView() {
        return R.layout.activity_change_image;
    }

    private Bitmap bitmap;

    @Override
    public void initView() {
        iv_image=findViewById(R.id.iv_image);
        change_image=findViewById(R.id.change_image);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        change_image.setLayoutManager(gridLayoutManager);
        changeImageAdaptere = new ChangeImageAdapter(this, mContent);
        bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_iamge1, null);
        iv_image.setImageBitmap(bitmap);
    }

    @Override
    public void initListener() {
        changeImageAdaptere.setOnItemClickListener(new ChangeImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                LogUtils.e(position);
                Bitmap mbitmap = null;
                if (position == 0) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeBlurBitmap(bitmap);
                } else if (position == 1) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeBlueBitmap(bitmap);
                } else if (position == 2) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeEnhancementBitmap(bitmap);
                } else if (position == 3) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeInverseEffecttBitmap(bitmap);
                } else if (position == 4) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeRetroStyleBitmap(bitmap);
                } else if (position == 5) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeReverseBitmap(bitmap);
                } else if (position == 6) {
                    mbitmap = ChangeImageColorUtils.getInstance().makeBlackOrWhiteBitmap(bitmap);
                } else {
                    mbitmap = ChangeImageColorUtils.getInstance().makeEmbossBitmap(bitmap);
                }
                iv_image.setImageBitmap(mbitmap);
            }
        });
    }

    private List<String> mContent = new ArrayList<>();

    @Override
    public void initData() {
        mContent.clear();
        mContent.add("模糊");
        mContent.add("蓝色");
        mContent.add("颜色增强");
        mContent.add("反相效果");
        mContent.add("复古");
        mContent.add("反色");
        mContent.add("黑白图片");
        mContent.add("浮雕");
        change_image.setAdapter(changeImageAdaptere);
    }
}
