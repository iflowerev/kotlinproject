package jcc.example.com.broweimg.api;
import com.mi.baselibrary.base.manager.RetrofitWrapper;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class OnLineMusicModel {


    /*
     * 静态代码块
     * 主要是作用于设置中心debug模式下，点击三下标题可以切换环境
     * 1001，测试接口
     * 2001，开发接口
     * 3001，正式接口
     */
    static {
        //int anInt = SPUtils.getInstance(Constant.SP_NAME).getInt(DebugActivity.SELECT_STATUS);
        int anInt = 1001;
        switch (anInt){
            case 1001:
                BASE_URL = "http://tingapi.ting.baidu.com/";
                break;
            case 2001:
                BASE_URL = "http://tingapi.ting.baidu.com/";
                break;
            case 3001:
                BASE_URL = "http://tingapi.ting.baidu.com/";
                break;
            default:
                BASE_URL = "http://tingapi.ting.baidu.com/";
                break;
        }
    }



    private static String BASE_URL = "https://api.zsfmyz.top/";


    private static OnLineMusicModel model;
    private OnLineMusicApi mApiService;

    private OnLineMusicModel() {
        mApiService = RetrofitWrapper
                .getInstance(BASE_URL)
                .create(OnLineMusicApi.class);
    }


    public static OnLineMusicModel getInstance(){
        if(model == null) {
            model = new OnLineMusicModel();
        }
        return model;
    }

    /**
     * 图片列表
     * @param p
     * @param n
     * @return
     */
    public Observable<ResponseBody> welfareList(int n,int p) {
        return mApiService.welfareList(p, n).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
