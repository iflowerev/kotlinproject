package jcc.example.com.broweimg.ui;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mi.baselibrary.base.view.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;
import jcc.example.com.broweimg.R;
import jcc.example.com.broweimg.adapter.ImageListAdapter;
import jcc.example.com.broweimg.api.OnLineMusicModel;
import jcc.example.com.broweimg.bean.ImageBean;
import jcc.example.com.broweimg.picturezoom.picman.ImagePagerActivity;
import jcc.example.com.broweimg.picturezoom.utils.AllUtils;
import okhttp3.ResponseBody;

public class BigImageActivity  extends BaseActivity {

    RecyclerView list_image;
    private ImageListAdapter mImageListAdapter;
    @Override
    public int getContentView() {
        return R.layout.activity_big_image;
    }

    @Override
    public void initView() {
        list_image=findViewById(R.id.list_image);
//        //线性布局
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        list_image.setLayoutManager(linearLayoutManager);
        //如果是横向滚动，后面的数值表示的是几行，如果是竖向滚动，后面的数值表示的是几列
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        list_image.setLayoutManager(gridLayoutManager);
        mImageListAdapter=new ImageListAdapter(this,mUrl);
    }

    @Override
    public void initListener() {
        mImageListAdapter.setOnItemClickListener(new ImageListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position, List<ImageView> mImageView) {
                AllUtils.startImagePage(BigImageActivity.this, ImagePagerActivity.class,mUrl,mImageView,position);
            }
        });
    }

    private List<String> mUrl=new ArrayList<>();
    @Override
    public void initData() {
        //开始请求网络
        OnLineMusicModel model = OnLineMusicModel.getInstance();
        model.welfareList(2,20).subscribe(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
//                LogUtils.e(responseBody.string());
                mUrl.clear();
                ImageBean imageBean=new Gson().fromJson(responseBody.string(),ImageBean.class);
                for (int i=0;i<imageBean.getData().getResults().size();i++){
                    mUrl.add(imageBean.getData().getResults().get(i).getUrl());
                }
                list_image.setAdapter(mImageListAdapter);
            }
        });

//        mUrl.clear();
//        mUrl.add("http://img.hb.aicdn.com/f22df3bca217f7435b6b7d3c66bc6d21d72b60c3a70eb-yctL70_fw658");
//        mUrl.add("http://img.hb.aicdn.com/266e4c85ef38c4ef468dd28cc5ae9deba47080867d89-urhOsD_fw658");
//        mUrl.add("http://img.hb.aicdn.com/652b269af2818f6f1c468399e00152d73d0a7beb29d1e-2vnLBW_fw658");
//        mUrl.add("http://img.hb.aicdn.com/b8ce046106dc17ebb3782351f2a493b52daf149611f57-YkEgOp_fw658");
//        list_image.setAdapter(mImageListAdapter);

    }
}
