package jcc.example.com.broweimg.ui;
import android.view.View;
import android.widget.Button;
import com.mi.baselibrary.base.view.BaseActivity;
import jcc.example.com.broweimg.R;

public class MainActivity extends BaseActivity{

     Button  btn1;

    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        btn1=findViewById(R.id.btn1);
    }

    @Override
    public void initListener() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(VagueImageActivity.class);
            }
        });
    }

    @Override
    public void initData() {
    }

}
