package jcc.example.com.broweimg.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import jcc.example.com.broweimg.R;

public class MyImage  extends View {

    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Bitmap mBitmap;
    private float [] array=new float[20];

    public MyImage(Context context) {
        super(context);
    }

    public MyImage(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mBitmap = BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ic_iamge1);
        invalidate();
    }

    public MyImage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public void setValues(float [] a){
        for(int i=0;i<20;i++){
            array[i]=a[i];
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = mPaint;
        canvas.drawBitmap(mBitmap, 0, 0, paint);
        Matrix cm = new Matrix();
        ColorMatrix myColorMatrix = new ColorMatrix();
        myColorMatrix.set(array);
        paint.setColorFilter(new ColorMatrixColorFilter(myColorMatrix));
        canvas.drawBitmap(mBitmap, cm, paint);
        Log.i("CMatrix", "--------->onDraw");
    }
}
