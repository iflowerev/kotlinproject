package jcc.example.com.broweimg.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import jcc.example.com.broweimg.R;

/**
 * 模糊图片
 */
public class VagueImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vagueimage);
    }
}
