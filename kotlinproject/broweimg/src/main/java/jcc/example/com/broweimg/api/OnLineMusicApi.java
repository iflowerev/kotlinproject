package jcc.example.com.broweimg.api;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OnLineMusicApi {

    /**
     * 图片列表
     */
    @GET("welfare/list")
    Observable<ResponseBody> welfareList(@Query("per_page") int per_page,
                                         @Query("page") int page);
}
