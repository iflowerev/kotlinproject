package jcc.example.com.broweimg.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;
import jcc.example.com.broweimg.R;

/**
 * 遮罩滤镜处理-----模糊
 */
public class BlurMaskFiltersView extends View {

    public BlurMaskFiltersView(Context context) {
        super(context);
    }

    public BlurMaskFiltersView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BlurMaskFiltersView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //需要关闭硬件加速(没有关闭则没效果)
        setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);


        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_iamge1);

        canvas.drawBitmap(bitmap,null,new RectF(0,0,350,400*bitmap.getHeight()/bitmap.getWidth()),paint);

        canvas.translate(400,0);

        /**
         * 模糊遮罩 滤镜效果
         * Blur.NORMAL 内外都模糊绘制
         * Blur.SOLID 内部正常绘制，外部模糊
         * Blur.OUTER 内部模糊，外部不绘制
         * Blur.INNER  内部不绘制，外部模糊
         */
        paint.setMaskFilter(new BlurMaskFilter(50, android.graphics.BlurMaskFilter.Blur.NORMAL));
        canvas.drawBitmap(bitmap,null,new RectF(0,0,350,400*bitmap.getHeight()/bitmap.getWidth()),paint);
//        canvas.drawBitmap(bitmap,null,new RectF(0,0,bitmap.getWidth(),bitmap.getHeight()),paint);
    }
}
