package jcc.example.com.broweimg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import jcc.example.com.broweimg.R;
/**
 * 修改图片
 */
public class ChangeImageAdapter extends RecyclerView.Adapter<ChangeImageAdapter.MyViewHolder>{
    private Context context;
    private List<String> contentStr;
    public ChangeImageAdapter(Context mContext, List<String> mContentStr) {
        this.context = mContext;
        this.contentStr = mContentStr;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return  new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_image_content, parent,
                false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
         holder.btn.setText(contentStr.get(position));
        //itemView 的点击事件
        if (mItemClickListener != null) {
            holder.btn.setOnClickListener(v -> {
                int adapterPosition = holder.getAdapterPosition();
                mItemClickListener.onItemClick(v, adapterPosition);
            });
        }
    }
    @Override
    public int getItemCount() {
        return contentStr.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        Button btn;
        public MyViewHolder(View view) {
            super(view);
            btn=(Button) view.findViewById(R.id.btn);
        }
    }
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener l) {
        this.mItemClickListener = l;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position);
    }

}
