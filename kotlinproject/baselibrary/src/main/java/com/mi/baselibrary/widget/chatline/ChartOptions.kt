package com.mi.baselibrary.widget.chatline
import android.graphics.Color
import android.graphics.PathEffect
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange

class ChartOptions {

    //x轴配置
    var xAxisOptions = AxisOptions()

    //y轴配置
    var yAxisOptions = AxisOptions()

    //数据配置
    var dataOptions =DataOptions()
}

/**
 * 轴线配置参数
 */

class  AxisOptions{
    companion object{

        private const val  DEFAULT_TEXT_SIZE = 20f

        private const val DEFAULT_TEXT_COLOR = Color.BLACK

        private const val DEFAULT_TEXT_MARGIM = 20

        private const val DEFAULT_LINE_WIDTH = 2f

        private const val DEFAULT_RULER_WIDTH = 10f
    }

    /**
     * 文字大小
     */
    @FloatRange(from = 1.0)
    var textSize:Float = DEFAULT_TEXT_SIZE

    @ColorInt
    var textColor:Int= DEFAULT_TEXT_COLOR

    /**
     * X轴文字内容上下两侧margin
     */
    var textMarginTop:Int= DEFAULT_TEXT_MARGIM

    var textMarginBottom:Int = DEFAULT_TEXT_MARGIM

    /**
     * Y轴文字内容左右两侧margin
     */
    var textMarginLeft:Int= DEFAULT_TEXT_MARGIM

    var textMarginRight:Int= DEFAULT_TEXT_MARGIM

    /**
     * 轴线
     */
    var lineWidth:Float= DEFAULT_LINE_WIDTH

    @ColorInt
    var lineColor:Int= DEFAULT_TEXT_COLOR

    var isEnableLine = true

    var linePathEffect: PathEffect?=null

    /**
     * 刻度
     */
    var rulerWidth = DEFAULT_LINE_WIDTH

    var rulerHeight= DEFAULT_RULER_WIDTH

    @ColorInt
    var rulerColor = DEFAULT_TEXT_COLOR

    var isEnableRuler =true

    /**
     * 网格
     */
    var gridWidth:Float= DEFAULT_LINE_WIDTH

    @ColorInt
    var gridColor:Int = DEFAULT_TEXT_COLOR

    var gridPathEffect: PathEffect?=null

    var isEnableGrid =true

}

/**
 * 数据配置参数
 */

class DataOptions{
    companion object{

        private const val DEFAULT_PATH_WIDTH =2f

        private const val DEFAULT_PATH_COLOR =Color.BLACK

        private const val DEFAULT_CIRCLE_RADIUS =10f

        private const val DEFAULT_CIRCEL_COLOR=Color.BLACK

    }

    var pathWidth = DEFAULT_PATH_WIDTH

    var pathColor = DEFAULT_PATH_COLOR

    var circleRadius = DEFAULT_CIRCLE_RADIUS

    var circleColor = DEFAULT_CIRCEL_COLOR
}