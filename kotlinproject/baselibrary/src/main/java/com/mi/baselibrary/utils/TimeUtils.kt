package com.mi.baselibrary.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * 时间工具
 */
object TimeUtils {
    /**
     * 描述：获取表示当前日期时间的字符串.
     * @param format 格式化字符串，如："yyyy-MM-dd HH:mm:ss"
     * @return String String类型的当前日期时间
     */
    fun getCurrentData(format:String):String{
        var curDateTime: String? = null
        try {
            val mSimpleDateFormat = SimpleDateFormat(format)
            val c: Calendar = GregorianCalendar()
            curDateTime = mSimpleDateFormat.format(c.time)
        } catch (e: Exception) {
            e.printStackTrace()
        }
      return  curDateTime!!
    }

    /**
     * 描述：计算两个日期所差的天数.
     *
     * @param date1 第一个时间的毫秒表示
     * @param date2 第二个时间的毫秒表示
     * @return int 所差的天数
     */
    fun getOffectDay(date1: Long, date2: Long): Int {
        val calendar1 = Calendar.getInstance()
        calendar1.timeInMillis = date1
        val calendar2 = Calendar.getInstance()
        calendar2.timeInMillis = date2
        //先判断是否同年
        val y1 = calendar1[Calendar.YEAR]
        val y2 = calendar2[Calendar.YEAR]
        val d1 = calendar1[Calendar.DAY_OF_YEAR]
        val d2 = calendar2[Calendar.DAY_OF_YEAR]
        var maxDays = 0
        var day = 0
        if (y1 - y2 > 0) {
            maxDays = calendar2.getActualMaximum(Calendar.DAY_OF_YEAR)
            day = d1 - d2 + maxDays
        } else if (y1 - y2 < 0) {
            maxDays = calendar1.getActualMaximum(Calendar.DAY_OF_YEAR)
            day = d1 - d2 - maxDays
        } else {
            day = d1 - d2
        }
        return day
    }

    /**
     * 描述：计算两个日期所差的小时数.
     *
     * @param date1 第一个时间的毫秒表示
     * @param date2 第二个时间的毫秒表示
     * @return int 所差的小时数
     */
    fun getOffectHour(date1: Long, date2: Long): Int {
        val calendar1 = Calendar.getInstance()
        calendar1.timeInMillis = date1
        val calendar2 = Calendar.getInstance()
        calendar2.timeInMillis = date2
        val h1 = calendar1[Calendar.HOUR_OF_DAY]
        val h2 = calendar2[Calendar.HOUR_OF_DAY]
        var h = 0
        val day: Int = getOffectDay(date1, date2)
        h = h1 - h2 + day * 24
        return h
    }

    /**
     * 描述：计算两个日期所差的分钟数.
     *
     * @param date1 第一个时间的毫秒表示
     * @param date2 第二个时间的毫秒表示
     * @return int 所差的分钟数
     */
    fun getOffectMinutes(date1: Long, date2: Long): Int {
        val calendar1 = Calendar.getInstance()
        calendar1.timeInMillis = date1
        val calendar2 = Calendar.getInstance()
        calendar2.timeInMillis = date2
        val m1 = calendar1[Calendar.MINUTE]
        val m2 = calendar2[Calendar.MINUTE]
        val h: Int = getOffectHour(date1, date2)
        var m = 0
        m = m1 - m2 + h * 60
        return m
    }

}