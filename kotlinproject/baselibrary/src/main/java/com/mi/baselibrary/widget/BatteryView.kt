package com.mi.baselibrary.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.view.View

/**
 * 自定义电池view
 */
class BatteryView : View {
    private var mWidth = 0
    private var mHeight = 0
    private val mWeight = 5 //画笔宽度
    private var mPower = 1 //电池电量
    private var rote = 0 //电池四周弧度
    private var mPoverRight = mWeight.toFloat()

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        //绘制电池的四周
        val paint1 =
            Paint(Paint.ANTI_ALIAS_FLAG)

        //绘制电池得电量
        val paint2 =
            Paint(Paint.ANTI_ALIAS_FLAG)

        //电池的纽扣
        val paint3 =
            Paint(Paint.ANTI_ALIAS_FLAG)

        //设置画笔颜色
        if (mPower < 21) {
            paint2.color = Color.GREEN
            rote = 10
        } else if (mPower < 71) {
            paint2.color = Color.BLUE
            rote = 0
        } else {
            paint2.color = Color.RED
            rote = 0
        }
        //电量view进度
        mPoverRight = (mWeight - mPower * mWeight / 100).toFloat()
        paint1.color = Color.BLACK
        paint3.color = Color.BLACK

        //设置画笔粗细
        paint1.strokeWidth = mWidth.toFloat()
        paint2.strokeWidth = mWidth.toFloat()
        paint3.strokeWidth = mWeight.toFloat()

        //设置画笔是否空心
        paint1.style = Paint.Style.STROKE
        paint3.style = Paint.Style.FILL
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawRoundRect(0f, 0f, mWidth.toFloat(), mHeight.toFloat(), 10f, 10f, paint1)
            canvas.drawRoundRect(
                mWeight - 3.toFloat(),
                mWeight - 3.toFloat(),
                mPoverRight,
                mHeight - mWeight + 3.toFloat(),
                rote.toFloat(),
                rote.toFloat(),
                paint2
            )
            canvas.drawRoundRect(
                mWidth.toFloat(),
                mHeight / 2 - (mWeight * 3).toFloat(),
                mWidth + (mHeight * 2).toFloat(),
                mHeight / 2 + (mWeight * 3).toFloat(),
                10f,
                10f,
                paint3
            )
        } else {
            canvas.drawRect(RectF(0F, 0F, mWidth.toFloat(), mHeight.toFloat()), paint1)
            canvas.drawRect(
                RectF(
                    (mWeight - 3).toFloat(),
                    (mWeight - 3).toFloat(),
                    mPoverRight,
                    (mHeight - mWeight + 3).toFloat()
                ),
                paint2
            )
            canvas.drawRect(
                RectF(
                    mWidth.toFloat(),
                    (mHeight / 2 - mWeight * 3).toFloat(),
                    (mWidth + mWeight * 2).toFloat(),
                    (mHeight / 2 + mWeight * 3).toFloat()
                ), paint3
            )
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        //对View上的內容进行测量后得到的View內容占据的宽度
        mWidth = measuredWidth - 50
        //对View上的內容进行测量后得到的View內容占据的高度
        mHeight = measuredHeight
    }

    /**
     * 设置电池电量
     *
     * @param power
     */
    fun setPower(power: Int) {
        mPower = power
        if (mPower < 0) {
            mPower = 100
        }
        invalidate()
    }

    /**
     * 获取电池电量
     */
    fun getmPower(): Int {
        return mPower
    }
}