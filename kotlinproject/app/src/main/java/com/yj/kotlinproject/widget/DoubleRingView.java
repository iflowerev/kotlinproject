package com.yj.kotlinproject.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class DoubleRingView extends View {

    private Paint paint;
    private int outerRadius; // 外环半径
    private int innerRadius; // 内环半径

    public DoubleRingView(Context context) {
        super(context);
        init();
    }

    public DoubleRingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DoubleRingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true); // 抗锯齿
        paint.setColor(0xFFFF0000); // 红色外环

        outerRadius = 300; // 假设外环半径为100px
        innerRadius = 260; // 假设内环半径为50px
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // 计算位置
//        int centerX = getWidth() / 2;
//        int centerY = getHeight() / 2;

        int centerX = getWidth()/2 ;
        int centerY = getHeight()/2 ;

        // 绘制外环
        canvas.drawCircle(centerX, centerY, outerRadius, paint);

        // 设置内环的颜色和半径
        paint.setColor(0xFFFFFF00); // 黄色内环
        paint.setAntiAlias(true);

        // 绘制内环
        canvas.drawCircle(centerX, centerY, innerRadius, paint);
    }
}