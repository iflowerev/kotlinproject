package com.yj.kotlinproject.test;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.yj.kotlinproject.R;
import com.yj.kotlinproject.update_app.ui.JavaActivity;
import com.yj.kotlinproject.update_app.ui.KotlinActivity;

public class MainTestActivity extends AppCompatActivity {

    private Button btn_java_update,btn_kotlin_update;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_main);
        btn_java_update=findViewById(R.id.btn_java_update);

        btn_kotlin_update=findViewById(R.id.btn_kotlin_update);

        initEvent();
    }

    private void initEvent(){
        btn_java_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainTestActivity.this, JavaActivity.class));
            }
        });

        btn_kotlin_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainTestActivity.this, KotlinActivity.class));
            }
        });
    }
}
