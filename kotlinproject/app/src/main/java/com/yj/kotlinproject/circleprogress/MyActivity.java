package com.yj.kotlinproject.circleprogress;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mi.baselibrary.widget.circleprogress.ArcProgress;
import com.mi.baselibrary.widget.circleprogress.CircleProgress;
import com.mi.baselibrary.widget.circleprogress.DonutProgress;
import com.yj.kotlinproject.R;

import java.util.Timer;

public class MyActivity extends AppCompatActivity  implements View.OnClickListener {
    private Timer timer;
    private DonutProgress donutProgress;
    private CircleProgress circleProgress;
    private ArcProgress arcProgress;

    private Button btn1,btn2,btn3;
    @SuppressLint("MissingInflatedId")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        donutProgress = (DonutProgress) findViewById(R.id.donut_progress);
        donutProgress.setMax(100);
        donutProgress.setPrefixText("");
        donutProgress.setSuffixText("0/100");
        btn1=findViewById(R.id.btn1);
        btn2=findViewById(R.id.btn2);
        btn3=findViewById(R.id.btn3);
        /*AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.progress_anim2);
        set.setTarget(donutProgress);
        set.start();*/

        /*ValueAnimator animation = ValueAnimator.ofObject(new IntEvaluator(), 0, 100);
        animation.setDuration(1000);
        animation.start();*/

        /*PropertyValuesHolder donutAlphaProperty = PropertyValuesHolder.ofFloat("alpha", 0f, 1f);
        PropertyValuesHolder donutProgressProperty = PropertyValuesHolder.ofInt("donut_progress", 0, 100);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(donutProgress, donutAlphaProperty, donutProgressProperty);
        animator.setDuration(2000);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.start();*/

//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        boolean a = false;
//                        if (a) {
//                            ObjectAnimator anim = ObjectAnimator.ofInt(donutProgress, "progress", 0, 10);
//                            anim.setInterpolator(new DecelerateInterpolator());
//                            anim.setDuration(500);
//                            anim.start();
//                        } else {
//                            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(MyActivity.this, R.animator.progress_anim);
//                            set.setInterpolator(new DecelerateInterpolator());
//                            set.setTarget(donutProgress);
//                            set.start();
//                        }
//                    }
//                });
//            }
//        }, 0, 2000);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        mHandler.sendEmptyMessage(1000);
    }

    private Integer slNumber=5;
    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
             switch (msg.what){
                 case 1000:
                     slNumber=slNumber+5;
                     if(slNumber<=100){
                         donutProgress.setProgress(slNumber);
//                         donutProgress.setPrefixText("");
                         donutProgress.setSuffixText("/100");
                         mHandler.sendEmptyMessageDelayed(1000,2500);
                     }
                     break;
             }
        }
    };
    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
    @Override
    public void onClick(View item) {
        if(item.getId()== R.id.btn1){
            startActivity(new Intent(this, ViewPagerActivity.class));
        }
        if(item.getId()== R.id.btn2){
            startActivity(new Intent(this, ItemListActivity.class));
        }
        if(item.getId()== R.id.btn3){
            startActivity(new Intent(this, ArcInFragment.class));
        }
    }
}
