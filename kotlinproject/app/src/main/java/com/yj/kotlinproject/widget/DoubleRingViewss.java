package com.yj.kotlinproject.widget;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.view.View;

import com.yj.kotlinproject.R;

public class DoubleRingViewss  extends View {
    private ShapeDrawable outerRing;
    private ShapeDrawable innerRing;

    public DoubleRingViewss(Context context) {
        super(context);
        init();
    }

    public DoubleRingViewss(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DoubleRingViewss(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        outerRing = new ShapeDrawable(new OvalShape());
        innerRing = new ShapeDrawable(new OvalShape());
        outerRing.getPaint().setColor(getResources().getColor(R.color.outer_ring_color));
        innerRing.getPaint().setColor(getResources().getColor(R.color.inner_ring_color));
        setBackgroundDrawable(getResources().getDrawable(R.drawable.double_ring));
    }
}