package com.yj.kotlinproject

import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Adapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog

/**
 * MD 风格的底部弹窗
 */
class BottomSheetActivity :AppCompatActivity(){

    private var btn1:Button?=null
    private var btn2:Button?=null
    private var btn3:Button?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)
        initView()
        initEvent()
    }

    private fun initView() {
        btn1=findViewById(R.id.btn_1)
        btn2=findViewById(R.id.btn_2)
        btn3=findViewById(R.id.btn_3)
    }

    private fun initEvent() {
        btn2!!.setOnClickListener {
            initBottomSheetDialog()
        }
    }
    private lateinit var bottomSheetDialog : BottomSheetDialog
    private fun initBottomSheetDialog(){
        bottomSheetDialog = BottomSheetDialog(this)
        // 添加 style
        bottomSheetDialog = BottomSheetDialog(this, R.style.BottomSheetDialog)
        val view = LayoutInflater.from(this).inflate(R.layout.include_bottomsheetdialog, null)
        val rv=view.findViewById<RecyclerView>(R.id.rv)
        val layoutManager = StaggeredGridLayoutManager(4,StaggeredGridLayoutManager.VERTICAL)
         rv.layoutManager = layoutManager
        bottomSheetDialog.setContentView(view)
        bottomSheetDialog.setCancelable(true)
        bottomSheetDialog.setCanceledOnTouchOutside(true)
        bottomSheetDialog.show()
    }
}