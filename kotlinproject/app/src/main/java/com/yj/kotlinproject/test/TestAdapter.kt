package com.yj.kotlinproject.test

import android.content.Context
import android.view.View
import android.widget.TextView
import com.adapter.CommonAdapter
import com.adapter.CommonViewHolder.get
import com.yj.kotlinproject.R
class TestAdapter(private val mContext: Context, layoutId: Int) : CommonAdapter<String?>(mContext, layoutId) {
    override fun convertView(item: View?, testBean: String?, position: Int) {
        val tv_name = get<TextView>(item, R.id.tv_name)
        tv_name.text = testBean
    }
}