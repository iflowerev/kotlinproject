package com.yj.kotlinproject.test

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.yj.kotlinproject.R
import java.util.*

class TestActivity : AppCompatActivity() {
    private var list_item: ListView? = null
    private var testAdapter: TestAdapter? = null
    private var data: MutableList<String>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        list_item = findViewById(R.id.list_item)
        testAdapter = TestAdapter(this, R.layout.item_test)
        list_item!!.setAdapter(testAdapter)
        initData()
        initEvent()
    }

    private var mPosition = 0
    private fun initEvent() {
        list_item!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            mPosition = position
            val intent = Intent(this@TestActivity, TestDetailsActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initData() {
        data = ArrayList()
        for (i in 0..19) {
            data!!.add("第" + i + "行")
        }
    }

    override fun onResume() {
        super.onResume()
        if (mPosition == 0) {
            testAdapter!!.datas = (data as List<String?>?)!!
        } else {
//            data!![mPosition-1] = "改变了00"
//            data!![mPosition+1] = "改变了00"
//            data!![mPosition] = "改变了"
            change()
            testAdapter!!.notifyDataSetChanged()
//            testAdapter!!.notifyDataSetChanged(list_item, mPosition)
        }
    }
    private  fun change(){
        data!!.forEachIndexed { index, s ->

            data!![index]="改变了"+s
        }
    }
}