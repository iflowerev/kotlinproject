package com.yj.kotlinproject
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout

/**
 * 侧边栏
 */
class DrawerLayoutActivity : AppCompatActivity(){

    private var mDrawerLayout: DrawerLayout?=null
    private var tvOpen:TextView?=null
    private var tvClose:TextView?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer_layout)
        initView()
        initEvent()
    }

    private fun initEvent(){
        //关闭手势滑动
        mDrawerLayout!!.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        tvOpen!!.setOnClickListener {
            mDrawerLayout!!.openDrawer(GravityCompat.START)
        }
        tvClose!!.setOnClickListener {
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
        }
    }

    private fun initView() {
        mDrawerLayout=findViewById(R.id.root)
        tvOpen=findViewById(R.id.tv_open)
        tvClose=findViewById(R.id.tv_close)
    }
}