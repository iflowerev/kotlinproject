package com.yj.kotlinproject;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.yj.widget.TempHumView;

public class TempHumViewActivity extends AppCompatActivity {
    private TempHumView mTempHumView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_temp_hum);
         mTempHumView=findViewById(R.id.device_temp_hum);
         mTempHumView.setHum(20);
         mTempHumView.setTemp(30);
    }
}
