@file:Suppress("NOTHING_TO_INLINE")

package com.yj.kotlinproject.update_app.ext

import android.app.Activity
import com.yj.kotlinproject.update_app.util.CProgressDialogUtils

/**
 * Created by Vector
 * on 2017/7/18 0018.
 */
inline fun Activity.showProgressDialog() {
    CProgressDialogUtils.showProgressDialog(this)
}

inline fun Activity.cancelProgressDialog() {
    CProgressDialogUtils.cancelProgressDialog(this)
}