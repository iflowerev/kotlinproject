package com.yj.algorithm

import android.util.Log
class algorithm2 {
    companion object {
        private val instance = algorithm2()

        /**
         * 获取单例
         */
        fun getInstance() = instance
    }

    fun makeComplement(dna : String) =
            dna.toCharArray().map { molecule ->
                when (molecule) {
                    'A' -> 'T'
                    'T' -> 'A'
                    'C' -> 'G'
                    'G' -> 'C'
                    else -> ' '
                }
            }.joinToString(separator = "")


    fun makeComplementMy(dna : String) : String {
        val arrayStr= dna.split("").toMutableList()
        var sb=StringBuffer()
         arrayStr.forEachIndexed { index, s ->
             when (s) {
                 "A" -> {
                     arrayStr[index]="T"
                 }
                 "T" -> {
                     arrayStr[index]="A"
                 }
                 "G" -> {
                     arrayStr[index]="C"
                 }
                 "C" -> {
                     arrayStr[index]="G"
                 }
             }
             sb.append(arrayStr[index])
         }
        Log.e("sb===",sb.toString())
        return sb.toString()
    }
}