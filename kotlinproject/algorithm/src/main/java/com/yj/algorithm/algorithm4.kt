package com.yj.algorithm

import android.util.Log
import java.util.*

class algorithm4 {
    companion object {
        private val instance = algorithm4()

        /**
         * 获取单例
         */
        fun getInstance() = instance
    }
    fun findEvenIndex(arr:IntArray):Int {
        var index=-1
       for(m in arr.indices){
           var allNumber1=0
           var allNumber2=0
           arr.copyOf(m).forEach {
               allNumber1+=it
           }
           arr.copyOfRange(m+1,arr.size).forEach {
                   allNumber2+=it
           }
           if(allNumber1==allNumber2){
                   index=m
           }
        }
        Log.e("index",index.toString())
        return  index
    }

}