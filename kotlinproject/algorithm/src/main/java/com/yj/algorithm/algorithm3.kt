package com.yj.algorithm

import android.util.Log
class algorithm3 {
    companion object {
        private val instance = algorithm3()

        /**
         * 获取单例
         */
        fun getInstance() = instance
    }

    fun Kata(word : String) : String {
        val valueNumber=(word.length+1)%2
        var isJo= valueNumber != 0
        val split=(word.length+1)/2
        var sb=StringBuffer()
        word.toCharArray().forEachIndexed { index, c ->
            if(index==split-1){
                sb.append(c)
            }
            if(isJo&&index==split){
                sb.append(c)
            }
        }
        Log.e("sb===",sb.toString())
        return  sb.toString()
    }

    fun Kata1(word : String) : String {

        val arrayStr= word.split("")
        val valueNumber=(arrayStr.size+1)%2
        var isJo= valueNumber != 0
        val split=(arrayStr.size+1)/2
        var sb=StringBuffer()
        arrayStr.forEachIndexed { index, c ->
            if(index==split-1){
                sb.append(c)
            }
            if(isJo&&index==split){
                sb.append(c)
            }
        }
        Log.e("sb===",sb.toString())
        return  sb.toString()
    }

}