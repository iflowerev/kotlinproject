package com.xian.jepathapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Jetpack架构组件库
 *
 *  Jepack是众多优秀组件的集合。是谷歌推出的一套引领Android开发者逐渐统一开发规范的架构
 *
 * 优势:
 *  基于生命周期感知的能力，可以减少NPE崩溃、内存泄露、模块代码。为我们开发出健壮且高质量的程序提供强力保障
 *  组件可以单独使用，也可以搭配使用，搭配Kotlin语言特性可进一步加速开发。
 *
 *  介绍:
 *  1.Navigation:为单Activity架构而生的端内路由
 *  特性:Activity , Fragment, Dialog提供路由能力的组件、导航时可携带参数、指定转场动画、支持
 *  deepline页面直达、fragment回退栈管理能力。
 *  不足之处:十分依赖xml文件、不利于模块化、组件化开发。
 *  2. Lifecycle:举办宿主声明后期感知能力的组件。
 *  特性: 它持有组件(如Activity或Fragment)生命周期状态的信息，并且允许其他对象观察此状态。
 *  3. ViewModel:具备生命周期感知能力的数据存储组件。
 *  特性: 页面配置更改数据不丢失、生命周期感应、数据共享
 *  4. LiveData: 具备生命周期感知能力的数据订阅、分发组件。
 *  特性: 支持共享资源、支持黏性事件的分发、不再需要手动处理生命周期、确保界面符号数据状态。
 *  不足: 黏性事件不支持取消
 *  5. Room: 轻量级orm数据库，本质上是一个SQLite抽象层。
 *  特性: 使用更加简单(类似于Retrofit库)。通过注解的方式实现相关功能。编译时自动生成实现类impl
 *  6. DataBinding: dataBinding只是一种工具，它解决的是View和数据之间的双向绑定。MVVM是一种架构模式，
 *  两者是有本质区别的。
 *  特性: 数据与视图双向绑定、数据绑定空安全、减少模块代码、释放Activity/Fragment
 *  7.WorkManager: 新一代后台任务管理组件，功能十分强悍。Service能做的事情它都能做
 *  支持周期性任务调度、链式任务调度、丰富的任务约束条件、即便程序退出，依旧能保证任务的执行
 *  8. Paging列表分页组件，可以轻松完成分页预加载以达到无限滑动的效果
 *  巧妙融合LiveData、提供多种数据源加载方式
 *  不足支持，不支持列表数据增删改。列表添加HeaderView,FooterView定位不准确。
 */

/**
 *
 * 什么是Lifecycle
 * 具备宿主声明后期感知能力的组件。它能持有组件(如Activity或Fragment)生命周期状态的信息，并且允许其他观察者监听宿主的状态
 *
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //启动服务
        startService(new Intent(this,MyAccessibilityService.class));
    }
}


/**
 *
 * native_crash如何监控
 * NativeCrashListener线程监控
 * Java Crash监控:
 * 1.如何收集Java_Crash 日志
 *
 *  UncaugthExceptionHandler (昂康持诶个赛科身含内得)   --------> 收集设备+堆栈信息写入文件 --------------> 杀进程、重启App
 *
 *   设备信息: 设备类型、OS版本、线程名、前后台、使用时长、App版本、升级渠道
 *   CPU架构、内存信息、存储信息、permission权限
 *
 * 2.如何重启应用
 *
 *
 * 3.如何还原混淆代码
 *  GUI工具
 *
 */



